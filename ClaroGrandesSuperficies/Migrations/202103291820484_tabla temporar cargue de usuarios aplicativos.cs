namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tablatemporarcarguedeusuariosaplicativos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TemporalAsignacionAplicativosUsuario",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdAplicativo = c.Int(nullable: false),
                        Documento = c.String(),
                        ClaveAplicativo = c.String(),
                        EstadoAplicativo = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                        AplicativoUsuario_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Aplicativo", t => t.AplicativoUsuario_Id)
                .Index(t => t.AplicativoUsuario_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TemporalAsignacionAplicativosUsuario", "AplicativoUsuario_Id", "dbo.Aplicativo");
            DropIndex("dbo.TemporalAsignacionAplicativosUsuario", new[] { "AplicativoUsuario_Id" });
            DropTable("dbo.TemporalAsignacionAplicativosUsuario");
        }
    }
}
