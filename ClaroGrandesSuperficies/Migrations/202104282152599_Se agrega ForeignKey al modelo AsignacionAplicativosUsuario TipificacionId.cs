namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeagregaForeignKeyalmodeloAsignacionAplicativosUsuarioTipificacionId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AplicativosUsuario", "TipificacionId", c => c.Int());
            CreateIndex("dbo.AplicativosUsuario", "TipificacionId");
            AddForeignKey("dbo.AplicativosUsuario", "TipificacionId", "dbo.Tipificaciones", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AplicativosUsuario", "TipificacionId", "dbo.Tipificaciones");
            DropIndex("dbo.AplicativosUsuario", new[] { "TipificacionId" });
            DropColumn("dbo.AplicativosUsuario", "TipificacionId");
        }
    }
}
