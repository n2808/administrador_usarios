namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addstatustoActivacionaplicativo : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ActivacionAplicativo", "IdAplicativo", "dbo.Aplicativo");
            DropForeignKey("dbo.ActivacionAplicativo", "UserId", "dbo.Users");
            DropIndex("dbo.ActivacionAplicativo", new[] { "IdAplicativo" });
            DropIndex("dbo.ActivacionAplicativo", new[] { "UserId" });
            AddColumn("dbo.ActivacionAplicativo", "Aplicativonombre", c => c.String());
            AddColumn("dbo.ActivacionAplicativo", "EstadoAplicativoNombre", c => c.String());
            AlterColumn("dbo.ActivacionAplicativo", "IdAplicativo", c => c.Int());
            AlterColumn("dbo.ActivacionAplicativo", "EstadoAplicativo", c => c.Boolean());
            AlterColumn("dbo.ActivacionAplicativo", "UserId", c => c.Guid());
            CreateIndex("dbo.ActivacionAplicativo", "IdAplicativo");
            CreateIndex("dbo.ActivacionAplicativo", "UserId");
            AddForeignKey("dbo.ActivacionAplicativo", "IdAplicativo", "dbo.Aplicativo", "Id");
            AddForeignKey("dbo.ActivacionAplicativo", "UserId", "dbo.Users", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ActivacionAplicativo", "UserId", "dbo.Users");
            DropForeignKey("dbo.ActivacionAplicativo", "IdAplicativo", "dbo.Aplicativo");
            DropIndex("dbo.ActivacionAplicativo", new[] { "UserId" });
            DropIndex("dbo.ActivacionAplicativo", new[] { "IdAplicativo" });
            AlterColumn("dbo.ActivacionAplicativo", "UserId", c => c.Guid(nullable: false));
            AlterColumn("dbo.ActivacionAplicativo", "EstadoAplicativo", c => c.Boolean(nullable: false));
            AlterColumn("dbo.ActivacionAplicativo", "IdAplicativo", c => c.Int(nullable: false));
            DropColumn("dbo.ActivacionAplicativo", "EstadoAplicativoNombre");
            DropColumn("dbo.ActivacionAplicativo", "Aplicativonombre");
            CreateIndex("dbo.ActivacionAplicativo", "UserId");
            CreateIndex("dbo.ActivacionAplicativo", "IdAplicativo");
            AddForeignKey("dbo.ActivacionAplicativo", "UserId", "dbo.Users", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ActivacionAplicativo", "IdAplicativo", "dbo.Aplicativo", "Id", cascadeDelete: true);
        }
    }
}
