namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CREACIONtablaactivacion : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ActivacionAplicativo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdAplicativo = c.Int(nullable: false),
                        UsuarioAplicativo = c.String(),
                        EstadoAplicativo = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Aplicativo", t => t.IdAplicativo, cascadeDelete: true)
                .Index(t => t.IdAplicativo);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ActivacionAplicativo", "IdAplicativo", "dbo.Aplicativo");
            DropIndex("dbo.ActivacionAplicativo", new[] { "IdAplicativo" });
            DropTable("dbo.ActivacionAplicativo");
        }
    }
}
