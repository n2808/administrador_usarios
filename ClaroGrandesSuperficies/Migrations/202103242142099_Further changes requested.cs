namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Furtherchangesrequested : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TemporaryUserPasswords", "Login", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "StatusLogin", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "Extension", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "StatusExtension", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "Sac", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "PassSac", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "StatusSac", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "LinkSac", c => c.String());
            AddColumn("dbo.UserPasswords", "Login", c => c.String());
            AddColumn("dbo.UserPasswords", "StatusLogin", c => c.String());
            AddColumn("dbo.UserPasswords", "Extension", c => c.String());
            AddColumn("dbo.UserPasswords", "StatusExtension", c => c.String());
            AddColumn("dbo.UserPasswords", "Sac", c => c.String());
            AddColumn("dbo.UserPasswords", "PassSac", c => c.String());
            AddColumn("dbo.UserPasswords", "StatusSac", c => c.String());
            AddColumn("dbo.UserPasswords", "LinkSac", c => c.String());
            DropColumn("dbo.TemporaryUserPasswords", "UserGerencia");
            DropColumn("dbo.TemporaryUserPasswords", "PassGerencia");
            DropColumn("dbo.TemporaryUserPasswords", "StatusGerencia");
            DropColumn("dbo.TemporaryUserPasswords", "LinkGerencia");
            DropColumn("dbo.UserPasswords", "UserGerencia");
            DropColumn("dbo.UserPasswords", "PassGerencia");
            DropColumn("dbo.UserPasswords", "StatusGerencia");
            DropColumn("dbo.UserPasswords", "LinkGerencia");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserPasswords", "LinkGerencia", c => c.String());
            AddColumn("dbo.UserPasswords", "StatusGerencia", c => c.String());
            AddColumn("dbo.UserPasswords", "PassGerencia", c => c.String());
            AddColumn("dbo.UserPasswords", "UserGerencia", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "LinkGerencia", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "StatusGerencia", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "PassGerencia", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "UserGerencia", c => c.String());
            DropColumn("dbo.UserPasswords", "LinkSac");
            DropColumn("dbo.UserPasswords", "StatusSac");
            DropColumn("dbo.UserPasswords", "PassSac");
            DropColumn("dbo.UserPasswords", "Sac");
            DropColumn("dbo.UserPasswords", "StatusExtension");
            DropColumn("dbo.UserPasswords", "Extension");
            DropColumn("dbo.UserPasswords", "StatusLogin");
            DropColumn("dbo.UserPasswords", "Login");
            DropColumn("dbo.TemporaryUserPasswords", "LinkSac");
            DropColumn("dbo.TemporaryUserPasswords", "StatusSac");
            DropColumn("dbo.TemporaryUserPasswords", "PassSac");
            DropColumn("dbo.TemporaryUserPasswords", "Sac");
            DropColumn("dbo.TemporaryUserPasswords", "StatusExtension");
            DropColumn("dbo.TemporaryUserPasswords", "Extension");
            DropColumn("dbo.TemporaryUserPasswords", "StatusLogin");
            DropColumn("dbo.TemporaryUserPasswords", "Login");
        }
    }
}
