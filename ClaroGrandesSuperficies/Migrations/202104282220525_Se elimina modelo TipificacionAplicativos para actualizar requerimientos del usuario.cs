namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeeliminamodeloTipificacionAplicativosparaactualizarrequerimientosdelusuario : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TipificacionAplicativos", "AsignacionAplicativosUsuarioId", "dbo.AplicativosUsuario");
            DropForeignKey("dbo.TipificacionAplicativos", "TipificacionId", "dbo.Tipificaciones");
            DropIndex("dbo.TipificacionAplicativos", new[] { "TipificacionId" });
            DropIndex("dbo.TipificacionAplicativos", new[] { "AsignacionAplicativosUsuarioId" });
            DropTable("dbo.TipificacionAplicativos");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.TipificacionAplicativos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TipificacionId = c.Int(nullable: false),
                        AsignacionAplicativosUsuarioId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.TipificacionAplicativos", "AsignacionAplicativosUsuarioId");
            CreateIndex("dbo.TipificacionAplicativos", "TipificacionId");
            AddForeignKey("dbo.TipificacionAplicativos", "TipificacionId", "dbo.Tipificaciones", "Id", cascadeDelete: true);
            AddForeignKey("dbo.TipificacionAplicativos", "AsignacionAplicativosUsuarioId", "dbo.AplicativosUsuario", "Id", cascadeDelete: true);
        }
    }
}
