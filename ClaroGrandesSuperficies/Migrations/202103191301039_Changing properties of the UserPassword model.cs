namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangingpropertiesoftheUserPasswordmodel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserPasswords", "Name", c => c.String());
            AddColumn("dbo.UserPasswords", "Document", c => c.String());
            AddColumn("dbo.UserPasswords", "UserRed", c => c.String());
            AddColumn("dbo.UserPasswords", "UserAc", c => c.String());
            AddColumn("dbo.UserPasswords", "UserPoliedro", c => c.String());
            AddColumn("dbo.UserPasswords", "UserMyApps", c => c.String());
            AddColumn("dbo.UserPasswords", "UserAgendamiento", c => c.String());
            AddColumn("dbo.UserPasswords", "UserVisor", c => c.String());
            AddColumn("dbo.UserPasswords", "UserRr", c => c.String());
            AddColumn("dbo.UserPasswords", "UserMiEnlace", c => c.String());
            AddColumn("dbo.UserPasswords", "UserEnEnlaceIspira", c => c.String());
            AddColumn("dbo.UserPasswords", "UserMyIt", c => c.String());
            AddColumn("dbo.UserPasswords", "UserPortalSac", c => c.String());
            AddColumn("dbo.UserPasswords", "UserGerencia", c => c.String());
            AddColumn("dbo.UserPasswords", "UserDime", c => c.String());
            AddColumn("dbo.UserPasswords", "UserIdVision", c => c.String());
            DropColumn("dbo.UserPasswords", "Red");
            DropColumn("dbo.UserPasswords", "Ac");
            DropColumn("dbo.UserPasswords", "Poliedro");
            DropColumn("dbo.UserPasswords", "MyApps");
            DropColumn("dbo.UserPasswords", "Agendamiento");
            DropColumn("dbo.UserPasswords", "Visor");
            DropColumn("dbo.UserPasswords", "Rr");
            DropColumn("dbo.UserPasswords", "MiEnlace");
            DropColumn("dbo.UserPasswords", "EnEnlaceIspira");
            DropColumn("dbo.UserPasswords", "MyIt");
            DropColumn("dbo.UserPasswords", "PortalSac");
            DropColumn("dbo.UserPasswords", "Gerencia");
            DropColumn("dbo.UserPasswords", "Dime");
            DropColumn("dbo.UserPasswords", "IdVision");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserPasswords", "IdVision", c => c.String());
            AddColumn("dbo.UserPasswords", "Dime", c => c.String());
            AddColumn("dbo.UserPasswords", "Gerencia", c => c.String());
            AddColumn("dbo.UserPasswords", "PortalSac", c => c.String());
            AddColumn("dbo.UserPasswords", "MyIt", c => c.String());
            AddColumn("dbo.UserPasswords", "EnEnlaceIspira", c => c.String());
            AddColumn("dbo.UserPasswords", "MiEnlace", c => c.String());
            AddColumn("dbo.UserPasswords", "Rr", c => c.String());
            AddColumn("dbo.UserPasswords", "Visor", c => c.String());
            AddColumn("dbo.UserPasswords", "Agendamiento", c => c.String());
            AddColumn("dbo.UserPasswords", "MyApps", c => c.String());
            AddColumn("dbo.UserPasswords", "Poliedro", c => c.String());
            AddColumn("dbo.UserPasswords", "Ac", c => c.String());
            AddColumn("dbo.UserPasswords", "Red", c => c.String());
            DropColumn("dbo.UserPasswords", "UserIdVision");
            DropColumn("dbo.UserPasswords", "UserDime");
            DropColumn("dbo.UserPasswords", "UserGerencia");
            DropColumn("dbo.UserPasswords", "UserPortalSac");
            DropColumn("dbo.UserPasswords", "UserMyIt");
            DropColumn("dbo.UserPasswords", "UserEnEnlaceIspira");
            DropColumn("dbo.UserPasswords", "UserMiEnlace");
            DropColumn("dbo.UserPasswords", "UserRr");
            DropColumn("dbo.UserPasswords", "UserVisor");
            DropColumn("dbo.UserPasswords", "UserAgendamiento");
            DropColumn("dbo.UserPasswords", "UserMyApps");
            DropColumn("dbo.UserPasswords", "UserPoliedro");
            DropColumn("dbo.UserPasswords", "UserAc");
            DropColumn("dbo.UserPasswords", "UserRed");
            DropColumn("dbo.UserPasswords", "Document");
            DropColumn("dbo.UserPasswords", "Name");
        }
    }
}
