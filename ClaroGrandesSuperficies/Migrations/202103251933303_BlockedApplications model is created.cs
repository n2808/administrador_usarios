namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BlockedApplicationsmodeliscreated : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.GetTotalBlockedApplications", newName: "BlockedApplications");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.BlockedApplications", newName: "GetTotalBlockedApplications");
        }
    }
}
