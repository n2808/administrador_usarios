namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeagregamodeloTemporalEstadoTramite : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TemporalEstadoTramites",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Documento = c.String(),
                        NombreAplicativo = c.String(),
                        EstadoTramite = c.String(),
                        Observacion = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TemporalEstadoTramites");
        }
    }
}
