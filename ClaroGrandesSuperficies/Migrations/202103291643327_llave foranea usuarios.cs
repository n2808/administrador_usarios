namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class llaveforaneausuarios : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ActivacionAplicativo", "UserId", c => c.Guid(nullable: false));
            AddColumn("dbo.AplicativosUsuario", "UserId", c => c.Guid(nullable: false));
            CreateIndex("dbo.ActivacionAplicativo", "UserId");
            CreateIndex("dbo.AplicativosUsuario", "UserId");
            AddForeignKey("dbo.ActivacionAplicativo", "UserId", "dbo.Users", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AplicativosUsuario", "UserId", "dbo.Users", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AplicativosUsuario", "UserId", "dbo.Users");
            DropForeignKey("dbo.ActivacionAplicativo", "UserId", "dbo.Users");
            DropIndex("dbo.AplicativosUsuario", new[] { "UserId" });
            DropIndex("dbo.ActivacionAplicativo", new[] { "UserId" });
            DropColumn("dbo.AplicativosUsuario", "UserId");
            DropColumn("dbo.ActivacionAplicativo", "UserId");
        }
    }
}
