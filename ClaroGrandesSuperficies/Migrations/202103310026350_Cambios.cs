namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Cambios : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TemporalAsignacionAplicativosUsuario", "EstadoAplicativo", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TemporalAsignacionAplicativosUsuario", "EstadoAplicativo");
        }
    }
}
