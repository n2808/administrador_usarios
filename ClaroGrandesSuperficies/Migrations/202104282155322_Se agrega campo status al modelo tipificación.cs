namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Seagregacampostatusalmodelotipificación : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tipificaciones", "Status", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tipificaciones", "Status");
        }
    }
}
