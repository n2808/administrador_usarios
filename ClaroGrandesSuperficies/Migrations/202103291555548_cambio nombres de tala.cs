namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cambionombresdetala : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Aplicativoes", newName: "Aplicativo");
            RenameTable(name: "dbo.AsignacionAplicativosUsuarios", newName: "AplicativosUsuario");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.AplicativosUsuario", newName: "AsignacionAplicativosUsuarios");
            RenameTable(name: "dbo.Aplicativo", newName: "Aplicativoes");
        }
    }
}
