namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class clavetipovarybinary : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AplicativosUsuario", "ClaveCrifada", c => c.Binary(storeType: "image"));
            AddColumn("dbo.TemporalAsignacionAplicativosUsuario", "ClaveCrifada", c => c.Binary(storeType: "image"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TemporalAsignacionAplicativosUsuario", "ClaveCrifada");
            DropColumn("dbo.AplicativosUsuario", "ClaveCrifada");
        }
    }
}
