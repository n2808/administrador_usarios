namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeagregacampoEsFormacionalastablastemporales : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TemporalAsignacionAplicativosUsuario", "EsFormacion", c => c.String(maxLength: 10));
            AddColumn("dbo.TemporalDesbloqueoAplicativoUsuarios", "EsFormacion", c => c.String(maxLength: 10));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TemporalDesbloqueoAplicativoUsuarios", "EsFormacion");
            DropColumn("dbo.TemporalAsignacionAplicativosUsuario", "EsFormacion");
        }
    }
}
