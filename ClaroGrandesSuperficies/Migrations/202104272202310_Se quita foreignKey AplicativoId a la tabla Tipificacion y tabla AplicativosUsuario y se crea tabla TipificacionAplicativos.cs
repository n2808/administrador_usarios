namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SequitaforeignKeyAplicativoIdalatablaTipificacionytablaAplicativosUsuarioysecreatablaTipificacionAplicativos : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Tipificaciones", "AplicativoId", "dbo.Aplicativo");
            DropForeignKey("dbo.AplicativosUsuario", "TipificacionId", "dbo.Tipificaciones");
            DropIndex("dbo.AplicativosUsuario", new[] { "TipificacionId" });
            DropIndex("dbo.Tipificaciones", new[] { "AplicativoId" });
            CreateTable(
                "dbo.TipificacionAplicativos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TipificacionId = c.Int(nullable: false),
                        IdAplicativo = c.Int(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Aplicativo", t => t.IdAplicativo, cascadeDelete: true)
                .ForeignKey("dbo.Tipificaciones", t => t.TipificacionId, cascadeDelete: true)
                .Index(t => t.TipificacionId)
                .Index(t => t.IdAplicativo);
            
            DropColumn("dbo.AplicativosUsuario", "TipificacionId");
            DropColumn("dbo.Tipificaciones", "AplicativoId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tipificaciones", "AplicativoId", c => c.Int());
            AddColumn("dbo.AplicativosUsuario", "TipificacionId", c => c.Int());
            DropForeignKey("dbo.TipificacionAplicativos", "TipificacionId", "dbo.Tipificaciones");
            DropForeignKey("dbo.TipificacionAplicativos", "IdAplicativo", "dbo.Aplicativo");
            DropIndex("dbo.TipificacionAplicativos", new[] { "IdAplicativo" });
            DropIndex("dbo.TipificacionAplicativos", new[] { "TipificacionId" });
            DropTable("dbo.TipificacionAplicativos");
            CreateIndex("dbo.Tipificaciones", "AplicativoId");
            CreateIndex("dbo.AplicativosUsuario", "TipificacionId");
            AddForeignKey("dbo.AplicativosUsuario", "TipificacionId", "dbo.Tipificaciones", "Id");
            AddForeignKey("dbo.Tipificaciones", "AplicativoId", "dbo.Aplicativo", "Id");
        }
    }
}
