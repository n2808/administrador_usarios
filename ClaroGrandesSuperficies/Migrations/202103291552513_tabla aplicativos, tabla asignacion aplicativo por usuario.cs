namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tablaaplicativostablaasignacionaplicativoporusuario : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Aplicativoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NombreApp = c.String(),
                        Estado = c.Boolean(nullable: false),
                        Link = c.String(),
                        Observaciones = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AsignacionAplicativosUsuarios",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdAplicativo = c.Int(nullable: false),
                        UsuarioAplicativo = c.String(),
                        ClaveAplicativo = c.String(),
                        EstadoAplicativo = c.Boolean(nullable: false),
                        FechaReporteBloqueoApp = c.DateTime(nullable: false),
                        FechaDesbloqueoApp = c.DateTime(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Aplicativoes", t => t.IdAplicativo, cascadeDelete: true)
                .Index(t => t.IdAplicativo);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AsignacionAplicativosUsuarios", "IdAplicativo", "dbo.Aplicativoes");
            DropIndex("dbo.AsignacionAplicativosUsuarios", new[] { "IdAplicativo" });
            DropTable("dbo.AsignacionAplicativosUsuarios");
            DropTable("dbo.Aplicativoes");
        }
    }
}
