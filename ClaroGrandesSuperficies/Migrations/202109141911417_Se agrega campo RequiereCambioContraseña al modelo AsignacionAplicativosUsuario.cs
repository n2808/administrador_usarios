namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeagregacampoRequiereCambioContraseñaalmodeloAsignacionAplicativosUsuario : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AplicativosUsuario", "RequiereCambioContraseña", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AplicativosUsuario", "RequiereCambioContraseña");
        }
    }
}
