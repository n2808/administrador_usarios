namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fechasopcionalestablaasignacionaplicativos : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AplicativosUsuario", "FechaReporteBloqueoApp", c => c.DateTime());
            AlterColumn("dbo.AplicativosUsuario", "FechaDesbloqueoApp", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AplicativosUsuario", "FechaDesbloqueoApp", c => c.DateTime(nullable: false));
            AlterColumn("dbo.AplicativosUsuario", "FechaReporteBloqueoApp", c => c.DateTime(nullable: false));
        }
    }
}
