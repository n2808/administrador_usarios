namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeagregacampoPerteneceAlSegmento : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AplicativosUsuario", "PerteneceAlSegmento", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AplicativosUsuario", "PerteneceAlSegmento");
        }
    }
}
