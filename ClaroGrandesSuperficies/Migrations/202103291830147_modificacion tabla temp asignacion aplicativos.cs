namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modificaciontablatempasignacionaplicativos : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TemporalAsignacionAplicativosUsuario", "AplicativoUsuario_Id", "dbo.Aplicativo");
            DropIndex("dbo.TemporalAsignacionAplicativosUsuario", new[] { "AplicativoUsuario_Id" });
            AddColumn("dbo.TemporalAsignacionAplicativosUsuario", "NombreAplicativo", c => c.String());
            AddColumn("dbo.TemporalAsignacionAplicativosUsuario", "UserId", c => c.Guid(nullable: false));
            CreateIndex("dbo.TemporalAsignacionAplicativosUsuario", "UserId");
            AddForeignKey("dbo.TemporalAsignacionAplicativosUsuario", "UserId", "dbo.Users", "Id", cascadeDelete: true);
            DropColumn("dbo.TemporalAsignacionAplicativosUsuario", "IdAplicativo");
            DropColumn("dbo.TemporalAsignacionAplicativosUsuario", "AplicativoUsuario_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TemporalAsignacionAplicativosUsuario", "AplicativoUsuario_Id", c => c.Int());
            AddColumn("dbo.TemporalAsignacionAplicativosUsuario", "IdAplicativo", c => c.Int(nullable: false));
            DropForeignKey("dbo.TemporalAsignacionAplicativosUsuario", "UserId", "dbo.Users");
            DropIndex("dbo.TemporalAsignacionAplicativosUsuario", new[] { "UserId" });
            DropColumn("dbo.TemporalAsignacionAplicativosUsuario", "UserId");
            DropColumn("dbo.TemporalAsignacionAplicativosUsuario", "NombreAplicativo");
            CreateIndex("dbo.TemporalAsignacionAplicativosUsuario", "AplicativoUsuario_Id");
            AddForeignKey("dbo.TemporalAsignacionAplicativosUsuario", "AplicativoUsuario_Id", "dbo.Aplicativo", "Id");
        }
    }
}
