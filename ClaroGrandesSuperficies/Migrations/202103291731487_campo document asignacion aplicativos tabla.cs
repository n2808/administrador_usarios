namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class campodocumentasignacionaplicativostabla : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AplicativosUsuario", "Documento", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AplicativosUsuario", "Documento");
        }
    }
}
