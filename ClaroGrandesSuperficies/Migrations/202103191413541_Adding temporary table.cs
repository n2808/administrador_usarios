namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addingtemporarytable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TemporaryUserPasswords",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Document = c.String(),
                        UserRed = c.String(),
                        PassRed = c.String(),
                        UserAc = c.String(),
                        PassAc = c.String(),
                        UserPoliedro = c.String(),
                        PassPoliedro = c.String(),
                        UserMyApps = c.String(),
                        PassMyApps = c.String(),
                        UserAgendamiento = c.String(),
                        PassAgendamiento = c.String(),
                        UserVisor = c.String(),
                        PassVisor = c.String(),
                        UserRr = c.String(),
                        PassRr = c.String(),
                        UserMiEnlace = c.String(),
                        PassMiEnlace = c.String(),
                        UserEnEnlaceIspira = c.String(),
                        PassEnEnlaceIspira = c.String(),
                        UserMyIt = c.String(),
                        PassMyIt = c.String(),
                        UserPortalSac = c.String(),
                        PassPortalSac = c.String(),
                        UserGerencia = c.String(),
                        PassGerencia = c.String(),
                        UserDime = c.String(),
                        PassDime = c.String(),
                        UserIdVision = c.String(),
                        PassIdVision = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.UserPasswords", "Name");
            DropColumn("dbo.UserPasswords", "Document");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserPasswords", "Document", c => c.String());
            AddColumn("dbo.UserPasswords", "Name", c => c.String());
            DropTable("dbo.TemporaryUserPasswords");
        }
    }
}
