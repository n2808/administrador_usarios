namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fieldsLogmodelisdeleted : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Logs", "BrowserName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Logs", "BrowserName", c => c.String());
        }
    }
}
