namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GetTotalBlockedApplicationsmodeliscreated : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GetTotalBlockedApplications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AppName = c.String(),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.GetTotalBlockedApplications");
        }
    }
}
