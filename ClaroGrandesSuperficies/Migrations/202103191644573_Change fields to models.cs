namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Changefieldstomodels : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TemporaryUserPasswords", "StatusRed", c => c.String());
            AlterColumn("dbo.TemporaryUserPasswords", "StatusAc", c => c.String());
            AlterColumn("dbo.TemporaryUserPasswords", "StatusPoliedro", c => c.String());
            AlterColumn("dbo.TemporaryUserPasswords", "StatusMyApps", c => c.String());
            AlterColumn("dbo.TemporaryUserPasswords", "StatusAgendamiento", c => c.String());
            AlterColumn("dbo.TemporaryUserPasswords", "StatusVisor", c => c.String());
            AlterColumn("dbo.TemporaryUserPasswords", "StatusRr", c => c.String());
            AlterColumn("dbo.TemporaryUserPasswords", "StatusMiEnlace", c => c.String());
            AlterColumn("dbo.TemporaryUserPasswords", "StatusEnEnlaceIspira", c => c.String());
            AlterColumn("dbo.TemporaryUserPasswords", "StatusIt", c => c.String());
            AlterColumn("dbo.TemporaryUserPasswords", "StatusPortalSac", c => c.String());
            AlterColumn("dbo.TemporaryUserPasswords", "StatusGerencia", c => c.String());
            AlterColumn("dbo.TemporaryUserPasswords", "StatusDime", c => c.String());
            AlterColumn("dbo.TemporaryUserPasswords", "StatusIdVision", c => c.String());
            AlterColumn("dbo.UserPasswords", "StatusRed", c => c.String());
            AlterColumn("dbo.UserPasswords", "StatusAc", c => c.String());
            AlterColumn("dbo.UserPasswords", "StatusPoliedro", c => c.String());
            AlterColumn("dbo.UserPasswords", "StatusMyApps", c => c.String());
            AlterColumn("dbo.UserPasswords", "StatusAgendamiento", c => c.String());
            AlterColumn("dbo.UserPasswords", "StatusVisor", c => c.String());
            AlterColumn("dbo.UserPasswords", "StatusRr", c => c.String());
            AlterColumn("dbo.UserPasswords", "StatusMiEnlace", c => c.String());
            AlterColumn("dbo.UserPasswords", "StatusEnEnlaceIspira", c => c.String());
            AlterColumn("dbo.UserPasswords", "StatusIt", c => c.String());
            AlterColumn("dbo.UserPasswords", "StatusPortalSac", c => c.String());
            AlterColumn("dbo.UserPasswords", "StatusGerencia", c => c.String());
            AlterColumn("dbo.UserPasswords", "StatusDime", c => c.String());
            AlterColumn("dbo.UserPasswords", "StatusIdVision", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserPasswords", "StatusIdVision", c => c.Boolean(nullable: false));
            AlterColumn("dbo.UserPasswords", "StatusDime", c => c.Boolean(nullable: false));
            AlterColumn("dbo.UserPasswords", "StatusGerencia", c => c.Boolean(nullable: false));
            AlterColumn("dbo.UserPasswords", "StatusPortalSac", c => c.Boolean(nullable: false));
            AlterColumn("dbo.UserPasswords", "StatusIt", c => c.Boolean(nullable: false));
            AlterColumn("dbo.UserPasswords", "StatusEnEnlaceIspira", c => c.Boolean(nullable: false));
            AlterColumn("dbo.UserPasswords", "StatusMiEnlace", c => c.Boolean(nullable: false));
            AlterColumn("dbo.UserPasswords", "StatusRr", c => c.Boolean(nullable: false));
            AlterColumn("dbo.UserPasswords", "StatusVisor", c => c.Boolean(nullable: false));
            AlterColumn("dbo.UserPasswords", "StatusAgendamiento", c => c.Boolean(nullable: false));
            AlterColumn("dbo.UserPasswords", "StatusMyApps", c => c.Boolean(nullable: false));
            AlterColumn("dbo.UserPasswords", "StatusPoliedro", c => c.Boolean(nullable: false));
            AlterColumn("dbo.UserPasswords", "StatusAc", c => c.Boolean(nullable: false));
            AlterColumn("dbo.UserPasswords", "StatusRed", c => c.Boolean(nullable: false));
            AlterColumn("dbo.TemporaryUserPasswords", "StatusIdVision", c => c.Boolean(nullable: false));
            AlterColumn("dbo.TemporaryUserPasswords", "StatusDime", c => c.Boolean(nullable: false));
            AlterColumn("dbo.TemporaryUserPasswords", "StatusGerencia", c => c.Boolean(nullable: false));
            AlterColumn("dbo.TemporaryUserPasswords", "StatusPortalSac", c => c.Boolean(nullable: false));
            AlterColumn("dbo.TemporaryUserPasswords", "StatusIt", c => c.Boolean(nullable: false));
            AlterColumn("dbo.TemporaryUserPasswords", "StatusEnEnlaceIspira", c => c.Boolean(nullable: false));
            AlterColumn("dbo.TemporaryUserPasswords", "StatusMiEnlace", c => c.Boolean(nullable: false));
            AlterColumn("dbo.TemporaryUserPasswords", "StatusRr", c => c.Boolean(nullable: false));
            AlterColumn("dbo.TemporaryUserPasswords", "StatusVisor", c => c.Boolean(nullable: false));
            AlterColumn("dbo.TemporaryUserPasswords", "StatusAgendamiento", c => c.Boolean(nullable: false));
            AlterColumn("dbo.TemporaryUserPasswords", "StatusMyApps", c => c.Boolean(nullable: false));
            AlterColumn("dbo.TemporaryUserPasswords", "StatusPoliedro", c => c.Boolean(nullable: false));
            AlterColumn("dbo.TemporaryUserPasswords", "StatusAc", c => c.Boolean(nullable: false));
            AlterColumn("dbo.TemporaryUserPasswords", "StatusRed", c => c.Boolean(nullable: false));
        }
    }
}
