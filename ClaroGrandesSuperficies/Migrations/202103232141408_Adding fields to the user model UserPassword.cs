namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingfieldstotheusermodelUserPassword : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TemporaryUserPasswords", "UserHelpDesk", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "PassHelpDesk", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "StatusHelpDesk", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "LinkHelpDesk", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "UserTeams", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "PassTeams", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "StatusTeams", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "LinkTeams", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "UserCifin", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "PassCifin", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "StatusCifin", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "LinkCifin", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "UserEvidenteMaster", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "PassEvidenteMaster", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "StatusEvidenteMaster", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "LinkEvidenteMaster", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "UserIntegracion", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "PassIntegracion", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "StatusIntegracion", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "LinkIntegracion", c => c.String());
            AddColumn("dbo.UserPasswords", "UserHelpDesk", c => c.String());
            AddColumn("dbo.UserPasswords", "PassHelpDesk", c => c.String());
            AddColumn("dbo.UserPasswords", "StatusHelpDesk", c => c.String());
            AddColumn("dbo.UserPasswords", "LinkHelpDesk", c => c.String());
            AddColumn("dbo.UserPasswords", "UserTeams", c => c.String());
            AddColumn("dbo.UserPasswords", "PassTeams", c => c.String());
            AddColumn("dbo.UserPasswords", "StatusTeams", c => c.String());
            AddColumn("dbo.UserPasswords", "LinkTeams", c => c.String());
            AddColumn("dbo.UserPasswords", "UserCifin", c => c.String());
            AddColumn("dbo.UserPasswords", "PassCifin", c => c.String());
            AddColumn("dbo.UserPasswords", "StatusCifin", c => c.String());
            AddColumn("dbo.UserPasswords", "LinkCifin", c => c.String());
            AddColumn("dbo.UserPasswords", "UserEvidenteMaster", c => c.String());
            AddColumn("dbo.UserPasswords", "PassEvidenteMaster", c => c.String());
            AddColumn("dbo.UserPasswords", "StatusEvidenteMaster", c => c.String());
            AddColumn("dbo.UserPasswords", "LinkEvidenteMaster", c => c.String());
            AddColumn("dbo.UserPasswords", "UserIntegracion", c => c.String());
            AddColumn("dbo.UserPasswords", "PassIntegracion", c => c.String());
            AddColumn("dbo.UserPasswords", "StatusIntegracion", c => c.String());
            AddColumn("dbo.UserPasswords", "LinkIntegracion", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserPasswords", "LinkIntegracion");
            DropColumn("dbo.UserPasswords", "StatusIntegracion");
            DropColumn("dbo.UserPasswords", "PassIntegracion");
            DropColumn("dbo.UserPasswords", "UserIntegracion");
            DropColumn("dbo.UserPasswords", "LinkEvidenteMaster");
            DropColumn("dbo.UserPasswords", "StatusEvidenteMaster");
            DropColumn("dbo.UserPasswords", "PassEvidenteMaster");
            DropColumn("dbo.UserPasswords", "UserEvidenteMaster");
            DropColumn("dbo.UserPasswords", "LinkCifin");
            DropColumn("dbo.UserPasswords", "StatusCifin");
            DropColumn("dbo.UserPasswords", "PassCifin");
            DropColumn("dbo.UserPasswords", "UserCifin");
            DropColumn("dbo.UserPasswords", "LinkTeams");
            DropColumn("dbo.UserPasswords", "StatusTeams");
            DropColumn("dbo.UserPasswords", "PassTeams");
            DropColumn("dbo.UserPasswords", "UserTeams");
            DropColumn("dbo.UserPasswords", "LinkHelpDesk");
            DropColumn("dbo.UserPasswords", "StatusHelpDesk");
            DropColumn("dbo.UserPasswords", "PassHelpDesk");
            DropColumn("dbo.UserPasswords", "UserHelpDesk");
            DropColumn("dbo.TemporaryUserPasswords", "LinkIntegracion");
            DropColumn("dbo.TemporaryUserPasswords", "StatusIntegracion");
            DropColumn("dbo.TemporaryUserPasswords", "PassIntegracion");
            DropColumn("dbo.TemporaryUserPasswords", "UserIntegracion");
            DropColumn("dbo.TemporaryUserPasswords", "LinkEvidenteMaster");
            DropColumn("dbo.TemporaryUserPasswords", "StatusEvidenteMaster");
            DropColumn("dbo.TemporaryUserPasswords", "PassEvidenteMaster");
            DropColumn("dbo.TemporaryUserPasswords", "UserEvidenteMaster");
            DropColumn("dbo.TemporaryUserPasswords", "LinkCifin");
            DropColumn("dbo.TemporaryUserPasswords", "StatusCifin");
            DropColumn("dbo.TemporaryUserPasswords", "PassCifin");
            DropColumn("dbo.TemporaryUserPasswords", "UserCifin");
            DropColumn("dbo.TemporaryUserPasswords", "LinkTeams");
            DropColumn("dbo.TemporaryUserPasswords", "StatusTeams");
            DropColumn("dbo.TemporaryUserPasswords", "PassTeams");
            DropColumn("dbo.TemporaryUserPasswords", "UserTeams");
            DropColumn("dbo.TemporaryUserPasswords", "LinkHelpDesk");
            DropColumn("dbo.TemporaryUserPasswords", "StatusHelpDesk");
            DropColumn("dbo.TemporaryUserPasswords", "PassHelpDesk");
            DropColumn("dbo.TemporaryUserPasswords", "UserHelpDesk");
        }
    }
}
