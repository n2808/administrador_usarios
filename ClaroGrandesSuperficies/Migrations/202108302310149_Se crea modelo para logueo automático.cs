namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Secreamodeloparalogueoautomático : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LogueosAutomaticos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IpAddress = c.String(),
                        Machine = c.String(),
                        IdUser = c.Guid(nullable: false),
                        DocumentUser = c.String(),
                        DateOfEntry = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.LogueosAutomaticos");
        }
    }
}
