namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addingrelationshiptotheusermodel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserPasswords", "UserId", c => c.Guid(nullable: false));
            CreateIndex("dbo.UserPasswords", "UserId");
            AddForeignKey("dbo.UserPasswords", "UserId", "dbo.Users", "Id", cascadeDelete: true);
            DropColumn("dbo.UserPasswords", "Document");
            DropColumn("dbo.UserPasswords", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserPasswords", "Name", c => c.String());
            AddColumn("dbo.UserPasswords", "Document", c => c.String());
            DropForeignKey("dbo.UserPasswords", "UserId", "dbo.Users");
            DropIndex("dbo.UserPasswords", new[] { "UserId" });
            DropColumn("dbo.UserPasswords", "UserId");
        }
    }
}
