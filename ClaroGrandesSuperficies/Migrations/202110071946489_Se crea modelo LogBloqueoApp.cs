namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SecreamodeloLogBloqueoApp : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LogBloqueoApp",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Documento = c.String(),
                        FechaReporteBloqueoApp = c.DateTime(),
                        UserId = c.Guid(nullable: false),
                        IdAplicativo = c.Int(nullable: false),
                        TipificacionId = c.Int(),
                        EstadoAplicativo = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Aplicativo", t => t.IdAplicativo, cascadeDelete: true)
                .ForeignKey("dbo.Tipificaciones", t => t.TipificacionId)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.IdAplicativo)
                .Index(t => t.TipificacionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LogBloqueoApp", "UserId", "dbo.Users");
            DropForeignKey("dbo.LogBloqueoApp", "TipificacionId", "dbo.Tipificaciones");
            DropForeignKey("dbo.LogBloqueoApp", "IdAplicativo", "dbo.Aplicativo");
            DropIndex("dbo.LogBloqueoApp", new[] { "TipificacionId" });
            DropIndex("dbo.LogBloqueoApp", new[] { "IdAplicativo" });
            DropIndex("dbo.LogBloqueoApp", new[] { "UserId" });
            DropTable("dbo.LogBloqueoApp");
        }
    }
}
