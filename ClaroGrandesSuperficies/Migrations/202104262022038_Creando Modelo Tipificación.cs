namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreandoModeloTipificación : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tipificaciones",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.AplicativosUsuario", "TipificacionId", c => c.Int());
            CreateIndex("dbo.AplicativosUsuario", "TipificacionId");
            AddForeignKey("dbo.AplicativosUsuario", "TipificacionId", "dbo.Tipificaciones", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AplicativosUsuario", "TipificacionId", "dbo.Tipificaciones");
            DropIndex("dbo.AplicativosUsuario", new[] { "TipificacionId" });
            DropColumn("dbo.AplicativosUsuario", "TipificacionId");
            DropTable("dbo.Tipificaciones");
        }
    }
}
