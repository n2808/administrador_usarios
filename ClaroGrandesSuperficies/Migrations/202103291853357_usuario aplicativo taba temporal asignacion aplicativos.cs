namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class usuarioaplicativotabatemporalasignacionaplicativos : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TemporalAsignacionAplicativosUsuario", "UsuarioAplicativo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TemporalAsignacionAplicativosUsuario", "UsuarioAplicativo");
        }
    }
}
