namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addingfieldstomodels : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UserPasswords", "UserId", "dbo.Users");
            DropIndex("dbo.UserPasswords", new[] { "UserId" });
            AddColumn("dbo.TemporaryUserPasswords", "StatusRed", c => c.Boolean(nullable: false));
            AddColumn("dbo.TemporaryUserPasswords", "LinkRed", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "StatusAc", c => c.Boolean(nullable: false));
            AddColumn("dbo.TemporaryUserPasswords", "LinkAc", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "StatusPoliedro", c => c.Boolean(nullable: false));
            AddColumn("dbo.TemporaryUserPasswords", "LinkPoliedro", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "StatusMyApps", c => c.Boolean(nullable: false));
            AddColumn("dbo.TemporaryUserPasswords", "LinkMyApps", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "StatusAgendamiento", c => c.Boolean(nullable: false));
            AddColumn("dbo.TemporaryUserPasswords", "LinkAgendamiento", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "StatusVisor", c => c.Boolean(nullable: false));
            AddColumn("dbo.TemporaryUserPasswords", "LinkVisor", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "StatusRr", c => c.Boolean(nullable: false));
            AddColumn("dbo.TemporaryUserPasswords", "LinkRr", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "StatusMiEnlace", c => c.Boolean(nullable: false));
            AddColumn("dbo.TemporaryUserPasswords", "LinkMiEnlace", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "StatusEnEnlaceIspira", c => c.Boolean(nullable: false));
            AddColumn("dbo.TemporaryUserPasswords", "LinkEnEnlaceIspira", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "StatusIt", c => c.Boolean(nullable: false));
            AddColumn("dbo.TemporaryUserPasswords", "LinkIt", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "StatusPortalSac", c => c.Boolean(nullable: false));
            AddColumn("dbo.TemporaryUserPasswords", "LinkPortalSac", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "StatusGerencia", c => c.Boolean(nullable: false));
            AddColumn("dbo.TemporaryUserPasswords", "LinkGerencia", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "StatusDime", c => c.Boolean(nullable: false));
            AddColumn("dbo.TemporaryUserPasswords", "LinkDime", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "StatusIdVision", c => c.Boolean(nullable: false));
            AddColumn("dbo.TemporaryUserPasswords", "LinkIdVision", c => c.String());
            AddColumn("dbo.UserPasswords", "Name", c => c.String());
            AddColumn("dbo.UserPasswords", "Document", c => c.String());
            AddColumn("dbo.UserPasswords", "StatusRed", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserPasswords", "LinkRed", c => c.String());
            AddColumn("dbo.UserPasswords", "StatusAc", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserPasswords", "LinkAc", c => c.String());
            AddColumn("dbo.UserPasswords", "StatusPoliedro", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserPasswords", "LinkPoliedro", c => c.String());
            AddColumn("dbo.UserPasswords", "StatusMyApps", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserPasswords", "LinkMyApps", c => c.String());
            AddColumn("dbo.UserPasswords", "StatusAgendamiento", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserPasswords", "LinkAgendamiento", c => c.String());
            AddColumn("dbo.UserPasswords", "StatusVisor", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserPasswords", "LinkVisor", c => c.String());
            AddColumn("dbo.UserPasswords", "StatusRr", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserPasswords", "LinkRr", c => c.String());
            AddColumn("dbo.UserPasswords", "StatusMiEnlace", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserPasswords", "LinkMiEnlace", c => c.String());
            AddColumn("dbo.UserPasswords", "StatusEnEnlaceIspira", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserPasswords", "LinkEnEnlaceIspira", c => c.String());
            AddColumn("dbo.UserPasswords", "StatusIt", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserPasswords", "LinkIt", c => c.String());
            AddColumn("dbo.UserPasswords", "StatusPortalSac", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserPasswords", "LinkPortalSac", c => c.String());
            AddColumn("dbo.UserPasswords", "StatusGerencia", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserPasswords", "LinkGerencia", c => c.String());
            AddColumn("dbo.UserPasswords", "StatusDime", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserPasswords", "LinkDime", c => c.String());
            AddColumn("dbo.UserPasswords", "StatusIdVision", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserPasswords", "LinkIdVision", c => c.String());
            DropColumn("dbo.UserPasswords", "UserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserPasswords", "UserId", c => c.Guid(nullable: false));
            DropColumn("dbo.UserPasswords", "LinkIdVision");
            DropColumn("dbo.UserPasswords", "StatusIdVision");
            DropColumn("dbo.UserPasswords", "LinkDime");
            DropColumn("dbo.UserPasswords", "StatusDime");
            DropColumn("dbo.UserPasswords", "LinkGerencia");
            DropColumn("dbo.UserPasswords", "StatusGerencia");
            DropColumn("dbo.UserPasswords", "LinkPortalSac");
            DropColumn("dbo.UserPasswords", "StatusPortalSac");
            DropColumn("dbo.UserPasswords", "LinkIt");
            DropColumn("dbo.UserPasswords", "StatusIt");
            DropColumn("dbo.UserPasswords", "LinkEnEnlaceIspira");
            DropColumn("dbo.UserPasswords", "StatusEnEnlaceIspira");
            DropColumn("dbo.UserPasswords", "LinkMiEnlace");
            DropColumn("dbo.UserPasswords", "StatusMiEnlace");
            DropColumn("dbo.UserPasswords", "LinkRr");
            DropColumn("dbo.UserPasswords", "StatusRr");
            DropColumn("dbo.UserPasswords", "LinkVisor");
            DropColumn("dbo.UserPasswords", "StatusVisor");
            DropColumn("dbo.UserPasswords", "LinkAgendamiento");
            DropColumn("dbo.UserPasswords", "StatusAgendamiento");
            DropColumn("dbo.UserPasswords", "LinkMyApps");
            DropColumn("dbo.UserPasswords", "StatusMyApps");
            DropColumn("dbo.UserPasswords", "LinkPoliedro");
            DropColumn("dbo.UserPasswords", "StatusPoliedro");
            DropColumn("dbo.UserPasswords", "LinkAc");
            DropColumn("dbo.UserPasswords", "StatusAc");
            DropColumn("dbo.UserPasswords", "LinkRed");
            DropColumn("dbo.UserPasswords", "StatusRed");
            DropColumn("dbo.UserPasswords", "Document");
            DropColumn("dbo.UserPasswords", "Name");
            DropColumn("dbo.TemporaryUserPasswords", "LinkIdVision");
            DropColumn("dbo.TemporaryUserPasswords", "StatusIdVision");
            DropColumn("dbo.TemporaryUserPasswords", "LinkDime");
            DropColumn("dbo.TemporaryUserPasswords", "StatusDime");
            DropColumn("dbo.TemporaryUserPasswords", "LinkGerencia");
            DropColumn("dbo.TemporaryUserPasswords", "StatusGerencia");
            DropColumn("dbo.TemporaryUserPasswords", "LinkPortalSac");
            DropColumn("dbo.TemporaryUserPasswords", "StatusPortalSac");
            DropColumn("dbo.TemporaryUserPasswords", "LinkIt");
            DropColumn("dbo.TemporaryUserPasswords", "StatusIt");
            DropColumn("dbo.TemporaryUserPasswords", "LinkEnEnlaceIspira");
            DropColumn("dbo.TemporaryUserPasswords", "StatusEnEnlaceIspira");
            DropColumn("dbo.TemporaryUserPasswords", "LinkMiEnlace");
            DropColumn("dbo.TemporaryUserPasswords", "StatusMiEnlace");
            DropColumn("dbo.TemporaryUserPasswords", "LinkRr");
            DropColumn("dbo.TemporaryUserPasswords", "StatusRr");
            DropColumn("dbo.TemporaryUserPasswords", "LinkVisor");
            DropColumn("dbo.TemporaryUserPasswords", "StatusVisor");
            DropColumn("dbo.TemporaryUserPasswords", "LinkAgendamiento");
            DropColumn("dbo.TemporaryUserPasswords", "StatusAgendamiento");
            DropColumn("dbo.TemporaryUserPasswords", "LinkMyApps");
            DropColumn("dbo.TemporaryUserPasswords", "StatusMyApps");
            DropColumn("dbo.TemporaryUserPasswords", "LinkPoliedro");
            DropColumn("dbo.TemporaryUserPasswords", "StatusPoliedro");
            DropColumn("dbo.TemporaryUserPasswords", "LinkAc");
            DropColumn("dbo.TemporaryUserPasswords", "StatusAc");
            DropColumn("dbo.TemporaryUserPasswords", "LinkRed");
            DropColumn("dbo.TemporaryUserPasswords", "StatusRed");
            CreateIndex("dbo.UserPasswords", "UserId");
            AddForeignKey("dbo.UserPasswords", "UserId", "dbo.Users", "Id", cascadeDelete: true);
        }
    }
}
