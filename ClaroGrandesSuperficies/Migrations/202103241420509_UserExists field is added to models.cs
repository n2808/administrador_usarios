namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserExistsfieldisaddedtomodels : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TemporaryUserPasswords", "UserExists", c => c.Boolean());
            AddColumn("dbo.UserPasswords", "UserExists", c => c.Boolean());
            AddColumn("dbo.Users", "UserExists", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "UserExists");
            DropColumn("dbo.UserPasswords", "UserExists");
            DropColumn("dbo.TemporaryUserPasswords", "UserExists");
        }
    }
}
