namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Furtherchangesrequested_2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TemporaryUserPasswords", "UserSac", c => c.String());
            AddColumn("dbo.UserPasswords", "UserSac", c => c.String());
            DropColumn("dbo.TemporaryUserPasswords", "Sac");
            DropColumn("dbo.UserPasswords", "Sac");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserPasswords", "Sac", c => c.String());
            AddColumn("dbo.TemporaryUserPasswords", "Sac", c => c.String());
            DropColumn("dbo.UserPasswords", "UserSac");
            DropColumn("dbo.TemporaryUserPasswords", "UserSac");
        }
    }
}
