namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seremueveidusuariotemp : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TemporalAsignacionAplicativosUsuario", "UserId", "dbo.Users");
            DropIndex("dbo.TemporalAsignacionAplicativosUsuario", new[] { "UserId" });
            DropColumn("dbo.TemporalAsignacionAplicativosUsuario", "UserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TemporalAsignacionAplicativosUsuario", "UserId", c => c.Guid(nullable: false));
            CreateIndex("dbo.TemporalAsignacionAplicativosUsuario", "UserId");
            AddForeignKey("dbo.TemporalAsignacionAplicativosUsuario", "UserId", "dbo.Users", "Id", cascadeDelete: true);
        }
    }
}
