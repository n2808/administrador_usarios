// <auto-generated />
namespace ClaroGrandesSuperficies.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class SeagregamodeloEstadoTramiteyseagregancamposalatablaAplicativosUsuario : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(SeagregamodeloEstadoTramiteyseagregancamposalatablaAplicativosUsuario));
        
        string IMigrationMetadata.Id
        {
            get { return "202108271632190_Se agrega modelo EstadoTramite y se agregan campos a la tabla AplicativosUsuario"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
