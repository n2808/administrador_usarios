namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Seagreganmodelosparaasignarlecampañasalosaplicativos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Campanas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Estado = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CampanasAplicativo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AplicativoId = c.Int(nullable: false),
                        CampanaId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Aplicativo", t => t.AplicativoId, cascadeDelete: true)
                .ForeignKey("dbo.Campanas", t => t.CampanaId, cascadeDelete: false)
                .Index(t => t.AplicativoId)
                .Index(t => t.CampanaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CampanasAplicativo", "CampanaId", "dbo.Campanas");
            DropForeignKey("dbo.CampanasAplicativo", "AplicativoId", "dbo.Aplicativo");
            DropIndex("dbo.CampanasAplicativo", new[] { "CampanaId" });
            DropIndex("dbo.CampanasAplicativo", new[] { "AplicativoId" });
            DropTable("dbo.CampanasAplicativo");
            DropTable("dbo.Campanas");
        }
    }
}
