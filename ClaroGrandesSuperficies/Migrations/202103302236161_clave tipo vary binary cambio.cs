namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class clavetipovarybinarycambio : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AplicativosUsuario", "ClaveCrifada", c => c.Binary());
            AlterColumn("dbo.TemporalAsignacionAplicativosUsuario", "ClaveCrifada", c => c.Binary());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TemporalAsignacionAplicativosUsuario", "ClaveCrifada", c => c.Binary(storeType: "image"));
            AlterColumn("dbo.AplicativosUsuario", "ClaveCrifada", c => c.Binary(storeType: "image"));
        }
    }
}
