namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeagregaforeignKeyAplicativoIdalatablaTipificacion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tipificaciones", "AplicativoId", c => c.Int());
            CreateIndex("dbo.Tipificaciones", "AplicativoId");
            AddForeignKey("dbo.Tipificaciones", "AplicativoId", "dbo.Aplicativo", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tipificaciones", "AplicativoId", "dbo.Aplicativo");
            DropIndex("dbo.Tipificaciones", new[] { "AplicativoId" });
            DropColumn("dbo.Tipificaciones", "AplicativoId");
        }
    }
}
