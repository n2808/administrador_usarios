namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeagregacolumnaparacalculartotaldebloqueosporApp : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LogBloqueoApp", "TotalBloqueos", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.LogBloqueoApp", "TotalBloqueos");
        }
    }
}
