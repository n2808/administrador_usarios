namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class claveparacifrarclavesenlastablasaplicativoytemporales : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ActivacionAplicativo", "ClaveCrifada", c => c.Binary(maxLength: 8000));
            AddColumn("dbo.TemporalDesbloqueoAplicativoUsuarios", "ClaveCrifada", c => c.Binary(maxLength: 8000));
            AlterColumn("dbo.AplicativosUsuario", "ClaveCrifada", c => c.Binary(maxLength: 8000));
            AlterColumn("dbo.TemporalAsignacionAplicativosUsuario", "ClaveCrifada", c => c.Binary(maxLength: 8000));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TemporalAsignacionAplicativosUsuario", "ClaveCrifada", c => c.Binary());
            AlterColumn("dbo.AplicativosUsuario", "ClaveCrifada", c => c.Binary());
            DropColumn("dbo.TemporalDesbloqueoAplicativoUsuarios", "ClaveCrifada");
            DropColumn("dbo.ActivacionAplicativo", "ClaveCrifada");
        }
    }
}
