namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeagregamodeloEstadoTramiteyseagregancamposalatablaAplicativosUsuario : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EstadoTramites",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Status = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.AplicativosUsuario", "Observacion", c => c.String());
            AddColumn("dbo.AplicativosUsuario", "FechaEstadoTramite", c => c.DateTime());
            AddColumn("dbo.AplicativosUsuario", "EstadoTramiteId", c => c.Int());
            CreateIndex("dbo.AplicativosUsuario", "EstadoTramiteId");
            AddForeignKey("dbo.AplicativosUsuario", "EstadoTramiteId", "dbo.EstadoTramites", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AplicativosUsuario", "EstadoTramiteId", "dbo.EstadoTramites");
            DropIndex("dbo.AplicativosUsuario", new[] { "EstadoTramiteId" });
            DropColumn("dbo.AplicativosUsuario", "EstadoTramiteId");
            DropColumn("dbo.AplicativosUsuario", "FechaEstadoTramite");
            DropColumn("dbo.AplicativosUsuario", "Observacion");
            DropTable("dbo.EstadoTramites");
        }
    }
}
