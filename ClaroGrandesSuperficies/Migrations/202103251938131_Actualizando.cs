namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Actualizando : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.BlockedApplications");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.BlockedApplications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AppName = c.String(),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
    }
}
