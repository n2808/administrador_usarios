namespace ClaroGrandesSuperficies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SequitaforeignKeyAplicativoIdalatablaTipificacionAplicativosyseagregaforeignkeyAsignacionAplicativosUsuarioId : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TipificacionAplicativos", "IdAplicativo", "dbo.Aplicativo");
            DropIndex("dbo.TipificacionAplicativos", new[] { "IdAplicativo" });
            AddColumn("dbo.TipificacionAplicativos", "AsignacionAplicativosUsuarioId", c => c.Int(nullable: false));
            CreateIndex("dbo.TipificacionAplicativos", "AsignacionAplicativosUsuarioId");
            AddForeignKey("dbo.TipificacionAplicativos", "AsignacionAplicativosUsuarioId", "dbo.AplicativosUsuario", "Id", cascadeDelete: true);
            DropColumn("dbo.TipificacionAplicativos", "IdAplicativo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TipificacionAplicativos", "IdAplicativo", c => c.Int(nullable: false));
            DropForeignKey("dbo.TipificacionAplicativos", "AsignacionAplicativosUsuarioId", "dbo.AplicativosUsuario");
            DropIndex("dbo.TipificacionAplicativos", new[] { "AsignacionAplicativosUsuarioId" });
            DropColumn("dbo.TipificacionAplicativos", "AsignacionAplicativosUsuarioId");
            CreateIndex("dbo.TipificacionAplicativos", "IdAplicativo");
            AddForeignKey("dbo.TipificacionAplicativos", "IdAplicativo", "dbo.Aplicativo", "Id", cascadeDelete: true);
        }
    }
}
