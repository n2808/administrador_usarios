﻿using Core.Models.Common;
using Core.Models.configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.User
{
	public class UserRol : Entity
	{
		public Guid RolId { get; set; }
		public Guid UserId { get; set; }

		[ForeignKey("RolId")]
		public Rol Rol  { get; set; }

		[ForeignKey("UserId")]
		public User User { get; set; }
	}
}