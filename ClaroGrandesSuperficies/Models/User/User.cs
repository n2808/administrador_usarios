﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;



namespace Core.Models.User
{
    public class User : Entity
	{
        [DisplayName("Documento")]
		public string Document { get; set; }
        [DisplayName("Nombres")]
        public string Names { get; set; }
        [DisplayName("Apellidos")]
        public string LastName { get; set; }
        [DisplayName("Telefono 1")]
        public string Phone1 { get; set; }
      
        [DisplayName("Correo")]
        public string Email { get; set; }
        [DisplayName("Estado")]
        public bool Status { get; set; }
        public bool? UserExists { get; set; }

        [DisplayName("password")]
        public string PassWord { get; set; }

        public string Campaign { get; set; }  
        
        public DateTime? LoginDate { get; set; }       
        
        public List<UserRol> UserRol { get; set; }
        

    }
}