﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClaroGrandesSuperficies.Models.AdministracionUsuariosApp
{
    [Table("LogueosAutomaticos")]
    public class LogueoAutomatico 
    {
        public LogueoAutomatico()
        {
            DateOfEntry = DateTime.Now;
        }

        [Key]
        public int Id { get; set; }
        public string IpAddress { get; set; }
        public string Machine { get; set; }
        public Guid IdUser { get; set; }
        public string DocumentUser { get; set; }
        public DateTime? DateOfEntry { get; set; }
    }
}