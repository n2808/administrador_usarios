﻿using Core.Models.Common;

namespace ClaroGrandesSuperficies.Models.AdministracionUsuariosApp
{
    public class EstadoTramite : EntityWithIntId
    {
        public string Nombre { get; set; }
        public bool Status { get; set; } = true;
    }
}