﻿using Core.Models.Common;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClaroGrandesSuperficies.Models.AdministracionUsuariosApp
{
    [Table("CampanasAplicativo")]
    public class CampanaAplicativo : EntityWithIntId
    {
        public int AplicativoId { get; set; }
        public int CampanaId { get; set; }

        [ForeignKey("AplicativoId")]
        public Aplicativo Aplicativo { get; set; }

        [ForeignKey("CampanaId")]
        public Campana Campana { get; set; }
    }
}