﻿using Core.Models.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClaroGrandesSuperficies.Models.AdministracionUsuariosApp
{

    [Table("TemporalDesbloqueoAplicativoUsuarios")]
    public class TemporalDesbloqueoAplicativoUsuarios : EntityWithIntId
    {
        public string Documento { get; set; }

        public string NombreAplicativo { get; set; }

        public string UsuarioAplicativo { get; set; }

        public string ClaveAplicativo { get; set; }

        [MaxLength(10)]
        public string EsFormacion { get; set; }

        [MaxLength(8000)]
        public byte[] ClaveCrifada { get; set; }

    }
}