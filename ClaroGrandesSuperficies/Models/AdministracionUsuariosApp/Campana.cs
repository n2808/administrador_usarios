﻿using Core.Models.Common;

namespace ClaroGrandesSuperficies.Models.AdministracionUsuariosApp
{
    public class Campana : EntityWithIntId
    {
        public string Nombre { get; set; }

        public bool Estado { get; set; }

    }
}