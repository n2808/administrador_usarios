﻿using Core.Models.Common;
using Core.Models.User;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClaroGrandesSuperficies.Models.AdministracionUsuariosApp
{
    [Table("AplicativosUsuario")]
    public class AsignacionAplicativosUsuario : EntityWithIntId
    {
  
        public int IdAplicativo { get; set; }
        public int? TipificacionId { get; set; }
        public string Documento { get; set; }
        public string UsuarioAplicativo { get; set; }
        public string ClaveAplicativo { get; set; }
        public bool EstadoAplicativo { get; set; }
        public bool? RequiereCambioContraseña { get; set; }
        public string Observacion { get; set; }
        public bool? PerteneceAlSegmento { get; set; }
        public DateTime? FechaReporteBloqueoApp { get; set; }
        public DateTime? FechaEstadoTramite { get; set; }
        public DateTime? FechaDesbloqueoApp { get; set; }
        public Guid UserId { get; set; }
        public int? EstadoTramiteId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        [MaxLength(8000)]
        public byte[] ClaveCrifada { get; set; }

        [ForeignKey("IdAplicativo")]
        public Aplicativo AplicativoUsuario { get; set; }

        [ForeignKey("TipificacionId")]
        public Tipificacion Tipificacion { get; set; }

        [ForeignKey("EstadoTramiteId")]
        public EstadoTramite EstadoTramite { get; set; }

    }
}