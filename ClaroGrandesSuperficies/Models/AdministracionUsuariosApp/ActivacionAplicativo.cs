﻿using Core.Models.Common;
using Core.Models.User;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClaroGrandesSuperficies.Models.AdministracionUsuariosApp
{
    [Table("ActivacionAplicativo")]
    public class ActivacionAplicativo: EntityWithIntId
    {
        public int? IdAplicativo { get; set; }

        public string Aplicativonombre { get; set; }

        [ForeignKey("IdAplicativo")]
        public Aplicativo AplicativoUsuario { get; set; }

        public string UsuarioAplicativo { get; set; }

        public string  EstadoAplicativoNombre { get; set; }
        public bool? EstadoAplicativo { get; set; }

        public Guid? UserId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }


        [MaxLength(8000)]
        public byte[] ClaveCrifada { get; set; }

    }
}