﻿using Core.Models.Common;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClaroGrandesSuperficies.Models.AdministracionUsuariosApp
{
    [Table("TemporalEstadoTramites")]
    public class TemporalEstadoTramite : EntityWithIntId
    {
        public string Documento { get; set; }
        public string NombreAplicativo { get; set; }
        public string EstadoTramite { get; set; }
        public string Observacion { get; set; }
     
    }
}