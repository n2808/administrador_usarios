﻿using Core.Models.Common;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClaroGrandesSuperficies.Models.AdministracionUsuariosApp
{

    [Table("Aplicativo")]
    public class Aplicativo : EntityWithIntId
    {

        public string NombreApp { get; set; }

        public bool Estado { get; set; }

        public string Link { get; set; }

        public string Observaciones { get; set; }
    }
}