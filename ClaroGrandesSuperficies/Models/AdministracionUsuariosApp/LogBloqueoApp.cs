﻿using Core.Models.Common;
using Core.Models.User;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClaroGrandesSuperficies.Models.AdministracionUsuariosApp
{
    [Table("LogBloqueoApp")]
    public class LogBloqueoApp : EntityWithIntId
    {
  
        public string Documento { get; set; }        
        public DateTime? FechaReporteBloqueoApp { get; set; }
        public int? TotalBloqueos { get; set; } = 0;
        public Guid UserId { get; set; }
        public int IdAplicativo { get; set; }
        public int? TipificacionId { get; set; }
        public bool EstadoAplicativo { get; set; }
     

        [ForeignKey("UserId")]
        public User User { get; set; }     

        [ForeignKey("IdAplicativo")]
        public Aplicativo AplicativoUsuario { get; set; }

        [ForeignKey("TipificacionId")]
        public Tipificacion Tipificacion { get; set; }

       

    }
}