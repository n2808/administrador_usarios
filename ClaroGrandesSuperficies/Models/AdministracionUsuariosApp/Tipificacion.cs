﻿using Core.Models.Common;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClaroGrandesSuperficies.Models.AdministracionUsuariosApp
{
    [Table("Tipificaciones")]
    public class Tipificacion : EntityWithIntId
    {
        public string Name { get; set; }
        public bool Status { get; set; } = true;

    }
}