﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace cifrado_CypherHelper
{
    public class CypherHelper
    {

        // Con este método ciframos el texto que introduzca el usuario y lo devolvemos
        public static String CifrarTexto(String texto, byte[] clave)
        {
            // Instanciamos el Rijndael
            Rijndael cifradoRijn = Rijndael.Create();
            byte[] encriptado = null;
            byte[] salida = null;
            try
            {
                // Asignamos en la clase Rijndael la clave introducida por el usuario
                cifradoRijn.Key = clave;
                // Generamos un vector de inicialización
                cifradoRijn.GenerateIV();
                // Convertimos el texto introducido a un array de bytes
                byte[] entrada = Encoding.UTF8.GetBytes(texto);
                // Encriptamos el mensaje
                encriptado = cifradoRijn.CreateEncryptor().TransformFinalBlock(entrada,
                0, entrada.Length);
                // Inicializamos un array de bytes con la longitud del mensaje 
                //encriptado y la longitud del vector de inicialización
              salida = new byte[cifradoRijn.IV.Length + encriptado.Length];
                // Copiamos el vector al principio del array salida
                cifradoRijn.IV.CopyTo(salida, 0);
                // Copiamos el mensaje encriptado en el array salida después del vector
                encriptado.CopyTo(salida, cifradoRijn.IV.Length);
            }
            catch (Exception)
            {
                throw new Exception("Error al cifrar los datos.");
            }
            finally
            {
                // Limpiamos el Rijndael
                cifradoRijn.Dispose();
                cifradoRijn.Clear();
            }
            // Convertimos el array de bytes salida a String
            String resultado = Encoding.Default.GetString(salida);
            // Devolvemos el resultado
            return resultado;
        }


        //Con este método usamos el mensaje cifrado y la clave introducida para descirar el mensaje
        public static String DescifrarTexto(byte[] entrada, byte[] clave)
        {
            // Instanciamos el Rijndael
            Rijndael cifradoRijn = Rijndael.Create();
            // Inicializamos un array con la longitud del vector de inicialización
            byte[] arrayTemporal = new byte[cifradoRijn.IV.Length];
            // Inicializamos un array que tendrá la longitud del mensaje encriptado
            byte[] encriptado = new byte[entrada.Length - cifradoRijn.IV.Length];
            String textodescifrado = String.Empty;
            try
            {
                // Asignamos la clave
                cifradoRijn.Key = clave;
                // Copiamos en el array temporal el vector de inicialización
                Array.Copy(entrada, arrayTemporal, arrayTemporal.Length);
                // Copiamos el mensaje sin el vector de inicialización en un array
                Array.Copy(entrada, arrayTemporal.Length, encriptado, 0,
                encriptado.Length);
                // Asignamos el vector de inicialización
                cifradoRijn.IV = arrayTemporal;
                // Desencriptamos el mensaje
                byte[] prueba =
                cifradoRijn.CreateDecryptor().TransformFinalBlock(encriptado, 0,
                encriptado.Length);
                // Convertimos el mensaje descifrado a String
                textodescifrado = Encoding.UTF8.GetString(prueba);
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
            }
            finally
            {
                // Limpiamos el Rijndael
                cifradoRijn.Dispose();
                cifradoRijn.Clear();
            }
            // Devolvemos el mensaje descifrado
            return textodescifrado;
        }

    }
}