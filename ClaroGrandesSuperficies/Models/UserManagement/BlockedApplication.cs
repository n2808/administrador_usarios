﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClaroGrandesSuperficies.Models.UserManagement
{
    public class BlockedApplication
    {
        public int Id { get; set; }
        public string AppName { get; set; }
        public int Quantity { get; set; }
    }
}