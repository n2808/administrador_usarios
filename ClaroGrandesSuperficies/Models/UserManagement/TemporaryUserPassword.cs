﻿using Core.Models.Common;
using Core.Models.User;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClaroGrandesSuperficies.Models.UserManagement
{
    public class TemporaryUserPassword : EntityWithIntId
    {
        public string Name { get; set; }
        public string Document { get; set; }
        public bool? UserExists { get; set; }
        public string UserRed { get; set; }
        public string PassRed { get; set; }
        public string StatusRed { get; set; }
        public string LinkRed { get; set; }
        public string Login { get; set; }
        public string StatusLogin { get; set; }
        public string Extension { get; set; }
        public string StatusExtension { get; set; }
        public string UserSac { get; set; }
        public string PassSac { get; set; }
        public string StatusSac { get; set; }
        public string LinkSac { get; set; }
        public string UserAc { get; set; }
        public string PassAc { get; set; }
        public string StatusAc { get; set; }
        public string LinkAc { get; set; }
        public string UserPoliedro { get; set; }
        public string PassPoliedro { get; set; }
        public string StatusPoliedro { get; set; }
        public string LinkPoliedro { get; set; }
        public string UserMyApps { get; set; }
        public string PassMyApps { get; set; }
        public string StatusMyApps { get; set; }
        public string LinkMyApps { get; set; }
        public string UserAgendamiento { get; set; }
        public string PassAgendamiento { get; set; }
        public string StatusAgendamiento { get; set; }
        public string LinkAgendamiento { get; set; }
        public string UserVisor { get; set; }
        public string PassVisor { get; set; }
        public string StatusVisor { get; set; }
        public string LinkVisor { get; set; }
        public string UserRr { get; set; }
        public string PassRr { get; set; }
        public string StatusRr { get; set; }
        public string LinkRr { get; set; }
        public string UserMiEnlace { get; set; }
        public string PassMiEnlace { get; set; }
        public string StatusMiEnlace { get; set; }
        public string LinkMiEnlace { get; set; }
        public string UserEnEnlaceIspira { get; set; }
        public string PassEnEnlaceIspira { get; set; }
        public string StatusEnEnlaceIspira { get; set; }
        public string LinkEnEnlaceIspira { get; set; }
        public string UserMyIt { get; set; }
        public string PassMyIt { get; set; }
        public string StatusIt { get; set; }
        public string LinkIt { get; set; }
        public string UserPortalSac { get; set; }
        public string PassPortalSac { get; set; }
        public string StatusPortalSac { get; set; }
        public string LinkPortalSac { get; set; }     
        public string UserDime { get; set; }
        public string PassDime { get; set; }
        public string StatusDime { get; set; }
        public string LinkDime { get; set; }
        public string UserIdVision { get; set; }
        public string PassIdVision { get; set; }
        public string StatusIdVision { get; set; }
        public string LinkIdVision { get; set; }
        public string UserHelpDesk { get; set; }
        public string PassHelpDesk { get; set; }
        public string StatusHelpDesk { get; set; }
        public string LinkHelpDesk { get; set; }
        public string UserTeams { get; set; }
        public string PassTeams { get; set; }
        public string StatusTeams { get; set; }
        public string LinkTeams { get; set; }
        public string UserCifin { get; set; }
        public string PassCifin { get; set; }
        public string StatusCifin { get; set; }
        public string LinkCifin { get; set; }
        public string UserEvidenteMaster { get; set; }
        public string PassEvidenteMaster { get; set; }
        public string StatusEvidenteMaster { get; set; }
        public string LinkEvidenteMaster { get; set; }
        public string UserIntegracion { get; set; }
        public string PassIntegracion { get; set; }
        public string StatusIntegracion { get; set; }
        public string LinkIntegracion { get; set; }

    }
}