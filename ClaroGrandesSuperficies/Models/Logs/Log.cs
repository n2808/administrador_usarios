﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ClaroGrandesSuperficies.Models.Logs
{
    public class Log
    {
        public Log()
        {
            DateOfEntry = DateTime.Now;
        }

        [Key]
        public int Id { get; set; }
        public string IpAddress { get; set; }    
        public string Machine { get; set; }
        public string DocumentUser { get; set; }
        public DateTime? DateOfEntry { get; set; }
      
    }
}