﻿using ClaroGrandesSuperficies.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Core.Models.Security
{
	public class AccountsModel
	{
        private ContextModel db = new ContextModel();

        private List<Accounts> ListAccounts = new List<Accounts>();


		public Accounts find(Guid? Id = null)
		{
			var Users = db.Users.Find(Id);
			string[] Roles = db.UserRols.Where(c => c.User.Id == Users.Id).Select(c => (string)c.Rol.Name).ToArray();
			Accounts UserAccount = new Accounts
			{
				UserName = Users.Names + " " + Users.LastName,
				Roles = Roles,
				Id = Users.Id
			};
			return UserAccount;
		}


		public Accounts Login(string UserName, string Password)
        {
			var Users = db.Users.Where(c => c.Document.Trim() == UserName.Trim() && c.PassWord.Trim() == Password.Trim()).FirstOrDefault();

			if (Users != null) {
				string[] Roles = db.UserRols.Where(c => c.User.Id == Users.Id).Select(c => (string)c.Rol.Name).ToArray();           

                Accounts UserAccount = new Accounts
				{
					UserName = Users.Names + " " + Users.LastName
					,
					Roles = Roles
					,
					Id = Users.Id
				};
				return UserAccount;
			}
			{
				return null; 
			}
			
		}
	}
} 