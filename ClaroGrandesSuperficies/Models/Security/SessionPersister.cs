﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Core.Models.Security
{
	public static class SessionPersister
	{
		static string UserNameSessionVar = "UserName";
		static string UserIdSessionVar = "Id";
		static string UserPointOfCareSessionVar = "PointOfCare";
		static string CurrentCareSessionVar = "CurrentPointOfCare";
        
        public static bool HasRol(string Role)
        {
            if(Id != null)
            {
                AccountsModel am = new AccountsModel();
                CustomPrincipal mp = new CustomPrincipal(am.find(Id));
                return mp.IsInRole(Role);
            }
            else
            {
                return false;
            }
        }


        public static string UserName {
			get {
                HttpContext.Current.Session.Timeout = 24 * 60;
                if (HttpContext.Current == null)
					return string.Empty;
					var sessionvar = HttpContext.Current.Session[UserNameSessionVar];
					if (sessionvar != null)
						return sessionvar as string;
					return null;
				}
			set {
                HttpContext.Current.Session.Timeout = 24 * 60;
                HttpContext.Current.Session[UserNameSessionVar] = value;
			}
		}




		public static Guid? Id
		{
			get
			{
                HttpContext.Current.Session.Timeout = 24 * 60;
                if (HttpContext.Current == null)
					return null;
				var sessionvar = HttpContext.Current.Session[UserIdSessionVar];
				if (sessionvar != null)
					return sessionvar as Guid?;
				return null;
			}
			set
			{
                HttpContext.Current.Session.Timeout = 24 * 60;
                HttpContext.Current.Session["Id"] = value;
			}
		}

        public static Guid[] PointOfCare
        {
            get
            {
                HttpContext.Current.Session.Timeout = 24 * 60;
                if (HttpContext.Current == null)
                    return null;
                var sessionvar = HttpContext.Current.Session[UserPointOfCareSessionVar];
                if (sessionvar != null)
                    return sessionvar as Guid[];
                return null;
            }
            set
            {
                HttpContext.Current.Session.Timeout = 24 * 60;
                HttpContext.Current.Session[UserPointOfCareSessionVar] = value;
            }
        }


        public static Guid? CurrentPointOfCare
        {
            get
            {
                HttpContext.Current.Session.Timeout = 24 * 60;
                if (HttpContext.Current == null)
                    return null;
                var sessionvar = HttpContext.Current.Session[CurrentCareSessionVar];
                if (sessionvar != null)
                    return sessionvar as Guid?;
                return null;
            }
            set
            {
                HttpContext.Current.Session.Timeout = 24 * 60;
                HttpContext.Current.Session[CurrentCareSessionVar] = value;
            }
        }



    }

}
