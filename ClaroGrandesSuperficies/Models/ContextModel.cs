﻿namespace ClaroGrandesSuperficies.Models
{
    using ClaroGrandesSuperficies.Models.AdministracionUsuariosApp;
    using ClaroGrandesSuperficies.Models.Logs;
    using ClaroGrandesSuperficies.Models.UserManagement;
    using Core.Models.configuration;
    using Core.Models.location;
    using Core.Models.User;
    using System.Data.Entity;

    public class ContextModel : DbContext
    {
        // El contexto se ha configurado para usar una cadena de conexión 'ContextModel' del archivo 
        // de configuración de la aplicación (App.config o Web.config). De forma predeterminada, 
        // esta cadena de conexión tiene como destino la base de datos 'ClaroGrandesSuperficies.Models.ContextModel' de la instancia LocalDb. 
        // 
        // Si desea tener como destino una base de datos y/o un proveedor de base de datos diferente, 
        // modifique la cadena de conexión 'ContextModel'  en el archivo de configuración de la aplicación.
        public ContextModel()
            : base("name=DefaultConnection")
        {
        }

        // Agregue un DbSet para cada tipo de entidad que desee incluir en el modelo. Para obtener más información 
        // sobre cómo configurar y usar un modelo Code First, vea http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }                            


        #region LOCATIONS
        public DbSet<Country> Countries { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<City> Cities { get; set; }      
       

        #endregion


        #region CONFIGURATIONS
        public DbSet<Category> Categories { get; set; }
        public DbSet<Configuration> Configurations { get; set; }
        #endregion

        #region USERS
        public DbSet<User> Users { get; set; }
        public DbSet<Rol> Rols { get; set; }
        public DbSet<UserRol> UserRols { get; set; }
        #endregion


        #region UserManagement
        public DbSet<UserPassword> UserPasswords { get; set; }
        public DbSet<TemporaryUserPassword> TemporaryUserPasswords { get; set; }
        #endregion

        #region Log
        public DbSet<Log> Logs { get; set; }

        #endregion


        #region AdministracionUsuarios

        public DbSet<Aplicativo> Aplicativos { get; set; }

        public DbSet<AsignacionAplicativosUsuario> AsignacionAplicativos { get; set; }
        public DbSet<LogBloqueoApp> LogBloqueoApps { get; set; }

        public DbSet<TemporalAsignacionAplicativosUsuario> TemporalAsignacionAplicativosUsuarios { get; set; }

        public DbSet<TemporalDesbloqueoAplicativoUsuarios> TemporalDesbloqueoUsuarios { get; set; }
        public DbSet<TemporalEstadoTramite> TemporalEstadoTramites { get; set; }

        public DbSet<ActivacionAplicativo> ActivacionAplicativo { get; set; }
        public DbSet<Tipificacion> Tipificaciones { get; set; }
        public DbSet<LogueoAutomatico> LogueoAutomaticos { get; set; }
        public DbSet<Campana> Campanas { get; set; }
        public DbSet<CampanaAplicativo> CampanasAplicativo { get; set; }


        #endregion
    }

}