﻿using System;

namespace ClaroGrandesSuperficies.Viewmodel
{
    public class StatusUserAppViewModel
    {
        public string Campaña { get; set; }
        public string Documento { get; set; }
        public string NombreCompleto { get; set; }
        public string EstadoAplicativo { get; set; }
        public string EstadoTrámite { get; set; }
        public string ObservaciónEstadoTrámite { get; set; }
        public string NombreAplicativo { get; set; }
        public string UsuarioAplicativo { get; set; }
        public string ObservacionesAplicativo { get; set; }
        public DateTime? FechaDeBloqueo { get; set; }
        public string Tipificación { get; set; }

    }
}