﻿namespace ClaroGrandesSuperficies.Viewmodel
{
    public class ActiveDirectoryFilterVO
    {
        /// <summary>
        /// Obtiene y/o establece el dominio del active directory.
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        /// Obtiene y/o establece el usuario del active directory.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Obtiene y/o establece la contraseña del active directory.
        /// </summary>
        public string Password { get; set; }
    }
}