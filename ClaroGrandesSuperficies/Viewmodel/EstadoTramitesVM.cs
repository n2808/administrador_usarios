﻿using System;

namespace ClaroGrandesSuperficies.Viewmodel
{
    public class EstadoTramitesVM
    {
        public string Documento { get; set; }
        public string Nombre { get; set; }
        public string NombreApp { get; set; }
        public string EstadoTramite { get; set; }
        public DateTime? FechaUltimoBloqueo { get; set; }
        public DateTime? FechaEstadoTramite { get; set; }
        public int DuracionTramite { get; set; }     
        public string Observacion { get; set; }

    }
}