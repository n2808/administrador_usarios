﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClaroGrandesSuperficies.Viewmodel
{
    public class UsuarioVM
    {

        public int Id { get; set; }

        public int? IdAplicativo { get; set; }

        public string UsuarioAplicativo { get; set; }
        public string NombreEstadoTramite { get; set; }

        public bool EstadoAplicativo { get; set; }

        public DateTime? FechaReporteBloqueoApp { get; set; }

        public DateTime? FechaDesbloqueoApp { get; set; }

        public DateTime? CreatedAt { get; set; }

        public DateTime? UpdatedAt { get; set; }

        public DateTime? DeletedAt { get; set; }

        public DateTime? CreatedBy { get; set; }

        public DateTime? UpdatedBy { get; set; }

        public string Documento { get; set; }

        public string ClaveAplicativo { get; set; }

        public string ClaveCifrada { get; set; }

        public string Names { get; set; }

        public string PassWord { get; set; }

        public bool Status { get; set; }

        public string NombreApp { get; set; }

        public string Link { get; set; }
        public string Campana { get; set; }
    }
}