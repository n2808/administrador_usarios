﻿namespace ClaroGrandesSuperficies.Viewmodel
{
    public class AplicativoVM
    {
        public string Usuario { get; set; }
        public string ClaveAplicativo { get; set; }
        public string Link { get; set; }
    }
}