﻿using ClaroGrandesSuperficies.Models.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClaroGrandesSuperficies.Viewmodel
{
    public class Mekano
    {

        public string Document { get; set; }
        public string Name { get; set; }
        public string Campaign { get; set; }
        public string DateReport { get; set; }
    }
}