﻿var OrderTypeId = null;
var value = null;
//Ocultando campos de fecha.
$(document).ready(function () {
    $('.containerLeasing').css('display', 'none');
    $('.containerPurchase').css('display', 'none');
});

// Ocultando campo fecha según tipo de orden seleccionado.
$('body').on('change', '#OrderTypeId', function () {

    var orderId = $(this).val();

    if (orderId == 'fb8953e8-d0ab-4433-948b-cad98b0b4083') {
        $('.containerLeasing').css('display', 'block');
        $('.containerPurchase').css('display', 'none');
        clearFields();

    } else {

        $('.containerPurchase').css('display', 'block');
        $('.containerLeasing').css('display', 'none');
        clearFields();
    }

});

// #region CREAR ORDEN LEASING

// Procedimiento para validar la creación de la orden de leasing.
$('body').on('click', '#createOrderLeasing', function () {

    // Definición de variables de entorno.
    var leasingId = $('#LeasingId').val();
    value = $('#Value').val();
    var contract = $('#Contract').val();
    //var addendum = $('#addendum').val();
    var startDate = $('#StartDate').val();
    var endDate = $('#EndDate').val();


    if (leasingId == "" || leasingId == undefined) {
        $('#errorLeasing').text('El campo proveedor es requerido').css('color', 'red');
        return false;
    }

    if (value <= 0) {
        $('#errorValue').text('El campo valor es requerido').css('color', 'red');
        return false;
    }
    if (contract == "" || contract == undefined) {
        $('#errorContract').text('El campo número de contrato es requerido').css('color', 'red');
        return false;
    }
    //if (addendum == "" || addendum == undefined) {
    //    $('#erroraddendum').text('El campo adenda es requerido').css('color', 'red');
    //    return false;
    //}
    if (startDate == "") {
        $('#errorStartDate').text('El campo fecha de inicio es requerido').css('color', 'red');
        return false;
    }
    if (endDate == "") {
        $('#errorEndDate').text('El campo fecha final es requerido').css('color', 'red');
        return false;
    }

});

// Ocultando alertas leasing
$('body').on('change', '#LeasingId', function () {
    $('#errorLeasing').text('');
});
$('body').on('keyup', '#Value', function () {
    $('#errorValue').text('');
});
$('body').on('keyup', '#Contract', function () {
    $('#errorContract').text('');
});
//$('body').on('keyup', '#addendum', function () {
//    $('#erroraddendum').text('');
//});
$('body').on('change', '#StartDate', function () {
    $('#errorStartDate').text('');
});
$('body').on('change', '#EndDate', function () {
    $('#errorEndDate').text('');
});

// #endregion


// #region CREAR ORDEN COMPRA

// Procedimiento para validar la creación de la orden de compra.
$('body').on('click', '#createOrderPurchase', function () {

    // Definición de variables de entorno.
    var supplierId = $('#SupplierId').val();
    var userAuthorizingId = $('#UserAuthorizingId').val();
    var dataOfPurchase = $('#DataOfPurchase').val();
    var purchaseOrderNumber = $('#PurchaseOrderNumber').val();
    var invoiceNumber = $('#InvoiceNumber').val();
    value = $('#ValuePurchase').val();
    $('#Value').val(value);


    if (supplierId == "" || supplierId == undefined) {
        $('#errorSupplier').text('El campo proveedor es requerido').css('color', 'red');
        return false;
    }
    if (userAuthorizingId == "" || userAuthorizingId == undefined) {
        $('#errorUserAuthorizingId').text('El campo quién autoriza es requerido').css('color', 'red');
        return false;
    }
    if (dataOfPurchase == "" || dataOfPurchase == undefined) {
        $('#errorDataOfPurchase').text('El campo fecha de compra es requerido').css('color', 'red');
        return false;
    }
    if (purchaseOrderNumber == "" || purchaseOrderNumber == undefined) {
        $('#errorPurchaseOrderNumber').text('El campo número de orden de compra es requerido').css('color', 'red');
        return false;
    }
    if (invoiceNumber == "" || invoiceNumber == undefined) {
        $('#errorInvoiceNumber').text('El campo número de factura es requerido').css('color', 'red');
        return false;
    }
    if (value <= 0) {
        $('#errorValuePurchase').text('El campo valor es requerido').css('color', 'red');
        return false;
    }

});

// Ocultando alertas compra
$('body').on('change', '#SupplierId', function () {
    $('#errorSupplier').text('');
});
$('body').on('change', '#UserAuthorizingId', function () {
    $('#errorUserAuthorizingId').text('');
});
$('body').on('change', '#DataOfPurchase', function () {
    $('#errorDataOfPurchase').text('');
});
$('body').on('keyup', '#PurchaseOrderNumber', function () {
    $('#errorPurchaseOrderNumber').text('');
});
$('body').on('keyup', '#InvoiceNumber', function () {
    $('#errorInvoiceNumber').text('');
});
$('body').on('change', '#ValuePurchase', function () {
    $('#errorValuePurchase').text('');
});

// #endregion

// Limpiar campos al cambiar de tipo de orden.
function clearFields() {
    // Campos de leasing.
    $('#Reference').val("");
    $('#LeasingId').val("");
    $('#Value').val("");
    $('#Contract').val("");
    $('#addendum').val("");
    $('#StartDate').val("");
    $('#EndDate').val("");

    // Campos de compra.
    $('#SupplierId').val("");
    $('#UserAuthorizingId').val("");
    $('#DataOfPurchase').val("");
    $('#PurchaseOrderNumber').val("");
    $('#InvoiceNumber').val("");
    $('#ValuePurchase').val("");
}

