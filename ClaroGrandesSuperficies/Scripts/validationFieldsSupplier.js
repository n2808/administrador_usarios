﻿$('body').on('click', '#btn_createSupplier', function () {

    // Variables de entorno.
    var name = $('#Name').val();
    var nit = $('#Nit').val();
    var city = $('#City').val();
    var phone = $('#Phone').val();
    var email = $('#Email').val();
    var address = $('#Address').val();



    if (name == "" || name == undefined) {
        $('#errorName').text('El campo nombre es requerido').css('color', 'red');
        return false;
    }
    if (nit == "" || nit == undefined) {
        $('#errorNit').text('El campo nit es requerido').css('color', 'red');
        return false;
    }
    if (city == "" || city == undefined) {
        $('#errorCity').text('El campo ciudad es requerido').css('color', 'red');
        return false;
    }
    if (phone == "" || phone == undefined) {
        $('#errorPhone').text('El campo teléfono es requerido').css('color', 'red');
        return false;
    }
    if (email == "" || email == undefined) {
        $('#errorEmail').text('El campo correo es requerido').css('color', 'red');
        return false;
    }
    if (address == "" || address == undefined) {
        $('#errorAddress').text('El campo dirección es requerido').css('color', 'red');
        return false;
    }


});

// Ocultando alertas
$('body').on('keydown', '#Name', function () {
    $('#errorName').text('');
});
$('body').on('keydown', '#Nit', function () {
    $('#errorNit').text('');
});
$('body').on('keydown', '#City', function () {
    $('#errorCity').text('');
});
$('body').on('keydown', '#Phone', function () {
    $('#errorPhone').text('');
});
$('body').on('keydown', '#Email', function () {
    $('#errorEmail').text('');
});
$('body').on('keydown', '#Address', function () {
    $('#errorAddress').text('');
});