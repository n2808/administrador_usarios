﻿$('body').on('change', '#endDate', function () {

    var startDate = $('#startDate').val();
    var endDate = $('#endDate').val();

    if (startDate == "") {
        alert("Debe seleccionar la fecha de Inicio");
        $('#endDate').val("");
        return false;
    }
    if (endDate == "") {
        alert("Debe seleccionar la fecha de Final");
        return false;
    }

    console.log(startDate);
    console.log(endDate);

    $.post(BaseUrl + "Reports/" + 'Outputs', { StartDate: startDate, EndDate: endDate })
        .done(function (Result) {

            console.log(Result.data);

            if (Result.status == 200) {

                //$("#exportExcel").append("<a href='/Tests/ExportExcel?StartDate=" + startDate + "&EndDate=" + endDate + "' id='downloadzip' download type='button' class='btn btn-danger ml-3'>Generar Archivo</a>");               
                var tablaCuerpo = $("#tbl_outputs tbody");
                tablaCuerpo.html("<tr style='background:#ccc;'>\
                                    <th>Fecha de Salida</th>\
                                    <th>Sede de Origen</th>\
                                    <th>Usuario</th>\
                                    <th>Documento</th>\
                                    <th>Dirección</th>\
                                    <th>Teléfono</th>\
                                    <th>Observación</th>\
                                    </tr >");


                for (var i = 0; i < Result.data.length; i++) {

                    var dateOutput = Result.data[i].Date == null ? "" : convertirFecha(Result.data[i].Date);
                    var pointOfOrigin = Result.data[i].PointOfCare == null ? "" : Result.data[i].PointOfCare.Name;
                    var name = Result.data[i].Names == null ? "" : Result.data[i].Names;
                    var document = Result.data[i].Document == null ? "" : Result.data[i].Document;
                    var address = Result.data[i].Address == null ? "" : Result.data[i].Address;
                    var phone = Result.data[i].Phone == null ? "" : Result.data[i].Phone;
                    var observation = Result.data[i].Observation == null ? "N/A" : Result.data[i].Observation;


                    var fila = "<tr>\
                                <td>"+ dateOutput + "</td>\
                                <td>"+ pointOfOrigin + "</td>\
                                <td>"+ name + "</td>\
                                <td>"+ document + "</td>\
                                <td>"+ address + "</td>\
                                <td>"+ phone + "</td>\
                                <td>"+ observation + "</td>\
                                </tr>";

                    tablaCuerpo.append(fila);


                }

            } else if (Result.status == 404) {
                Swal.fire({
                    icon: 'info',
                    title: Result.message,
                    showConfirmButton: false,
                    timer: 1500
                });
            } else {
                Swal.fire({
                    icon: 'error',
                    title: Result.message,
                    showConfirmButton: false,
                    timer: 1500
                });
            }

        });

});


$('body').on('click', '#btn_chat', function () {

    var ID = $(this).val();

    if (ID == "" || ID == undefined) {
        alert("Intente de nuevo");
        return false;
    }
    console.log(ID);

    window.location.href = '/Tests/' + 'Alls/' + ID;
});


function convertirFecha(fecha) {
    var fechaString = (fecha).substr(6);
    var fechaActual = new Date(parseInt(fechaString));
    var mes = fechaActual.getMonth() + 1;
    var dia = fechaActual.getDate();
    var anio = fechaActual.getFullYear();
    var fecha = "" + anio + "/" + mes + "/" + dia;

    return fecha;

}