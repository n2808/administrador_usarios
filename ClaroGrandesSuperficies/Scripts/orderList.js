﻿$(document).ready(function () {
    $('.content_purchase').css('display', 'none');
    $('.content_leasing').css('display', 'none');

});

$('body').on('click', '#btn_search', function () {

    var order = $('#txt_nombre').val();


    $.post('/OrderIncomes/' + 'Orderlist', { txt_nombre: order })
        .done(function (Result) {

            console.log(Result)
                      

            if (Result.status == 200) {

                if (order === 'FB8953E8-D0AB-4433-948B-CAD98B0B4083') {
                    $('.content_leasing').css('display', 'block');

                    var tablaCuerpo = $("#tbl_leasing tbody");
                    tablaCuerpo.html("<tr style='background:#ccc;'>\
                                        <th>Proveedor</th>\
                                        <th>Valor</th>\
                                        <th>Número de Contrato</th>\
                                        <th>Adenda</th>\
                                        <th>Fecha de Inicio</th>\
                                        <th> Fecha Final</th>\
                                        <th>Acciones</th>\
                                        </tr>");

                    for (var i = 0; i < Result.data.length; i++) {

                        var startDate = convertirFecha(Result.data[i].StartDate);
                        var endDate = convertirFecha(Result.data[i].EndDate);

                        var fila = "<tr class='checkFila'>\
                                    <td>"+ Result.data[i].Supplier.Name + "</td>\
                                    <td>"+ Result.data[i].Value + "</td>\
                                    <td>"+ Result.data[i].Contract + "</td>\
                                    <td>"+ Result.data[i].addendum + "</td>\
                                    <td>"+ startDate + "</td>\
                                    <td>"+ endDate + "</td>\
                                    <td></div><input type='button' id='" + Result.data[i].Id + "' data-value='" + Result.data[i].Id + "' class='btn btn-xs btn-success addProducts' value='Editar' />\
                                    <input type='button' id='" + Result.data[i].Id + "' data-value='" + Result.data[i].Id + "' class='btn btn-xs btn-danger deleteOrder' value='Eliminar' /></td>\
                                    </tr>";

                        tablaCuerpo.append(fila);

                    }


                } else if (order === '164D910D-F0CA-4FAA-AF9C-78CD571ED4A5') {
                    $('.content_purchase').css('display', 'block');

                    var tablaCuerpo = $("#tbl_purchase tbody");
                    tablaCuerpo.html("<tr style='background:#ccc;'>\
                                                    <th>Proveedor</th>\
                                                    <th>Usuario que Autoriza</th>\
                                                    <th>Fecha de Compra</th>\
                                                    <th> Número de Orden de Compra</th>\
                                                    <th> Número de Factura</th>\
                                                    <th> Valor</th>\
                                                    <th>Garantía</th>\
                                                    <th>Acciones</th>\
                                                    </tr>");



                    for (var i = 0; i < Result.data.length; i++) {

                        var supplier = Result.data[i].Supplier == null ? "" : Result.data[i].Supplier.Name;
                        var userAuthorizing = Result.data[i].UserAuthorizing == null ? "" : Result.data[i].UserAuthorizing.Names;
                        var dataOfPurchase = convertirFecha(Result.data[i].DataOfPurchase);


                        var fila = "<tr class='checkFila'>\
                                                <td>"+ supplier + "</td>\
                                                <td>"+ userAuthorizing + "</td>\
                                                <td>"+ dataOfPurchase + "</td>\
                                                <td>"+ Result.data[i].PurchaseOrderNumber + "</td>\
                                                <td>"+ Result.data[i].InvoiceNumber + "</td>\
                                                <td>"+ Result.data[i].Value + "</td>\
                                                <td>"+ Result.data[i].Guarantee + "</td>\
                                                <td></div><input type='button' id='" + Result.data[i].Id + "' data-value='" + Result.data[i].Id + "' class='btn btn-xs btn-success addProducts' value='Editar' /></div>\
                                                <input type='button' id='" + Result.data[i].Id + "' data-value='" + Result.data[i].Id + "' class='btn btn-xs btn-danger deleteOrder' value='Eliminar' /></td>\
                                                </tr>";

                        tablaCuerpo.append(fila);

                    }
                }

            } else {

                Swal.fire({
                    icon: 'info',
                    title: 'La orden seleccionada no posee datos',
                    showConfirmButton: false,
                    timer: 1500
                })
                    .then(() => {

                        window.location.reload = true;

                    });
            }

        });

    if ($('#txt_nombre').val() == '164D910D-F0CA-4FAA-AF9C-78CD571ED4A5') {
        $('.content_purchase').css('display', 'block');
        $('.content_leasing').css('display', 'none');
    } else {
        $('.content_purchase').css('display', 'none');
        $('.content_leasing').css('display', 'block');
    }
});

$('body').on('click', '.addProducts', function () {

    var id = $(this).data('value');   

    window.location.href = '/OrderIncomes/' + 'Edit/' + id;

});


$('body').on('click', '.deleteOrder', function () {

    var orderId = $(this).data('value');

    $.post('/OrderIncomes/' + 'Delete', { id: orderId })
        .done(function (Result) {

            if (Result.status == 200) {
                Swal.fire({
                    icon: 'success',
                    title: Result.message,
                    showConfirmButton: false,
                    timer: 1500
                })
                    .then(() => {

                        location.reload(true);


                    });
            }
            if (Result.status == 404) {
                Swal.fire({
                    icon: 'info',
                    title: Result.message,
                    showConfirmButton: false,
                    timer: 1500
                })
                    .then(() => {

                        location.reload(true);

                    });
            }

            if (Result.status == 500) {
                Swal.fire({
                    icon: 'error',
                    title: Result.message,
                    showConfirmButton: false,
                    timer: 1500
                })
                    .then(() => {

                        location.reload(true);

                    });
            }

        });  

});

// Función para transformar fecha en formato json a un formato fecha.
function convertirFecha(fecha) {
    var fechaString = (fecha).substr(6);
    var fechaActual = new Date(parseInt(fechaString));
    var mes = fechaActual.getMonth() + 1;
    var dia = fechaActual.getDate();
    var anio = fechaActual.getFullYear();
    var fecha = "" + anio + "/" + mes + "/" + dia;

    return fecha;

}