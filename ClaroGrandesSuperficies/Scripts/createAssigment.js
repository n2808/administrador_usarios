﻿var data_Imagen = null;
$("body").on("click", "#AssigmentProduct", function () {
    $(".alert").remove();
    var s_elm = $(this);
    var product = $("#Inventory").val();
    if (product == 0) {
        s_elm.after(string_mensaje("por favor seleccione el producto"))
        return;
    }
    if ($("#fila_product_" + product).length > 0) {
        s_elm.after(string_mensaje("el producto ya existe"))
        return;
    }
    var name = $("#Inventory option:selected").text();
    var s_fila = '<tr data-id="' + product + '" data-name="' + name + '" id="fila_product_' + product + '">\
                <td>'+ name + '</td>\
                <td><button class="btn btn-danger btn_removerFila"><i class="glyphicon glyphicon-trash"></i></button></td>\
                </tr>';
    //_console(s_fila);
    $("#table_equiposAssigment tbody").append(s_fila);

});

// INSERT SALE INTO THE DATA
$("body").on("click", "#assigmentProducts", function () {
    $(".alert").remove();
    var s_elm = $(this);
    if ($("#table_equiposAssigment tbody tr").length == 0) {
        s_elm.after(string_mensaje("No se ha seleccionado ningun producto"))
        return;
    }

    if ($("#FloorId :selected").val() === "") {
        s_elm.after(string_mensaje("Seleccione el piso de la Sede"));
        return;
    }

    if ($("#DateOfAssignment").val() === "") {
        s_elm.after(string_mensaje("Seleccione la fecha de asignación"));
        return;
    }

    primerElementoG = 0;
    b_validacion = true;
    //este codigo hace la validacion
    $(".check_verificar").each(void_validaCampo);
    if (!b_validacion) { return false }

    var userId = $("#userId").val();
    var dateOfAssignment = $("#DateOfAssignment").val();
    var floorId = $("#FloorId").val();

    var _products = [];
    var fd = new FormData();
    $("#table_equiposAssigment tbody tr").each(function (ind, val) {
        var _id = $(this).data("id");
        _products.push(_id);
        fd.append('productoId', _id);
    });

  
    fd.append('Foto', $('#InvoiceImage')[0].files[0]);
    fd.append('UserId', userId);
    fd.append('DateOfAssignment', dateOfAssignment);
    fd.append('FloorId', floorId);
    fd.append('InvoiceImage', data_Imagen);


    $.ajax({
        url: BaseUrl + "Sales/" + 'CreateAssignment',
        type: 'POST',
        data: fd,
        contentType: false,
        processData: false,
    })
        .done((Result) => {

            void_RedirectLogin(Result.status, Result.message)
            if (Result.status == 200) {
                Swal.fire({
                    icon: 'success',
                    title: 'Registro creado correctamente',
                    showConfirmButton: false,
                    timer: 1500
                })
                    .then(() => {
                        window.location = BaseUrl + "Sales/Home"

                    });

            }
            else if (Result.status == 404) {
                if (typeof Result.products == Array) {
                    $.each(Result.status, function (ind, val) {
                        s_elm.after(string_mensaje("El Producto " + val.Serial + " no esta disponible"))
                    });
                }
            }
            else if (Result.status == 500) {
                s_elm.after(string_mensaje("No se ha podido registrar la salida"))
            }

            s_elm.attr('disabled', false);
        })
        .fail((Error) => {
            s_elm.after(string_mensaje("No se ha podido registrar la salida"))
        })
        .always(function () {
            s_elm.attr('disabled', false);
        })

})


// convertir imagen en data js.
document.getElementById('InvoiceImage').onchange = function (evt) {

    try {
        nombre = this.files[0].name;
    }
    catch (err) {
        document.getElementById('invoicePreview').removeAttribute("src");
        data_Imagen = null;
        return;
    }

    nombre = this.files[0].name;
    ImageTools.resize(this.files[0], {
        width: 1100, // maximum width
        height: 1000 // maximum height
    }, function (blob, didItResize) {
        // didItResize will be true if it managed to resize it, otherwise false (and will return the original file as 'blob')
        document.getElementById('invoicePreview').src = window.URL.createObjectURL(blob);
        // you can also now upload this blob using an XHR.

        var myFile = blobToFile(blob, nombre);

        function blobToFile(theBlob, fileName) {
            var b = theBlob;
            //A Blob() is almost a File() - it's just missing the two properties below which we will add
            b.name = fileName;
            // b.id3_get_frame_short_name(frameId) = $('#idor').val();
            b.lastModifiedDate = new Date();

            return b;
        }

        //var idor = $('#idor').val();
        convertirBlobAData(myFile);

        function convertirBlobAData(blob) {

            var reader = new FileReader();
            reader.onload = function (event) {

                data_Imagen = event.target.result;

            };

            reader.readAsDataURL(blob);
        }
    });

};



$('body').on('click', '#btn_search', function () {

    var doc = $('#txt_Document').val();    

    $.post(BaseUrl + "Sales/" + '_GetUser/', { document: doc })
        .done(function (Result) {          

            if (Result.status == 200) {
                $('.content_Assigment').css('display', 'block');
                $('#userId').val(Result.data.Id);
                $('#Names').val(Result.data.Names);
                $('#Document').val(Result.data.Document);
                $('#Campaign').val(Result.data.Campaign);
                //$('#Date').val(convertirFecha(Result.data.LoginDate));
            }
            if (Result.status == 404) {
                Swal.fire({
                    icon: 'info',
                    title: Result.message,
                    showConfirmButton: false,
                    timer: 1500
                })
            }
            if (Result.status == 500) {
                Swal.fire({
                    icon: 'error',
                    title: Result.message,
                    showConfirmButton: false,
                    timer: 1500
                })
            }

        });
});

function convertirFecha(fecha) {
    var fechaString = (fecha).substr(6);
    var fechaActual = new Date(parseInt(fechaString));
    var mes = fechaActual.getMonth() + 1;
    var dia = fechaActual.getDate();
    var anio = fechaActual.getFullYear();
    var fecha = "" + anio + "/" + mes + "/" + dia;

    return fecha;

}

$(document).ready(function () {
    $('.content_Assigment').css('display', 'none');
});

$('body').on('keyup', '#txt_Document', function (e) {
    
    if (e.keyCode === 13) {
        e.preventDefault();
        $('#btn_search').click();
    }
});

$('body').on('click', '#openModal', function (e) {

    var image = $('#InvoiceImage')[0].files[0];
   
    if (image == "" || image == undefined) {
        alert("Debe seleccionar la imágen de evidencia");
        return false;
    }
   
    $('#modalEvidence').modal('show');
  
});