﻿$("#cargarUsuarios").click(function () {

    if (window.FormData == undefined)
        alert("Error: FormData is undefined");

    else {
        var fileUpload = $("#fileUsuario").get(0);
        var files = fileUpload.files;

        if (files.length === 0) {
            swal({
                position: 'top-right',
                type: 'error',
                title: "por favor seleccionar el archivo.",
                showConfirmButton: false,
                timer: 1800
            });
        } else {

            $('#loading').css('display', 'block');
            var fileData = new FormData();

            fileData.append(files[0].name, files[0]);

            $.ajax({
                url: '/CargueEstadoTramites/CargarArchivoUsuarios',
                type: 'post',
                datatype: 'json',
                contentType: false,
                processData: false,
                async: true,
                data: fileData,
                success: function (response) {
                    if (response.status === 201) {
                        swal({
                            position: 'top-right',
                            type: 'success',
                            title: response.message,
                            showConfirmButton: false,
                            timer: 2500
                        });

                        $("#mostrarErrores").css("display", "none");
                        $('#loading').css('display', 'none');

                        limpiarFileInput("fileUsuario");

                    } else {
                        swal({
                            position: 'top-right',
                            type: 'error',
                            title: response.message,
                            showConfirmButton: false,
                            timer: 2500
                        });
                        if (response.message.includes('Error Formato')) {
                            $("#mostrarErrores").css("display", "block");
                        }
                        $('#loading').css('display', 'none');
                        limpiarFileInput("fileUsuario");
                    }
                }
            });
        }
    }

});


function limpiarFileInput(idFileInput) {
    document.getElementById(idFileInput).value = "";
}


