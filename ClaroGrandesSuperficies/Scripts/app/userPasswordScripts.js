﻿

var color = 'transparent';
var rgb = "";
var app = {};
var id = 0;


// Capturando los eventos de cada botón.
$('body').on('click', '.Red', function () {
    rgb = $('.textRed').css('color');
    $('.textRed').css('color', hideFields(rgb));
});

$('body').on('click', '.Login', function () {
    rgb = $('.textLogin').css('color');
    $('.textLogin').css('color', hideFields(rgb));
});

$('body').on('click', '.Extension', function () {
    rgb = $('.textExtension').css('color');
    $('.textExtension').css('color', hideFields(rgb));
});

$('body').on('click', '.Sac', function () {
    rgb = $('.textSac').css('color');
    $('.textSac').css('color', hideFields(rgb));
});

$('body').on('click', '.Ac', function () {
    rgb = $('.textAc').css('color');
    $('.textAc').css('color', hideFields(rgb));
});

$('body').on('click', '.Poliedro', function () {
    rgb = $('.textPoliedro').css('color');
    $('.textPoliedro').css('color', hideFields(rgb));

});
$('body').on('click', '.MyApps', function () {
    rgb = $('.textMyApps').css('color');
    $('.textMyApps').css('color', hideFields(rgb));
});
$('body').on('click', '.Agendamiento', function () {
    rgb = $('.textAgendamiento').css('color');
    $('.textAgendamiento').css('color', hideFields(rgb));
});
$('body').on('click', '.Visor', function () {
    rgb = $('.textVisor').css('color');
    $('.textVisor').css('color', hideFields(rgb));
});
$('body').on('click', '.Rr', function () {
    rgb = $('.textRr').css('color');
    $('.textRr').css('color', hideFields(rgb));
});

$('body').on('click', '.MiEnlace', function () {
    rgb = $('.textMiEnlace').css('color');
    $('.textMiEnlace').css('color', hideFields(rgb));
});

$('body').on('click', '.EnEnlaceIspira', function () {
    rgb = $('.textEnEnlaceIspira').css('color');
    $('.textEnEnlaceIspira').css('color', hideFields(rgb));
});

$('body').on('click', '.MyIt', function () {
    rgb = $('.textMyIt').css('color');
    $('.textMyIt').css('color', hideFields(rgb));
});

$('body').on('click', '.PortalSac', function () {
    rgb = $('.textPortalSac').css('color');
    $('.textPortalSac').css('color', hideFields(rgb));
});


$('body').on('click', '.Dime', function () {
    rgb = $('.textDime').css('color');
    $('.textDime').css('color', hideFields(rgb));
});

$('body').on('click', '.IdVision', function () {
    rgb = $('.textIdVision').css('color');
    $('.textIdVision').css('color', hideFields(rgb));
});

$('body').on('click', '.HelpDesk', function () {
    rgb = $('.textHelpDesk').css('color');
    $('.textHelpDesk').css('color', hideFields(rgb));
});
$('body').on('click', '.Teams', function () {
    rgb = $('.textTeams').css('color');
    $('.textTeams').css('color', hideFields(rgb));
});
$('body').on('click', '.Cifin', function () {
    rgb = $('.textCifin').css('color');
    $('.textCifin').css('color', hideFields(rgb));
});
$('body').on('click', '.EvidenteMaster', function () {
    rgb = $('.textEvidenteMaster').css('color');
    $('.textEvidenteMaster').css('color', hideFields(rgb));
});
$('body').on('click', '.Integracion', function () {
    rgb = $('.textIntegracion').css('color');
    $('.textIntegracion').css('color', hideFields(rgb));
});



$(document).ready(function () {
    $('.textRed').css('color', 'transparent');
    $('.textLogin').css('color', 'transparent');
    $('.textExtension').css('color', 'transparent');
    $('.textSac').css('color', 'transparent');
    $('.textAc').css('color', 'transparent');
    $('.textPoliedro').css('color', 'transparent');
    $('.textMyApps').css('color', 'transparent');
    $('.textAgendamiento').css('color', 'transparent');
    $('.textVisor').css('color', 'transparent');
    $('.textRr').css('color', 'transparent');
    $('.textMiEnlace').css('color', 'transparent');
    $('.textEnEnlaceIspira').css('color', 'transparent');
    $('.textMyIt').css('color', 'transparent');
    $('.textPortalSac').css('color', 'transparent');
    $('.textDime').css('color', 'transparent');
    $('.textIdVision').css('color', 'transparent');
    $('.textHelpDesk').css('color', 'transparent');
    $('.textTeams').css('color', 'transparent');
    $('.textCifin').css('color', 'transparent');
    $('.textEvidenteMaster').css('color', 'transparent');
    $('.textIntegracion').css('color', 'transparent');


});

// Función que se encarga de ocultar el texto.
function hideFields(_rgb) {

    _rgb = "" + _rgb + "";

    if (rgb == 'rgba(0, 0, 0, 0)') {
        color = 'black';
    } else {
        color = 'transparent';
    }
    return color;
}


// Procedimiento para actualizar el estado de los usuarios.
$('body').on('click', '.LockRed', function () {
    
    id = $(this).val()
    SendApp("Red", id);
});
$('body').on('click', '.LockLogin', function () {
    
    id = $(this).val()
    SendApp("Login", id);
});
$('body').on('click', '.LockExtension', function () {
    
    id = $(this).val()
    SendApp("Extension", id);
});
$('body').on('click', '.LockSac', function () {
    
    id = $(this).val()
    SendApp("Sac", id);
});
$('body').on('click', '.LockAc', function () {
    
    id = $(this).val()
    SendApp("Ac", id);
});
$('body').on('click', '.LockPoliedro', function () {
    
    id = $(this).val()
    SendApp("Poliedro", id);
});
$('body').on('click', '.LockMyApps', function () {
    
    id = $(this).val()
    SendApp("MyApps", id);
});
$('body').on('click', '.LockAgendamiento', function () {
    
    id = $(this).val()
    SendApp("Agendamiento", id);
});
$('body').on('click', '.LockVisor', function () {
    
    id = $(this).val()
    SendApp("Visor", id);
});
$('body').on('click', '.LockRr', function () {
    
    id = $(this).val()
    SendApp("Rr", id);
});
$('body').on('click', '.LockMiEnlace', function () {
    
    id = $(this).val()
    SendApp("MiEnlace", id);
});
$('body').on('click', '.LockEnEnlaceIspira', function () {
    
    id = $(this).val()
    SendApp("EnEnlaceIspira", id);
});
$('body').on('click', '.LockMyIt', function () {
    
    id = $(this).val()
    SendApp("MyIt", id);
});
$('body').on('click', '.LockPortalSac', function () {
    
    id = $(this).val()
    SendApp("PortalSac", id);
});
$('body').on('click', '.LockDime', function () {
    
    id = $(this).val()
    SendApp("Dime", id);
});
$('body').on('click', '.LockIdVision', function () {
    
    id = $(this).val()
    SendApp("IdVision", id);
});
$('body').on('click', '.LockHelpDesk', function () {
    
    id = $(this).val()
    SendApp("HelpDesk", id);
});
$('body').on('click', '.LockTeams', function () {
    
    id = $(this).val()
    SendApp("Teams", id);
});
$('body').on('click', '.LockCifin', function () {
    
    id = $(this).val()
    SendApp("Cifin", id);
});
$('body').on('click', '.LockEvidenteMaster', function () {
    
    id = $(this).val()
    SendApp("EvidenteMaster", id);
});
$('body').on('click', '.LockIntegracion', function () {
    
    id = $(this).val()
    SendApp("Integracion", id);
});


// función que actualiza el estado de los usuarios. 
function SendApp(app, id) {

    if (confirm("¿Está seguro de reportar este Aplicativo como bloqueado?")) {

        $.get('UserPasswords/LockUser/', { app, id })
            .done((result) => {
                if (result.status == 200) {                   
                    console.log(result.message);
                    location.reload();
                } else {

                    console.log(result.message);                  
                }

            })
            .fail((data, status) => {
                _console("error interno ");
                _console(data);
                _console(status);
                swal({
                    position: 'top-right',
                    type: 'error',
                    title: Result.message,
                    showConfirmButton: false,
                    timer: 1500
                });

            });
    }

}

