
var dimerG = "";
var botonCargaG = false;
var b_validacion = true;
var i_close = 0;    // elimina el boton de cerrado
var primerElementoG = true; // permite saber si es el primer elemento para las validaciones
var i_ubicacionMensaje = 1;
url = $("#url").text();
$(document).ready(function () {
    if ($(".fecha").length > 0 || $(".fechaCalendario").length > 0) {
        calendarioEs();
        $(".fecha").datepicker({ dateFormat: 'yy-mm-dd' });
        $(".fechaCalendario.todo").datepicker({ dateFormat: 'yy-mm-dd' });
        $(".fechaCalendario.hoy").datepicker({ dateFormat: 'yy-mm-dd', minDate: -3 });
        $(".fechaCalendario.despues").datepicker({ dateFormat: 'yy-mm-dd', minDate: 1 });
        $(".fechaCalendario.antes").datepicker({ dateFormat: 'yy-mm-dd', maxDate: -1 });
    }
});


/*ººººººººººººººººººººººººººººººººººººººººººººººººººººººººººººººººººººººººººº*/
/*ººººººººººº|	DECLARACION EVENTOS DEL MODULO 			 	   |ººººººººººººº*f/
/*ººººººººººººººººººººººººººººººººººººººººººººººººººººººººººººººººººººººººººº*/
$(document).ready(function () {
    $("body").on('click', '.btn_removerFila', void_EliminarFila);
    $("body").on('click', '#_volver', closeToBack);
});


/*  Nombre Funcion: void_EliminarFila
	Descripción: elimina la fila padre de una tabla
	param: 
	return: 
*/

function void_EliminarFila() {
    $(this).parent().parent().remove();
}



/*  Nombre Funcion: void_EliminarFila
	Descripción: elimina la fila padre de una tabla
	param: 
	return: 
*/

function void_RedirectLogin(status, Ruta) {
    if (status == 401) {
        window.location = Ruta;
    }
    return true;
}





/*  Nombre Funcion: volver
	Descripción: devolver a la pestaña anterior
	param: color,m_padre,m_hijo
	return: mensaje
*/

function volver(url) {
    history.back();
    window.location = url;
}

function refreshParent() {
    window.opener.location.reload();
}

function closeToBack() {
    var refresh = $(this).data("refresh");

    if (window.opener != null || history.length == 1) {
        console.log(window.opener);
        window.close()
        if (refresh)
            window.opener.location.reload();
    } else {
        console.log('test test1');
        history.back()
    }
}



function string_mensaje(m_mensaje, color) {

    var color = typeof color !== 'undefined' ? color : "alert-danger";
    var close = (!i_close) ? " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" : "";
    color == "alert-danger" ? "" : $(".alert").remove();
    var mensaje = "<div class='" + color + " alert'> \
					   "+ close + "\
					  <p>"+ m_mensaje + "</p> \
					</div>";
    return mensaje;
}


/*  Nombre Funcion: EsCorreo
	Descripción: verifica si un campo en especifico es de tipo correo
	param: mail
	return: booleano
*/

function EsCorreo(mail) {
    return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(mail);
}

function EsLetra(cadena) {
    // var patron = /^[a-zA-Z]*$/;
    var patron = /^[a-zA-Z\sñÑ]*$/;
    // En caso de querer validar cadenas con espacios usar: /^[a-zA-Z\s]*$/
    if (!cadena.search(patron))
        return true;
    else
        return false;
}

/*  Nombre Funcion: EsEntero
	Descripción: verifica si un campo en especifico es de tipo entero
	param: entero
	return: booleano
*/

function EsEntero(entero) {
    return /^([0-9])*$/.test(entero);
}

/*  Nombre Funcion: SeguridadPassword
	Descripción: verifica si un campo en especifico es de tipo entero
	param: entero
	return: a_respuesta
*/

function SeguridadPassword(password) {
    a_respuesta = new Array();
    a_respuesta[0] = 1
    if (!/\W/.test(password)) {
        a_respuesta[0] = 0;
        a_respuesta[1] = "debe poseer almenos un caracter especial";
        return a_respuesta;
    }
    if (/ /.test(password)) {
        a_respuesta[0] = 0;
        a_respuesta[1] = "no puede tener espacios por seguridad";
        return a_respuesta;
    }
    if (!/[0-9]/.test(password)) {
        a_respuesta[0] = 0;
        a_respuesta[1] = "debe poseer almenos un numero";
        return a_respuesta;
    }
    if (!/[a-zA-Z]/.test(password)) {
        a_respuesta[0] = 0;
        a_respuesta[1] = "debe poseer almenos una letra";
        return a_respuesta;
    }
    return a_respuesta;
}

/*  Nombre Funcion: void_borrarCampo
	Descripción: borra el campo del valor seleccionado
	param: 
	return: 
*/
function void_borrarCampo() {
    elm = $(this);

    tipoElm = elm.prop('tagName');
    if (tipoElm == "INPUT") {
        $(this).val("");
        // $(this).text("");
    }

    if (tipoElm == "SELECT") {
        elm.val("0");
    }
    if (tipoElm == "TEXTAREA") {
        elm.val("");
    }
}

/*  Nombre Funcion: void_validaCampo
	Descripción: verifi
	param: 
	return: 
*/

function void_validaCampo() {

    var elm = $(this);

    switch (i_ubicacionMensaje) {
        case 1:
            ubicacion = elm.parent();
            break;
        case 2:
            ubicacion = elm.parent().parent();
            break;
        case 3:
            ubicacion = elm.parent().parent().parent();
            break;
        case 4:
            ubicacion = elm.parent().parent().parent().parent();
            break;
        default:
            ubicacion = elm.parent();
            break;
    }
    var nombre = elm.data("nombre");
    var minimo = elm.data("largo");
    var maximo = elm.data("maximolargo");
    var tipo = elm.data('tipo');
    var etiq = elm.prop("tagName");
    var requerido = elm.prop("required");

    nombre = typeof nombre !== 'undefined' ? nombre : "";
    if (tipo == "numero" && elm.val() != "") {
        if (!EsEntero(elm.val())) {
            b_validacion = false;
            ubicacion.append(string_mensaje("El campo " + nombre + " no es un numero"));
            if (primerElementoG == 1) { elm.focus(); primerElementoG = 0 }
        }
    }
    if (tipo == "letras" && elm.val() != "") {
        if (!EsLetra(elm.val())) {
            b_validacion = false;
            ubicacion.append(string_mensaje("El campo " + nombre + " solo puede tener letras"));
            if (primerElementoG == 1) { elm.focus(); primerElementoG = 0 }
        }
    }
    if (tipo == "correo" && elm.val() != "") {
        if (!EsCorreo(elm.val())) {
            b_validacion = false;
            ubicacion.append(string_mensaje("El campo " + nombre + " no tiene un correo valido"));
            if (primerElementoG == 1) { elm.focus(); primerElementoG = 0 }
        }
    }
    if (tipo == "password" && elm.val() != "") {
        console.log('comprobando seguridad password');
        a_comprobacion = SeguridadPassword(elm.val());
        if (!a_comprobacion[0]) {
            b_validacion = false;
            ubicacion.append(string_mensaje("Error password inseguro, El campo " + nombre + " " + a_comprobacion[1]));
            if (primerElementoG == 1) { elm.focus(); primerElementoG = 0 }
        }
    }
    if ((elm.val() == 0 || elm.val() == "") && requerido) {
        b_validacion = false;
        etiq == "INPUT" ? ubicacion.append(string_mensaje("El campo " + nombre + " esta vacio")) : "";
        etiq == "SELECT" ? ubicacion.append(string_mensaje("No ha escogido una opción para el campo " + nombre)) : "";
        etiq == "TEXTAREA" ? ubicacion.append(string_mensaje("no se ha ingresado ningun dato para el campo " + nombre)) : "";
        if (primerElementoG == 1) { elm.focus(); primerElementoG = 0 }

    };
    if (minimo > 0 && (minimo > elm.val().length) && b_validacion) {
        b_validacion = false;
        ubicacion.append(string_mensaje("se necesitan minimo " + minimo + " letras En el campo " + nombre + " solo se ingresaron " + elm.val().length + " letras"));
        if (primerElementoG == 1) { elm.focus(); primerElementoG = 0 }
    };
    if (maximo > 0 && (maximo < elm.val().length) && b_validacion) {
        console.log("tiene maximo", maximo);
        b_validacion = false;
        ubicacion.append(string_mensaje("se adminte un  maximo de " + maximo + " letras En el campo " + nombre + " y  se ingresaron " + elm.val().length + " letras"));
        if (primerElementoG == 1) { elm.focus(); primerElementoG = 0 }
    };
}





function calendarioEs() {
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: '',
        changeMonth: true,
        changeYear: true,
        //yearRange: '1970:2030'
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
}



/*  Nombre Funcion: ajaxStart
	Descripción: activa el dimer para el elemento que se necesita
	param: 
	return: 
*/

$(document).ajaxStart(function () {
    var s_dimmer_I = "<div class='overlay'>\
                  <i class='fa fa-refresh fa-spin'></i>\
                </div>";



    if (botonCargaG) {
        dimerG.addClass('loading');
    }
    else {
        $(dimerG + ".box-body").append(s_dimmer_I);
    }
});
$(document).ajaxStop(function () {
    if (botonCargaG) {
        dimerG.removeClass('loading');
        botonCargaG = 0;
    }
    else {
        $(".overlay").remove();
    }
});


/*  Nombre Funcion: string_solicituError
	Descripción: verifica si la solicitud ajax viene con un error en el sistema
	param: ubicacion,datos
	return: booleano
*/

function string_solicituError(ubicacion, datos) {
    if (/ALERTA/.test(datos)) {
        data = datos.split("[ALERTA]")
        ubicacion.append(string_mensaje(data[1]));
        console.log('prueba 1')
        return 1;
    }
    return 0;
}

/*ªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªºº*/
/*ªªªªªªªª| codigo reloj javascript 						     |ªªªªºº*/
/*ªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªªºº*/
function reloj() {
    var hoy = new Date(); var h = hoy.getHours(); var m = hoy.getMinutes(); var s = hoy.getSeconds();
    m = actualizarHora(m); s = actualizarHora(s);
    fechaActual = fecha();
    document.getElementById('displayReloj').innerHTML = fechaActual + " " + h + ":" + m + ":" + s;
    var t = setTimeout(function () { reloj() }, 500);
}

function actualizarHora(i) {
    if (i < 10) { i = "0" + i };  // Añadir el cero en números menores de 10
    return i;
}
function fecha() {
    var d = new Date();
    var dia = new Array(7);
    dia[0] = "Domingo";
    dia[1] = "Lunes";
    dia[2] = "Martes";
    dia[3] = "Miercoles";
    dia[4] = "Jueves";
    dia[5] = "Viernes";
    dia[6] = "Sabado";
    var mm = new Date();
    var m2 = mm.getMonth() + 1;
    var mesok = (m2 < 10) ? '0' + m2 : m2;
    var mesok = new Array(12);
    mesok[0] = "Enero";
    mesok[1] = "Febrero";
    mesok[2] = "Marzo";
    mesok[3] = "Abril";
    mesok[4] = "Mayo";
    mesok[5] = "Junio";
    mesok[6] = "Julio";
    mesok[7] = "Agosto";
    mesok[8] = "Septiembre";
    mesok[9] = "Octubre";
    mesok[10] = "Noviembre";
    mesok[11] = "Diciembre";
    var yy = d.getYear();
    var year = (yy < 1000) ? yy + 1900 : yy;
    return d.getDate() + " de " + mesok[mm.getMonth()] + " del " + year;
}

/*  Nombre Funcion: void_pie
	Descripción: nos lleva al final de la pagina
	param: 
	return: 
*/


function void_pie() {
    window.scrollTo(0, document.body.scrollHeight);
}
/*  Nombre Funcion: convertirFormato
	Descripción: convierte una fecha del formato   año/mes/dia a año/dia/mes
	param: fechaAnterior
	return: fechaNueva
*/



/*  Nombre Funcion: convertirFormato
	Descripción: convierte una fecha del formato   año/mes/dia a año/dia/mes
	param: fechaAnterior
	return: fechaNueva
*/

function string_ajaxError(columna, texto) {
    var tx = typeof texto !== 'undefined' ? texto : "hay un error en la consulta por favor consulte al administrador";
    var cl = typeof columna !== 'undefined' ? columna : "2";

    var ms = "<tbody>\
		<tr  class='alert-danger'>\
			<td colspan="+ cl + ">" + tx + " \
			</td>\
		</tr>\
		<tbody>";
    return ms;
}


/*globales de admin tl*/
//Initialize Select2 Elements
// $(".select2").select2();

function restaFechas(texto, texto2) {
    var fFecha1 = Date.UTC(texto.substring(0, 4), texto.substring(4, 6) - 1, texto.substring(6, 8));
    var fFecha2 = Date.UTC(texto2.substring(0, 4), texto2.substring(4, 6) - 1, texto2.substring(6, 8));
    var dif = fFecha2 - fFecha1;
    var dias = Math.floor(dif / (1000 * 60 * 60 * 24));
    return dias;
}




$(document).on('submit', "form#frmGuardarUsuario", function (e) {
    e.preventDefault();

})




/*  Nombre Funcion: void_checkUncheck
	Descripción: borra el campo del valor seleccionado
	param: 
	return: 
*/

function void_checkUncheck(e, t) {
    var s_lm = $(e);
    var type = s_lm.prop("tagName");
    var check = s_lm.is(":checked");
    return check === true ? $(t).prop('checked', true) : $(t).prop('checked', false);
}


function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}