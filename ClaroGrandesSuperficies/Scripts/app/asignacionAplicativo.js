﻿
var color = 'transparent';
var rgb = "";
var app = {};
var idApp = 0;



$(".table tr").mouseover(function (e) {
    var id = $(this).attr('id');
  
    $('#usuario_' + id).css('color', '');
    $('#clave_' + id).css('color', '');
});

$(".table tr").mouseout(function (e) {
    var id = $(this).attr('id');
    $('#usuario_' + id).css('color', 'transparent');
    $('#clave_' + id).css('color', 'transparent');
});








// Función que se encarga de ocultar el texto.

$(document).ready(function () {
    $('.hideText').css('color', 'transparent');
    $('.hideText').css('color', 'transparent');
});

$('body').on('click', '.btn_App', function () {

    idApp = $(this).val();
   
});

$('body').on('change', '#tipificacion', function () {

    var tipificacion = $('#tipificacion option:selected').val();
    if (tipificacion > 0) {
        $('#errorTipificacion').text("");

    } else {
        $('#errorTipificacion').text("Seleccione el motivo de Bloqueo...").css('color', 'red');
    }
   
});

// Procedimiento para actualizar el estado de los usuarios.

$('body').on('click', '.Lock', function () {  

    var tipificacion = $('#tipificacion option:selected').val();

    if (tipificacion == "" || tipificacion == undefined) {    
        $('#errorTipificacion').text("Seleccione el motivo de Bloqueo...").css('color', 'red');
        return false;
    }

    var id = idApp;  

    SendApp(id, tipificacion);
});




// función que actualiza el estado de los usuarios. 
function SendApp(id, tipificacion) {

    if (confirm("¿Está seguro de reportar este Aplicativo como bloqueado?")) {

        $.get('AsignacionAplicativos/LockUser/', { id, tipificacion })
            .done((result) => {
                if (result.status == 200) {                   
                    console.log(result.message);
                    location.reload();
                } else {

                    console.log(result.message);                  
                }

            })
            .fail((data, status) => {
                _console("error interno ");
                _console(data);
                _console(status);
                swal({
                    position: 'top-right',
                    type: 'error',
                    title: result.message,
                    showConfirmButton: false,
                    timer: 1500
                });

            });
    }

}


////// CARGA DEL ROL
//function updateDropdown(Result) {
//    let dropdown = $("#app");
//    let dropdownContent = "<option selected value='0'>Seleccione</option>";
//    dropdown.append(dropdownContent);

//    $.each(Result, function (index, Value) {
//        $(dropdown).append('<option value="' + Value.Id + '">' + Value.NameRol + '</option>');
//    });

//    window.scrollTo(0, document.body.scrollHeight);
//}


//// Debe ir cuando se seleccione la primera opción
//// LIMPIANDO EL SELECT
//$('#app option').each(function () {
//    $(this).remove();
//});


//// FIN CARGA ROL

// Cambiar contraseña Aplicativo.
$('body').on('click', '.change', function () {
    var id = $(this).val();

    $.get('AsignacionAplicativos/CambiarContraseñaApp/', { id })
        .done((result) => {
            if (result.status == 200) {
                console.log(result.data);
                $('#UsuarioAplicativo').val(result.data.UsuarioAplicativo);
                $('#btnCambiarContraseñaApp').val(result.data.Id);

            } else {

                console.log(result.message);
            }

        })
        .fail((data, status) => {
            _console("error interno ");
            _console(data);
            _console(status);
            swal({
                position: 'top-right',
                type: 'error',
                title: Result.message,
                showConfirmButton: false,
                timer: 1500
            });

        });
});



// Cambiar contraseña Aplicativo.
$('body').on('click', '#btnCambiarContraseñaApp', function () {

    $('input, .pass').parent('.form-group').find('.alert-danger').remove();
    b_validacion = true;
    primerElementoG = 1;
    $('input, .pass').each(void_validaCampo)
   

    if (!b_validacion) { return false }

    var id = $(this).val();   
    var contrasena = $('#password').val();
    var ConfirmarContrasena = $('#confirmPassword').val();

    // Verificamos las contraseñas
    if (!confirmarContraseña(contrasena, ConfirmarContrasena)) {
        alert("Las contraseñas no coinciden");
        $('#password').val("");
        $('#confirmPassword').val("");

        return false;
    }
  

    $.post('AsignacionAplicativos/EditarContraseñaApp/', { id, contrasena })
        .done((result) => {
            if (result.status == 200) {
                swal({
                    position: 'top-end',
                    type: 'success',
                    title: "La contraseña se cambió correctamente",
                    showConfsirmButton: false,
                    timer: 2000
                });
                setTimeout(function () {
                    location.reload();   
                }, 1000);

               


            } else {

                console.log(result.message);
            }

        })
        .fail((data, status) => {
            _console("error interno ");
            _console(data);
            _console(status);
            swal({
                position: 'top-right',
                type: 'error',
                title: Result.message,
                showConfirmButton: false,
                timer: 1500
            });

            $('#cerrar').click();

        });
});



$("body").on('input', '.pass', function () {

    $(this).parent('.form-group').find('.alert-danger').remove();
    b_validacion = true;
    primerElementoG = 1;
    $(this).each(void_validaCampo)

    if (!b_validacion) { return false }

});

// Confirmando contraseña

function confirmarContraseña(contrasena, confirmarContrasena) {

    if (contrasena === confirmarContrasena) {
        return true;
    } else {
        return false;
    }
}


// Mostrar tipificaciones por Aplicativo
// Este proceso se suspende por nuevos cambios 
//$('body').on('click', '.btn_App', function () {
//    var id = $(this).val();

//    $.get('AsignacionAplicativos/BloquearApp/', { id })
//        .done((result) => {
//            if (result.status == 200) {
                
//                $('#slt-tipificacion option').each(function () {
//                    $(this).remove();
//                });
//                for (let i = 0; i < result.data.length; i++) {
//                    console.log(result.data[i]);
//                    $("#slt-tipificacion").append('<option value="' + result.data[i].Tipificacion.Id + '">' + result.data[i].Tipificacion.Name + '</option>');
//                }

//            } else {

//                console.log(result.message);
//            }

//        })
//        .fail((data, status) => {
//            _console("error interno ");
//            _console(data);
//            _console(status);
//            swal({
//                position: 'top-right',
//                type: 'error',
//                title: Result.message,
//                showConfirmButton: false,
//                timer: 1500
//            });

//        });
//});