﻿$("#eliminarUsuarios").click(function () {

    if (confirm("¿Está seguro de eliminar los usuarios seleccionados?")) {

        if (window.FormData == undefined)
            alert("Error: FormData is undefined");

        else {
            var fileUpload = $("#fileUsuario").get(0);
            var files = fileUpload.files;

            if (files.length === 0) {
                swal({
                    position: 'top-right',
                    type: 'error',
                    title: "por favor seleccionar el archivo.",
                    showConfirmButton: false,
                    timer: 1800
                });
            } else {
                var fileData = new FormData();

                fileData.append(files[0].name, files[0]);

                $.ajax({
                    url: '/DeleteUsuarios/CargarArchivoUsuarios',
                    type: 'post',
                    datatype: 'json',
                    contentType: false,
                    processData: false,
                    async: false,
                    data: fileData,
                    success: function (response) {
                        if (response.status === 201) {
                            swal({
                                position: 'top-right',
                                type: 'success',
                                title: response.message,
                                showConfirmButton: false,
                                timer: 1800
                            });
                        } else {
                            swal({
                                position: 'top-right',
                                type: 'success',
                                title: response.message,
                                showConfirmButton: false,
                                timer: 1800
                            });

                        }
                    }
                });
            }
        }
    }
   
   

});