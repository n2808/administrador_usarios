﻿namespace ClaroGrandesSuperficies.Constantes
{
    public static class Configuraciones
    {

        public const string MENSAJE_ERROR_FORMATO_ASIGNACION = "Error Formato, Por favor verique el formato de cargue, se ha generado un archivo excel indicando los errores en el formato.";

        public const string MENSAJE_ERROR_COLUMNAS = "Por favor verifiqué el nombre de las columnas del archivo excel.";

        public const string MENSAJE_ERROR_FORMATO_INCORRECTO = "Error Formato, Por favor verique que el formato sea el adecuado.";
    }

    public static class AplicativosSelenium
    {
        public const int SAC = 4;
        public const int POLIEDRO = 6;
        public const int MY_APPS = 7;
        public const int AGENDAMIENTO = 8;
        public const int VISOR = 9;
        public const int MI_ENLACE = 11;
        public const int EN_ENLACE_INSPIRA = 12;
        public const int MY_IT = 13;
        public const int PORTALSAC = 14;
        public const int DIME = 15;
        public const int ID_VISION = 16;
        public const int HELPDESK = 17;
        public const int TEAMS = 18;
        public const int MI_CLARO_ASESOR = 22;
        public const int DIAGNOSTICADOR = 23;
        public const int CMAX = 24;
        public const int TYD = 25;
        public const int ASCARD = 26;
        public const int SARA = 27;
        public const int POLIEDRO_1 = 29;
        public const int POLIEDRO_2 = 30;
        public const int POLIEDRO_3 = 31;
        public const int POLIEDRO_4 = 32;
        public const int POLIEDRO_5 = 33;
        public const int WHATSAPP_AGENTE = 34;
        public const int WHATSAPP_SUPERVISOR = 35;
        public const int SSFF = 36;

    }

    public static class AplicativosSistema
    {
        public const int SAC = 4;
        public const int AC = 5;
        public const int POLIEDRO = 6;
        public const int MY_APPS = 7;
        public const int AGENDAMIENTO = 8;
        public const int VISOR = 9;
        public const int MI_ENLACE = 11;
        public const int EN_ENLACE_INSPIRA = 12;
        public const int MY_IT = 13;
        public const int PORTALSAC = 14;
        public const int DIME = 15;
        public const int ID_VISION = 16;
        public const int HELPDESK = 17;
        public const int TEAMS = 18;
        public const int MI_CLARO_ASESOR = 22;
        public const int DIAGNOSTICADOR = 23;
        public const int CMAX = 24;
        public const int TYD = 25;
        public const int ASCARD = 26;
        public const int SARA = 27;
        public const int POLIEDRO_1 = 29;
        public const int POLIEDRO_2 = 30;
        public const int POLIEDRO_3 = 31;
        public const int POLIEDRO_4 = 32;
        public const int POLIEDRO_5 = 33;
        public const int WHATSAPP_AGENTE = 34;
        public const int WHATSAPP_SUPERVISOR = 35;
        public const int SSFF = 36;

    }
}
