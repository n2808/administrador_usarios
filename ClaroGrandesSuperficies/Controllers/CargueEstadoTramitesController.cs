﻿using ClaroGrandesSuperficies.BusinessLogic;
using ClaroGrandesSuperficies.Constantes;
using System;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace ClaroGrandesSuperficies.Controllers
{
    public class CargueEstadoTramitesController : Controller
    {
        CargueEstadoTramiteBS cargueEstadoTramite = new CargueEstadoTramiteBS();
        // GET: CargueEstadoTramites
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult CargarArchivoUsuarios()
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;

                    HttpPostedFileBase file = files[0];
                    string fileName = file.FileName;

                    EliminarArchivosResources();

                    string path = Path.Combine(Server.MapPath("~/Resources/CargueEstadoTramite"),
                                  Path.GetFileName(file.FileName));
                    file.SaveAs(path);

                    var dt = cargueEstadoTramite.InsertarCargueEstadoTramite(path);

                    return Json(new { status = 201, message = "El archivo se ha cargado correctamente!" });

                }

                catch (Exception ex)
                {
                    return Json(new { status = 504, message = Configuraciones.MENSAJE_ERROR_FORMATO_INCORRECTO });
                }
            }

            return Json("Por favor seleccione el archivo");
        }


        public void EliminarArchivosResources()
        {
            try
            {
                var pathResources = Path.Combine(Server.MapPath("~/Resources/CargueEstadoTramite").ToString());

                DirectoryInfo di = new DirectoryInfo(pathResources);

                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();

                }
            }
            catch (Exception ex)
            {
                throw new Exception(Configuraciones.MENSAJE_ERROR_FORMATO_INCORRECTO);
            }
        }
    }
}