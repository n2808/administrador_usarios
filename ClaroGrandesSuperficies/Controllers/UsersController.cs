﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ClaroGrandesSuperficies.Models;
using Core.Models.Security;
using Core.Models.User;
using PagedList;

namespace ClaroGrandesSuperficies.Controllers
{
    [CustomAuthorize(Roles = "Administrador")]
    public class UsersController : Controller
    {
        private ContextModel db = new ContextModel();

        // GET: Users
        public ActionResult Index(int? page, string filterUserByDocument, string filterUserByName)
        {
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PaginationUsers"]);

            IQueryable<User> listUsers = from u in db.Users
                                         select u;

            if (!string.IsNullOrEmpty(filterUserByDocument))
            {
                string document = filterUserByDocument.Trim();
                listUsers = listUsers.Where(u => u.Document == document);
            }

           
            return View(listUsers.OrderBy(u => u.Names).ToList().ToPagedList(page ?? 1, pageSize));
        }

        public ActionResult ActivatteOrDeactivate(Guid? id)
        {
            User user = db.Users.Where(x => x.Id == id).FirstOrDefault();

            if (user != null)
            {
                bool status = user.Status;
                if (status)
                {
                    user.Status = false;
                }
                else
                {
                    user.Status = true;

                }

                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { filterUserByDocument = user.Document });
            }

            return RedirectToAction("Index");

        }

        // GET: Users/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Document,Names,CreatedAt")] User user)
        {
            if (ModelState.IsValid)
            {
                user.Id = Guid.NewGuid();
                user.PassWord = user.Document.Trim();
                user.Status = true;
                db.Users.Add(user);
                db.SaveChanges();
                return RedirectToAction("Create");
            }

            return View(user);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Document,Names,LastName,Phone1,Phone2,Phone3,Email,Status,PassWord,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Roles(Guid Id)
        {
            User Usuario = db.Users.Find(Id);
            if (Id == null || Usuario == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.Name = Usuario.Names;
            ViewBag.Id = Id;
            ViewBag.Email = Usuario.Email;
            ViewBag.Pass = Usuario.PassWord;
            ViewBag.UsersRol = db.UserRols.Include(u => u.Rol).Include(u => u.User).Where(p => p.UserId == Id);
            ViewBag.RolId = new SelectList(db.Rols, "Id", "Name");
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Roles([Bind(Include = "Id,RolId,UserId")] UserRol userRol, string Name, string password, string email)
        {
            if (ModelState.IsValid)
            {
                var Exist = db.UserRols
                    .Where(
                    c => c.UserId == userRol.UserId &&
                    c.RolId == userRol.RolId
                ).FirstOrDefault();

                if (Exist != null)
                {
                    ModelState.AddModelError("RolId", "este permiso ya fue asignado");
                }
                else
                {
                    User user = db.Users.Where(x => x.Id == userRol.UserId).FirstOrDefault();

                    if (user != null)
                    {
                        user.PassWord = password.Trim();
                        user.Email = email.Trim();
                        db.Entry(user).State = EntityState.Modified;                        
                        userRol.Id = Guid.NewGuid();
                        db.UserRols.Add(userRol);
                        db.SaveChanges();
                        return RedirectToAction("Roles", new { Id = userRol.UserId });
                    }
                 
                }


            }
            var Id = userRol.UserId;
            ViewBag.Email = email.Trim();
            ViewBag.Pass = password.Trim();
            ViewBag.Name = Name;
            ViewBag.Id = Id;
            ViewBag.UsersRol = db.UserRols.Include(u => u.Rol).Include(u => u.User).Where(p => p.UserId == Id);
            ViewBag.RolId = new SelectList(db.Rols, "Id", "Name");
            return View("Roles");
        }

        [HttpPost, ActionName("DeleteRol")]
        [ValidateAntiForgeryToken]
        public ActionResult RolesDeleteConfirmed(Guid id)
        {
            UserRol UsusarioRol = db.UserRols.Find(id);
            var UserId = UsusarioRol.UserId;
            db.UserRols.Remove(UsusarioRol);
            db.SaveChanges();
            return RedirectToAction("Roles", new { Id = UserId });
        }


    }
}
