﻿using ClaroGrandesSuperficies.Models;
using ClaroGrandesSuperficies.Models.AdministracionUsuariosApp;
using ClaroGrandesSuperficies.Viewmodel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.Configuration;

namespace ClaroGrandesSuperficies.Controllers
{
    public class EstadoTramitesController : Controller
    {
        private ContextModel db = new ContextModel();

        // GET: EstadoTramites
        public ActionResult Index(DateTime? fechaBloqueoApp, string estadoApp, int? page)
        {

            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PaginationUsers"]);

            var estado = false;

            ViewBag.SinEscalar = RetornarTotales("SinEscalar");
            ViewBag.Entramite = RetornarTotales("EnTramite");
            ViewBag.Solucionado = RetornarTotales("Solucionado");

            if (!ValidarFiltros(fechaBloqueoApp))
            {              
                return View(new List<EstadoTramitesVM>().ToPagedList(page ?? 1, pageSize));
            }

            estado = Convert.ToBoolean(estadoApp);

            var aplicativos = db.AsignacionAplicativos
                                                    .Include(x => x.User)
                                                    .Include(x => x.AplicativoUsuario)
                                                    .Include(x => x.EstadoTramite)
                                                    .Where(x => x.FechaReporteBloqueoApp >= fechaBloqueoApp && x.EstadoAplicativo == estado)
                                                    .OrderBy(x => x.FechaReporteBloqueoApp)
                                                    .ToList();

            ViewBag.fechaBloqueoApp = fechaBloqueoApp;

           
            return View(RetornarAplicativos(aplicativos).ToPagedList(page ?? 1, pageSize));
        }


        public int RetornarDiasDiferencia(AsignacionAplicativosUsuario model)
        {
            var valor = 0;

            if (model.FechaReporteBloqueoApp == null)
            {
                return valor;

            }else if (model.FechaDesbloqueoApp == null)
            {
                return (DateTime.Now - model.FechaReporteBloqueoApp).Value.Days;
            }
            else
            {
                return (model.FechaDesbloqueoApp - model.FechaReporteBloqueoApp).Value.Days;

            }
            
        }

        public bool ValidarFiltros(DateTime? fechaBloqueoApp)
        {
            if (fechaBloqueoApp == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public ActionResult EstadosAplicativos(string name)
        {
            List<AsignacionAplicativosUsuario> aplicativos = new List<AsignacionAplicativosUsuario>();

            if (name.Equals("SinEscalar"))
            {
                aplicativos = db.AsignacionAplicativos
                                                 .Include(x => x.User)
                                                 .Include(x => x.AplicativoUsuario)
                                                 .Include(x => x.EstadoTramite)
                                                 .Where(x => x.FechaReporteBloqueoApp != null && x.EstadoAplicativo == false && x.User.Status)
                                                 .OrderBy(x => x.FechaReporteBloqueoApp)
                                                 .ToList();
                ViewBag.estado = "SIN ESCALAR";


            }
            else if (name.Equals("EnTramite"))
            {
                aplicativos = db.AsignacionAplicativos
                                                 .Include(x => x.User)
                                                 .Include(x => x.AplicativoUsuario)
                                                 .Include(x => x.EstadoTramite)
                                                 .Where(x => x.FechaReporteBloqueoApp != null && x.EstadoAplicativo == false && x.EstadoTramiteId == 2 && x.User.Status)
                                                 .OrderBy(x => x.FechaReporteBloqueoApp)
                                                 .ToList();
                ViewBag.estado = "EN TRÁMITE";


            }
            else
            {
                aplicativos = db.AsignacionAplicativos
                                                 .Include(x => x.User)
                                                 .Include(x => x.AplicativoUsuario)
                                                 .Include(x => x.EstadoTramite)
                                                 .Where(x => x.FechaReporteBloqueoApp != null && x.EstadoAplicativo == true && x.EstadoTramiteId == 3 && x.User.Status)
                                                 .OrderBy(x => x.FechaReporteBloqueoApp)
                                                 .ToList();
                ViewBag.estado = "SOLUCIONADOS";


            }

            return View(RetornarAplicativos(aplicativos));

        }

        public List<EstadoTramitesVM> RetornarAplicativos(List<AsignacionAplicativosUsuario> aplicativos)
        {
            List<EstadoTramitesVM> estadoTramites = new List<EstadoTramitesVM>();

            foreach (var item in aplicativos)
            {
                EstadoTramitesVM estadoTramite = new EstadoTramitesVM();

                estadoTramite.Documento = item.Documento;
                estadoTramite.DuracionTramite = RetornarDiasDiferencia(item);
                estadoTramite.EstadoTramite = item.EstadoTramite == null ? "" : item.EstadoTramite.Nombre;
                estadoTramite.NombreApp = item.AplicativoUsuario == null ? "" : item.AplicativoUsuario.NombreApp;
                estadoTramite.FechaUltimoBloqueo = item.FechaReporteBloqueoApp;
                estadoTramite.FechaEstadoTramite = item.FechaEstadoTramite;
                estadoTramite.Observacion = item.Observacion;
                estadoTramite.Nombre = item.User == null ? "" : item.User.Names;              

                estadoTramites.Add(estadoTramite);

            }

            return estadoTramites;
        }

        public int RetornarTotales(string name)
        {
            int total = 0;

            if (name.Equals("SinEscalar"))
            {
                total = db.AsignacionAplicativos
                                                 .Include(x => x.User)
                                                 .Where(x => x.FechaReporteBloqueoApp != null && x.EstadoAplicativo == false && x.User.Status)
                                                 .Count();

            }
            else if (name.Equals("EnTramite"))
            {
                total = db.AsignacionAplicativos
                                                 .Include(x => x.User)
                                                 .Where(x => x.FechaReporteBloqueoApp != null && x.EstadoAplicativo == false && x.EstadoTramiteId == 2 && x.User.Status)
                                                 .Count();

            }
            else
            {
                total = db.AsignacionAplicativos
                                                 .Include(x => x.User)
                                                 .Where(x => x.FechaReporteBloqueoApp != null && x.EstadoAplicativo == true && x.EstadoTramiteId == 3 && x.User.Status)
                                                 .OrderBy(x => x.FechaReporteBloqueoApp)
                                                 .Count();

            }

            return total;
        }

    }


}





