﻿using ClaroGrandesSuperficies.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClaroGrandesSuperficies.Controllers
{
    public class DeleteUsuariosController : Controller
    {

        private ContextModel db = new ContextModel();

        // GET: CargueUsuarios
        public ActionResult Index()
        {
            return View();
        }
      

        public JsonResult CargarArchivoUsuarios()
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;

                    HttpPostedFileBase file = files[0];
                    string fileName = file.FileName;

                    EliminarArchivosResources();

                    string path = Path.Combine(Server.MapPath("~/Resources"),
                                  Path.GetFileName(file.FileName));
                    file.SaveAs(path);

                    var dt = CargarExcel(path);

                    BulkUsuarios(dt);


                    return Json(new { status = 201, message = "Los Usuarios se han eliminado correctamente!" });

                }

                catch (Exception e)
                {
                    return Json(new { status = 504, message = e.Message });
                }
            }

            return Json("Por favor seleccione el archivo");
        }


        public void EliminarArchivosResources()
        {
            try
            {
                var pathResources = Path.Combine(Server.MapPath("~/Resources").ToString());

                DirectoryInfo di = new DirectoryInfo(pathResources);

                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable CargarExcel(string cadenaConexion)
        {

            var strconn = ("Provider=Microsoft.ACE.OLEDB.12.0;" +
                 ("Data Source=" + (cadenaConexion + ";Extended Properties=\"Excel 12.0;HDR=YES\"")));

            DataTable dataTable = new DataTable();


            OleDbConnection mconn = new OleDbConnection(strconn);
            mconn.Open();
            DataTable dtSchema = mconn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

            if ((null != dtSchema) && (dtSchema.Rows.Count > 0))
            {
                string firstSheetName = dtSchema.Rows[0]["TABLE_NAME"].ToString();
                new OleDbDataAdapter("SELECT * FROM [" + firstSheetName + "]", mconn).Fill(dataTable);
            }
            mconn.Close();

            return dataTable;
        }

        public void BulkUsuarios(DataTable dtUsuarios)
        {
            TruncarTablaTemporal();

            using (SqlBulkCopy bulkcopy = new SqlBulkCopy(db.Database.Connection.ConnectionString))
            {
                bulkcopy.DestinationTableName = "TemporaryUserPasswords";
                bulkcopy.ColumnMappings.Add("Cedula", "Document");
                bulkcopy.ColumnMappings.Add("Nombre", "Name");
                bulkcopy.ColumnMappings.Add("UsuarioRed", "UserRed");
                bulkcopy.ColumnMappings.Add("ClaveUsuarioRed", "PassRed");
                bulkcopy.ColumnMappings.Add("Estado_UsuarioRed", "StatusRed");

                bulkcopy.ColumnMappings.Add("Login", "Login");
                bulkcopy.ColumnMappings.Add("Estado_Login", "StatusLogin");

                bulkcopy.ColumnMappings.Add("Extension", "Extension");
                bulkcopy.ColumnMappings.Add("Estado_Extension", "StatusExtension");

                bulkcopy.ColumnMappings.Add("Usuario_SAC", "UserSac");
                bulkcopy.ColumnMappings.Add("Clave_SAC", "PassSac");
                bulkcopy.ColumnMappings.Add("Estado_SAC", "StatusSac");
                bulkcopy.ColumnMappings.Add("Link_SAC", "LinkSac");

                bulkcopy.ColumnMappings.Add("UsuarioAC", "UserAc");
                bulkcopy.ColumnMappings.Add("ClaveAC", "PassAc");
                bulkcopy.ColumnMappings.Add("EstadoAC", "StatusAc");
                bulkcopy.ColumnMappings.Add("LinkAC", "LinkAc");

                bulkcopy.ColumnMappings.Add("UsuarioPoliedro", "UserPoliedro");
                bulkcopy.ColumnMappings.Add("ClavePoliedro", "PassPoliedro");
                bulkcopy.ColumnMappings.Add("EstadoPoliedro", "StatusPoliedro");
                bulkcopy.ColumnMappings.Add("LinkPoliedro", "LinkPoliedro");

                bulkcopy.ColumnMappings.Add("UsuarioMYAPPS", "UserMyApps");
                bulkcopy.ColumnMappings.Add("ClaveMYAPPS", "PassMyApps");
                bulkcopy.ColumnMappings.Add("EstadoMYAPPS", "StatusMyApps");
                bulkcopy.ColumnMappings.Add("LinkMYAPPS", "LinkMyApps");

                bulkcopy.ColumnMappings.Add("UsuarioAgendamiento", "UserAgendamiento");
                bulkcopy.ColumnMappings.Add("ClaveAgendamiento", "PassAgendamiento");
                bulkcopy.ColumnMappings.Add("EstadoAgendamiento", "StatusAgendamiento");
                bulkcopy.ColumnMappings.Add("LinkAgendamiento", "LinkAgendamiento");

                bulkcopy.ColumnMappings.Add("UsuarioVisor", "UserVisor");
                bulkcopy.ColumnMappings.Add("ClaveVisor", "PassVisor");
                bulkcopy.ColumnMappings.Add("EstadoVisor", "StatusVisor");
                bulkcopy.ColumnMappings.Add("LinkVisor", "LinkVisor");

                bulkcopy.ColumnMappings.Add("UsuarioRR", "UserRr");
                bulkcopy.ColumnMappings.Add("ClaveRR", "PassRr");
                bulkcopy.ColumnMappings.Add("EstadoRR", "StatusRr");
                bulkcopy.ColumnMappings.Add("LinkRR", "LinkRr");

                bulkcopy.ColumnMappings.Add("UsuarioMIENLACE", "UserMiEnlace");
                bulkcopy.ColumnMappings.Add("ClaveMIENLACE", "PassMiEnlace");
                bulkcopy.ColumnMappings.Add("EstadoMIENLACE", "StatusMiEnlace");
                bulkcopy.ColumnMappings.Add("LinkMIENLACE", "LinkMiEnlace");

                bulkcopy.ColumnMappings.Add("Usuario_EN_ENLACE_ISPIRA", "UserEnEnlaceIspira");
                bulkcopy.ColumnMappings.Add("Clave_EN_ENLACE_ISPIRA", "PassEnEnlaceIspira");
                bulkcopy.ColumnMappings.Add("Estado_EN_ENLACE_ISPIRA", "StatusEnEnlaceIspira");
                bulkcopy.ColumnMappings.Add("Link_EN_ENLACE_ISPIRA", "LinkEnEnlaceIspira");

                bulkcopy.ColumnMappings.Add("Usuario_MI_IT", "UserMyIt");
                bulkcopy.ColumnMappings.Add("Clave_MI_IT", "PassMyIt");
                bulkcopy.ColumnMappings.Add("Estado_MI_IT", "StatusIt");
                bulkcopy.ColumnMappings.Add("Link_MI_IT", "LinkIt");

                bulkcopy.ColumnMappings.Add("UsuarioPortalSAC", "UserPortalSac");
                bulkcopy.ColumnMappings.Add("ClavePortalSAC", "PassPortalSac");
                bulkcopy.ColumnMappings.Add("EstadoPortalSAC", "StatusPortalSac");
                bulkcopy.ColumnMappings.Add("LinkPortalSAC", "LinkPortalSac");

                bulkcopy.ColumnMappings.Add("UsuarioDime", "UserDime");
                bulkcopy.ColumnMappings.Add("ClaveDime", "PassDime");
                bulkcopy.ColumnMappings.Add("EstadoDime", "StatusDime");
                bulkcopy.ColumnMappings.Add("LinkDime", "LinkDime");

                bulkcopy.ColumnMappings.Add("Usuario_ID_VISION", "UserIdVision");
                bulkcopy.ColumnMappings.Add("Clave_ID_VISION", "PassIdVision");
                bulkcopy.ColumnMappings.Add("Estado_ID_VISION", "StatusIdVision");
                bulkcopy.ColumnMappings.Add("Link_ID_VISION", "LinkIdVision");

                bulkcopy.ColumnMappings.Add("Usuario_HelpDesk", "UserHelpDesk");
                bulkcopy.ColumnMappings.Add("Clave_HelpDesk", "PassHelpDesk");
                bulkcopy.ColumnMappings.Add("Estado_HelpDesk", "StatusHelpDesk");
                bulkcopy.ColumnMappings.Add("Link_HelpDesk", "LinkHelpDesk");


                bulkcopy.ColumnMappings.Add("Usuario_Teams", "UserTeams");
                bulkcopy.ColumnMappings.Add("Clave_Teams", "PassTeams");
                bulkcopy.ColumnMappings.Add("Estado_Teams", "StatusTeams");
                bulkcopy.ColumnMappings.Add("LinkTeams", "LinkTeams");


                bulkcopy.ColumnMappings.Add("Usuario_Cifin", "UserCifin");
                bulkcopy.ColumnMappings.Add("Clave_Cifin", "PassCifin");
                bulkcopy.ColumnMappings.Add("Estado_Cifin", "StatusCifin");
                bulkcopy.ColumnMappings.Add("Link_Cifin", "LinkCifin");


                bulkcopy.ColumnMappings.Add("Usuario_Evidente_Master", "UserEvidenteMaster");
                bulkcopy.ColumnMappings.Add("Clave_Evidente_Master", "PassEvidenteMaster");
                bulkcopy.ColumnMappings.Add("Estado_Evidente_Master", "StatusEvidenteMaster");
                bulkcopy.ColumnMappings.Add("Link_Evidente_Master", "LinkEvidenteMaster");


                bulkcopy.ColumnMappings.Add("Usuario_Integracion", "UserIntegracion");
                bulkcopy.ColumnMappings.Add("Clave_Integracion", "PassIntegracion");
                bulkcopy.ColumnMappings.Add("Estado_Integracion", "StatusIntegracion");
                bulkcopy.ColumnMappings.Add("Link_Integracion", "LinkIntegracion");


                try
                {
                    bulkcopy.WriteToServer(dtUsuarios);

                    int errores = ValidarCamposVacios();

                    if (errores > 0)
                    {
                        throw new Exception("El número de documento es obligatorio, por favor verifique los campos vacios en el archivo excel..");
                    }
                    else
                    {
                        //InsertarUserPasswords();
                        EliminarUsers();
                    }

                }
                catch (Exception ex)
                {

                    return;
                }
            }

        }



        public void TruncarTablaTemporal()
        {

            try
            {
                SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString);

                SqlCommand comandoInsert = new SqlCommand("TRUNCATE TABLE TemporaryUserPasswords", connection);

                comandoInsert.CommandType = CommandType.Text;

                // Add the input parameter and set its properties.
                SqlParameter parameter = new SqlParameter();

                comandoInsert.Parameters.Clear();

                connection.Open();
                SqlDataReader reader = comandoInsert.ExecuteReader();

                connection.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("Error al ejecutar procedimiento almacenado. \n" + ex.Message.ToString());
            }
        }

        public void EliminarUsers()
        {
            try
            {
                SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString);

                SqlCommand comandoInsert = new SqlCommand("SP_DeleteUsers", connection);

                comandoInsert.CommandType = CommandType.StoredProcedure;

                // Add the input parameter and set its properties.
                SqlParameter parameter = new SqlParameter();

                comandoInsert.Parameters.Clear();

                connection.Open();
                SqlDataReader reader = comandoInsert.ExecuteReader();

                connection.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("Error al ejecutar procedimiento almacenado. \n" + ex.Message.ToString());
            }
        }



        public int ValidarCamposVacios()
        {
            try
            {
                int count = 0;
                SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString);

                SqlCommand comando = new SqlCommand("P_ValidarCamposVaciosUserPasswords", connection);

                comando.CommandType = CommandType.StoredProcedure;

                // Add the input parameter and set its properties.
                SqlParameter parameter = new SqlParameter();

                comando.Parameters.Clear();

                connection.Open();
                SqlDataReader reader = comando.ExecuteReader();

                while (reader.Read())
                {
                    count = int.Parse(reader[0].ToString());

                }

                connection.Close();

                return count;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al ejecutar procedimiento almacenado. \n" + ex.Message.ToString());
            }
        }

    }
}