﻿using cifrado_CypherHelper;
using ClaroGrandesSuperficies.BusinessLogic;
using ClaroGrandesSuperficies.Constantes;
using ClaroGrandesSuperficies.Models;
using ClaroGrandesSuperficies.Models.UserManagement;
using LinqToExcel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ClaroGrandesSuperficies.Controllers
{
    public class CargueUsuariosController : Controller
    {

        private ContextModel db = new ContextModel();

        CargueUsuariosAplicativosBS cargueUsuariosAplicativosBS = new CargueUsuariosAplicativosBS();

        // GET: CargueUsuarios
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult IndexDelete()
        {
            return View();
        }



        public JsonResult CargarArchivoUsuarios()
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;

                    HttpPostedFileBase file = files[0];
                    string fileName = file.FileName;

                    EliminarArchivosResources();

                    string path = Path.Combine(Server.MapPath("~/Resources/CargueAppUsuario"),
                                  Path.GetFileName(file.FileName));
                    file.SaveAs(path);

                    var dt = cargueUsuariosAplicativosBS.InsertarUsuariosAplicativos(path);

                    return Json(new { status = 201, message = "El archivo se ha cargado correctamente!" });


                }

                catch (Exception ex)
                {
                    return Json(new { status = 504, message = ex.Message });
                }
            }

            return Json("Por favor seleccione el archivo");
        }


        public void EliminarArchivosResources()
        {
            try
            {
                var pathResources = Path.Combine(Server.MapPath("~/Resources/CargueAppUsuario").ToString());

                DirectoryInfo di = new DirectoryInfo(pathResources);

                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();

                }
            }
            catch (Exception ex)            {
               
                throw new Exception();
            }
        }
    }
}