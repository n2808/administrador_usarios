﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClaroGrandesSuperficies.Models;
using ClaroGrandesSuperficies.Models.UserManagement;
using Core.Models.Security;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace ClaroGrandesSuperficies.Controllers
{
    public class UserPasswordsController : Controller
    {
        private ContextModel db = new ContextModel();

        // GET: UserPasswords
        public ActionResult Index()
        {
            var userSession = SessionPersister.Id;
            var document = db.Users.Where(x => x.Id == userSession && x.Status).Select(x => x.Document).FirstOrDefault();

            if (document == null)
            {
                return RedirectToAction("Index", "Accounts");
            }

            var userPass = db.UserPasswords.Where(x => x.Document.Trim() == document.Trim()).FirstOrDefault();

            if (userPass == null)
            {

                if (SessionPersister.HasRol("Administrador"))
                {
                    TempData["Message"] = "El Usuario no posee registros en el aplicativo, debe descargar el formato y subirlos por este medio."; 
                    return RedirectToAction("Index", "CargueUsuarios");
                }
                else
                {
                    TempData["Message"] = "El Usuario no posee registros en el aplicativo, consulte con el Administrador.";
                    SessionPersister.UserName = string.Empty;
                    SessionPersister.Id = null;
                    return RedirectToAction("Index", "Accounts");
                }                            

            }

            return View(userPass);
        }

        [HttpGet]
        public JsonResult LockUser(string app, int id)
        {
            if (string.IsNullOrEmpty(app))
            {
                return Json(new { status = 500, message = "Error, consulte con el Administrador" }, JsonRequestBehavior.AllowGet);
            }

            UserPassword userPassword = new UserPassword();


            switch (app)
            {
                case "Red":

                    userPassword = db.UserPasswords.Where(x => x.Id == id).FirstOrDefault();
                    userPassword.StatusRed = "Bloqueado";
                    try
                    {
                        db.Entry(userPassword).State = EntityState.Modified;
                        db.SaveChanges();
                        break;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }


                case "Sac":

                    userPassword = db.UserPasswords.Where(x => x.Id == id).FirstOrDefault();
                    userPassword.StatusSac = "Bloqueado";

                    try
                    {
                        db.Entry(userPassword).State = EntityState.Modified;
                        db.SaveChanges();
                        break;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }


                case "Login":



                    userPassword = db.UserPasswords.Where(x => x.Id == id).FirstOrDefault();
                    userPassword.StatusLogin = "Bloqueado";

                    try
                    {
                        db.Entry(userPassword).State = EntityState.Modified;
                        db.SaveChanges();
                        break;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }


                case "Extension":

                    userPassword = db.UserPasswords.Where(x => x.Id == id).FirstOrDefault();
                    userPassword.StatusExtension = "Bloqueado";

                    try
                    {
                        db.Entry(userPassword).State = EntityState.Modified;
                        db.SaveChanges();
                        break;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }


                case "Ac":

                    userPassword = db.UserPasswords.Where(x => x.Id == id).FirstOrDefault();
                    userPassword.StatusAc = "Bloqueado";

                    try
                    {
                        db.Entry(userPassword).State = EntityState.Modified;
                        db.SaveChanges();
                        break;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }


                case "Poliedro":

                    userPassword = db.UserPasswords.Where(x => x.Id == id).FirstOrDefault();
                    userPassword.StatusPoliedro = "Bloqueado";

                    try
                    {
                        db.Entry(userPassword).State = EntityState.Modified;
                        db.SaveChanges();
                        break;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }


                case "MyApps":

                    userPassword = db.UserPasswords.Where(x => x.Id == id).FirstOrDefault();
                    userPassword.StatusMyApps = "Bloqueado";

                    try
                    {
                        db.Entry(userPassword).State = EntityState.Modified;
                        db.SaveChanges();
                        break;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }


                case "Agendamiento":

                    userPassword = db.UserPasswords.Where(x => x.Id == id).FirstOrDefault();
                    userPassword.StatusAgendamiento = "Bloqueado";

                    try
                    {
                        db.Entry(userPassword).State = EntityState.Modified;
                        db.SaveChanges();
                        break;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }


                case "Visor":

                    userPassword = db.UserPasswords.Where(x => x.Id == id).FirstOrDefault();
                    userPassword.StatusVisor = "Bloqueado";

                    try
                    {
                        db.Entry(userPassword).State = EntityState.Modified;
                        db.SaveChanges();
                        break;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }


                case "Rr":

                    userPassword = db.UserPasswords.Where(x => x.Id == id).FirstOrDefault();
                    userPassword.StatusRr = "Bloqueado";

                    try
                    {
                        db.Entry(userPassword).State = EntityState.Modified;
                        db.SaveChanges();
                        break;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                case "MiEnlace":

                    userPassword = db.UserPasswords.Where(x => x.Id == id).FirstOrDefault();
                    userPassword.StatusMiEnlace = "Bloqueado";

                    try
                    {
                        db.Entry(userPassword).State = EntityState.Modified;
                        db.SaveChanges();
                        break;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                case "EnEnlaceIspira":

                    userPassword = db.UserPasswords.Where(x => x.Id == id).FirstOrDefault();
                    userPassword.StatusEnEnlaceIspira = "Bloqueado";

                    try
                    {
                        db.Entry(userPassword).State = EntityState.Modified;
                        db.SaveChanges();
                        break;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                case "MyIt":

                    userPassword = db.UserPasswords.Where(x => x.Id == id).FirstOrDefault();
                    userPassword.StatusIt = "Bloqueado";

                    try
                    {
                        db.Entry(userPassword).State = EntityState.Modified;
                        db.SaveChanges();
                        break;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }


                case "PortalSac":

                    userPassword = db.UserPasswords.Where(x => x.Id == id).FirstOrDefault();
                    userPassword.StatusPortalSac = "Bloqueado";

                    try
                    {
                        db.Entry(userPassword).State = EntityState.Modified;
                        db.SaveChanges();
                        break;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                case "IdVision":

                    userPassword = db.UserPasswords.Where(x => x.Id == id).FirstOrDefault();
                    userPassword.StatusIdVision = "Bloqueado";

                    try
                    {
                        db.Entry(userPassword).State = EntityState.Modified;
                        db.SaveChanges();
                        break;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }



                case "HelpDesk":

                    userPassword = db.UserPasswords.Where(x => x.Id == id).FirstOrDefault();
                    userPassword.StatusHelpDesk = "Bloqueado";

                    try
                    {
                        db.Entry(userPassword).State = EntityState.Modified;
                        db.SaveChanges();
                        break;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }


                case "Cifin":

                    userPassword = db.UserPasswords.Where(x => x.Id == id).FirstOrDefault();
                    userPassword.StatusSac = "Bloqueado";

                    try
                    {
                        db.Entry(userPassword).State = EntityState.Modified;
                        db.SaveChanges();
                        break;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }


                case "EvidenteMaster":

                    userPassword = db.UserPasswords.Where(x => x.Id == id).FirstOrDefault();
                    userPassword.StatusEvidenteMaster = "Bloqueado";

                    try
                    {
                        db.Entry(userPassword).State = EntityState.Modified;
                        db.SaveChanges();
                        break;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }


                case "Integracion":

                    userPassword = db.UserPasswords.Where(x => x.Id == id).FirstOrDefault();
                    userPassword.StatusIntegracion = "Bloqueado";

                    try
                    {
                        db.Entry(userPassword).State = EntityState.Modified;
                        db.SaveChanges();
                        break;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }


            }
            return Json(new { status = 200, message = "El estado fue actualizado con éxito" }, JsonRequestBehavior.AllowGet);
        }


        public void GetBlockedUsers()
        {
            var blockedUsers = db.UserPasswords.Where(x =>
                            x.StatusRed.Equals("Bloqueado") ||
                            x.StatusLogin.Equals("Bloqueado") ||
                            x.StatusExtension.Equals("Bloqueado") ||
                            x.StatusSac.Equals("Bloqueado") ||
                            x.StatusAc.Equals("Bloqueado") ||
                            x.StatusPoliedro.Equals("Bloqueado") ||
                            x.StatusAgendamiento.Equals("Bloqueado") ||
                            x.StatusVisor.Equals("Bloqueado") ||
                            x.StatusRr.Equals("Bloqueado") ||
                            x.StatusMiEnlace.Equals("Bloqueado") ||
                            x.StatusEnEnlaceIspira.Equals("Bloqueado") ||
                            x.StatusPortalSac.Equals("Bloqueado") ||
                            x.StatusIdVision.Equals("Bloqueado") ||
                            x.StatusHelpDesk.Equals("Bloqueado") ||
                            x.StatusCifin.Equals("Bloqueado") ||
                            x.StatusEvidenteMaster.Equals("Bloqueado") ||
                            x.StatusEvidenteMaster.Equals("Bloqueado") ||
                            x.StatusIntegracion.Equals("Bloqueado")
                            ).ToList();

            FileInfo newFile = new FileInfo(Path.Combine(Server.MapPath("~/Resources/UsuariosBloqueados.xlsx")));

            string FileName = string.Empty;

            if (newFile.Exists)
            {
                newFile.Delete();  // ensures we create a new workbook
                newFile = new FileInfo(Path.Combine(Server.MapPath("~/Resources/UsuariosBloqueados.xlsx")));
            }
                     

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage pck = new ExcelPackage(newFile);

          

            try
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Report");

                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#B7DEE8");

                ws.Cells["A1:CC1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells["A1:CC1"].Style.Fill.BackgroundColor.SetColor(colFromHex);

                ws.Cells["A1"].Value = "Cedula";
                ws.Cells["B1"].Value = "Nombre";
                ws.Cells["C1"].Value = "UsuarioRed";
                ws.Cells["D1"].Value = "ClaveUsuarioRed";
                ws.Cells["E1"].Value = "Estado_UsuarioRed";
                ws.Cells["F1"].Value = "Login";
                ws.Cells["G1"].Value = "Estado_Login";
                ws.Cells["H1"].Value = "Extension";
                ws.Cells["I1"].Value = "Estado_Extension";
                ws.Cells["J1"].Value = "Usuario_SAC";
                ws.Cells["K1"].Value = "Clave_SAC";
                ws.Cells["L1"].Value = "Estado_SAC";
                ws.Cells["M1"].Value = "Link_SAC";
                ws.Cells["N1"].Value = "UsuarioAC";
                ws.Cells["O1"].Value = "ClaveAC";
                ws.Cells["P1"].Value = "EstadoAC";
                ws.Cells["Q1"].Value = "LinkAC";
                ws.Cells["R1"].Value = "UsuarioPoliedro";
                ws.Cells["S1"].Value = "ClavePoliedro";
                ws.Cells["T1"].Value = "EstadoPoliedro";
                ws.Cells["U1"].Value = "LinkPoliedro";
                ws.Cells["V1"].Value = "UsuarioMYAPPS";
                ws.Cells["W1"].Value = "ClaveMYAPPS";
                ws.Cells["X1"].Value = "EstadoMYAPPS";
                ws.Cells["Y1"].Value = "LinkMYAPPS";
                ws.Cells["Z1"].Value = "UsuarioAgendamiento";
                ws.Cells["AA1"].Value = "ClaveAgendamiento";
                ws.Cells["AB1"].Value = "EstadoAgendamiento";
                ws.Cells["AC1"].Value = "LinkAgendamiento";
                ws.Cells["AD1"].Value = "UsuarioVisor";
                ws.Cells["AE1"].Value = "ClaveVisor";
                ws.Cells["AF1"].Value = "EstadoVisor";
                ws.Cells["AG1"].Value = "LinkVisor";
                ws.Cells["AH1"].Value = "UsuarioRR";
                ws.Cells["AI1"].Value = "ClaveRR";
                ws.Cells["AJ1"].Value = "EstadoRR";
                ws.Cells["AK1"].Value = "LinkRR";
                ws.Cells["AL1"].Value = "UsuarioMIENLACE";
                ws.Cells["AM1"].Value = "ClaveMIENLACE";
                ws.Cells["AN1"].Value = "EstadoMIENLACE";
                ws.Cells["AO1"].Value = "LinkMIENLACE";
                ws.Cells["AP1"].Value = "Usuario_EN_ENLACE_ISPIRA";
                ws.Cells["AQ1"].Value = "Clave_EN_ENLACE_ISPIRA";
                ws.Cells["AR1"].Value = "Estado_EN_ENLACE_ISPIRA";
                ws.Cells["AS1"].Value = "Link_EN_ENLACE_ISPIRA";
                ws.Cells["AT1"].Value = "Usuario_MI_IT";
                ws.Cells["AU1"].Value = "Clave_MI_IT";
                ws.Cells["AV1"].Value = "Estado_MI_IT";
                ws.Cells["AW1"].Value = "Link_MI_IT";
                ws.Cells["AX1"].Value = "UsuarioPortalSAC";
                ws.Cells["AY1"].Value = "ClavePortalSAC";
                ws.Cells["AZ1"].Value = "EstadoPortalSAC";
                ws.Cells["BA1"].Value = "LinkPortalSAC";
                ws.Cells["BB1"].Value = "UsuarioDime";
                ws.Cells["BC1"].Value = "ClaveDime";
                ws.Cells["BD1"].Value = "EstadoDime";
                ws.Cells["BE1"].Value = "LinkDime";
                ws.Cells["BF1"].Value = "Usuario_ID_VISION";
                ws.Cells["BG1"].Value = "Clave_ID_VISION";
                ws.Cells["BH1"].Value = "Estado_ID_VISION";
                ws.Cells["BI1"].Value = "Link_ID_VISION";
                ws.Cells["BJ1"].Value = "Usuario_HelpDesk";
                ws.Cells["BK1"].Value = "Clave_HelpDesk";
                ws.Cells["BL1"].Value = "Estado_HelpDesk";
                ws.Cells["BM1"].Value = "Link_HelpDesk";
                ws.Cells["BN1"].Value = "Usuario_Teams";
                ws.Cells["BO1"].Value = "Clave_Teams";
                ws.Cells["BP1"].Value = "Estado_Teams";
                ws.Cells["BQ1"].Value = "LinkTeams";
                ws.Cells["BR1"].Value = "Usuario_Cifin";
                ws.Cells["BS1"].Value = "Clave_Cifin";
                ws.Cells["BT1"].Value = "Estado_Cifin";
                ws.Cells["BU1"].Value = "Link_Cifin";
                ws.Cells["BV1"].Value = "Usuario_Evidente_Master";
                ws.Cells["BW1"].Value = "Clave_Evidente_Master";
                ws.Cells["BX1"].Value = "Estado_Evidente_Master";
                ws.Cells["BY1"].Value = "Link_Evidente_Master";
                ws.Cells["BZ1"].Value = "Usuario_Integracion";
                ws.Cells["CA1"].Value = "Clave_Integracion";
                ws.Cells["CB1"].Value = "Estado_Integracion";
                ws.Cells["CC1"].Value = "Link_Integracion";

             



                int loop = 1;
                foreach (var item in blockedUsers)
                {
                    loop++;
                    ws.Cells["A" + loop].Value = item.Document == null ? "N/A" : item.Document;
                    ws.Cells["B" + loop].Value = item.Name == null ? "N/A" : item.Name;
                    ws.Cells["C" + loop].Value = item.UserRed == null ? "N/A" : item.UserRed;
                    ws.Cells["D" + loop].Value = item.PassRed == null ? "N/A" : item.PassRed;
                    ws.Cells["E" + loop].Value = item.StatusRed == null ? "N/A" : item.StatusRed;

                    ws.Cells["F" + loop].Value = item.Login == null ? "N/A" : item.Login;
                    ws.Cells["G" + loop].Value = item.StatusLogin == null ? "N/A" : item.StatusLogin;

                    ws.Cells["H" + loop].Value = item.Extension == null ? "N/A" : item.Extension;
                    ws.Cells["I" + loop].Value = item.StatusExtension == null ? "N/A" : item.StatusExtension;

                    ws.Cells["J" + loop].Value = item.UserSac == null ? "N/A" : item.UserSac;
                    ws.Cells["K" + loop].Value = item.PassSac == null ? "N/A" : item.PassSac;
                    ws.Cells["L" + loop].Value = item.StatusSac == null ? "N/A" : item.StatusSac;
                    ws.Cells["M" + loop].Value = item.LinkSac == null ? "N/A" : item.LinkSac;

                    ws.Cells["N" + loop].Value = item.UserAc == null ? "N/A" : item.UserAc;
                    ws.Cells["O" + loop].Value = item.PassAc == null ? "N/A" : item.PassAc;
                    ws.Cells["P" + loop].Value = item.StatusAc == null ? "N/A" : item.StatusAc;
                    ws.Cells["Q" + loop].Value = item.LinkAc == null ? "N/A" : item.LinkAc;

                    ws.Cells["R" + loop].Value = item.UserPoliedro == null ? "N/A" : item.UserPoliedro;
                    ws.Cells["S" + loop].Value = item.PassPoliedro == null ? "N/A" : item.PassPoliedro;
                    ws.Cells["T" + loop].Value = item.StatusPoliedro == null ? "N/A" : item.StatusPoliedro;
                    ws.Cells["U" + loop].Value = item.LinkPoliedro == null ? "N/A" : item.LinkPoliedro;

                    ws.Cells["V" + loop].Value = item.UserMyApps == null ? "N/A" : item.UserMyApps;
                    ws.Cells["W" + loop].Value = item.PassMyApps == null ? "N/A" : item.PassMyApps;
                    ws.Cells["X" + loop].Value = item.StatusMyApps == null ? "N/A" : item.StatusMyApps;
                    ws.Cells["Y" + loop].Value = item.LinkMyApps == null ? "N/A" : item.LinkMyApps;

                    ws.Cells["Z" + loop].Value = item.UserAgendamiento == null ? "N/A" : item.UserAgendamiento;
                    ws.Cells["AA" + loop].Value = item.PassAgendamiento == null ? "N/A" : item.PassAgendamiento;
                    ws.Cells["AB" + loop].Value = item.StatusAgendamiento == null ? "N/A" : item.StatusAgendamiento;
                    ws.Cells["AC" + loop].Value = item.LinkAgendamiento == null ? "N/A" : item.LinkAgendamiento;

                    ws.Cells["AD" + loop].Value = item.UserVisor == null ? "N/A" : item.UserVisor;
                    ws.Cells["AE" + loop].Value = item.PassVisor == null ? "N/A" : item.PassVisor;
                    ws.Cells["AF" + loop].Value = item.StatusVisor == null ? "N/A" : item.StatusVisor;
                    ws.Cells["AG" + loop].Value = item.LinkVisor == null ? "N/A" : item.LinkVisor;

                    ws.Cells["AH" + loop].Value = item.UserRr == null ? "N/A" : item.UserRr;
                    ws.Cells["AI" + loop].Value = item.PassRr == null ? "N/A" : item.PassRr;
                    ws.Cells["AJ" + loop].Value = item.StatusRr == null ? "N/A" : item.StatusRr;
                    ws.Cells["AK" + loop].Value = item.LinkRr == null ? "N/A" : item.LinkRr;

                    ws.Cells["AL" + loop].Value = item.UserMiEnlace == null ? "N/A" : item.UserMiEnlace;
                    ws.Cells["AM" + loop].Value = item.PassMiEnlace == null ? "N/A" : item.PassMiEnlace;
                    ws.Cells["AN" + loop].Value = item.StatusMiEnlace == null ? "N/A" : item.StatusMiEnlace;
                    ws.Cells["AO" + loop].Value = item.LinkMiEnlace == null ? "N/A" : item.LinkMiEnlace;

                    ws.Cells["AP" + loop].Value = item.UserEnEnlaceIspira == null ? "N/A" : item.UserEnEnlaceIspira;
                    ws.Cells["AQ" + loop].Value = item.PassEnEnlaceIspira == null ? "N/A" : item.PassEnEnlaceIspira;
                    ws.Cells["AR" + loop].Value = item.StatusEnEnlaceIspira == null ? "N/A" : item.StatusEnEnlaceIspira;
                    ws.Cells["AS" + loop].Value = item.LinkEnEnlaceIspira == null ? "N/A" : item.LinkEnEnlaceIspira;

                    ws.Cells["AT" + loop].Value = item.UserMyIt == null ? "N/A" : item.UserMyIt;
                    ws.Cells["AU" + loop].Value = item.PassMyIt == null ? "N/A" : item.PassMyIt;
                    ws.Cells["AV" + loop].Value = item.StatusIt == null ? "N/A" : item.StatusIt;
                    ws.Cells["AW" + loop].Value = item.LinkIt == null ? "N/A" : item.LinkIt;

                    ws.Cells["AX" + loop].Value = item.UserPortalSac == null ? "N/A" : item.UserPortalSac;
                    ws.Cells["AY" + loop].Value = item.PassPortalSac == null ? "N/A" : item.PassPortalSac;
                    ws.Cells["AZ" + loop].Value = item.StatusPortalSac == null ? "N/A" : item.StatusPortalSac;
                    ws.Cells["BA" + loop].Value = item.LinkPortalSac == null ? "N/A" : item.LinkPortalSac;

                    ws.Cells["BB" + loop].Value = item.UserDime == null ? "N/A" : item.UserDime;
                    ws.Cells["BC" + loop].Value = item.PassDime == null ? "N/A" : item.PassDime;
                    ws.Cells["BD" + loop].Value = item.StatusDime == null ? "N/A" : item.StatusDime;
                    ws.Cells["BE" + loop].Value = item.LinkDime == null ? "N/A" : item.LinkDime;

                    ws.Cells["BF" + loop].Value = item.UserIdVision == null ? "N/A" : item.UserIdVision;
                    ws.Cells["BG" + loop].Value = item.PassIdVision == null ? "N/A" : item.PassIdVision;
                    ws.Cells["BH" + loop].Value = item.StatusIdVision == null ? "N/A" : item.StatusIdVision;
                    ws.Cells["BI" + loop].Value = item.LinkIdVision == null ? "N/A" : item.LinkIdVision;

                    ws.Cells["BJ" + loop].Value = item.UserHelpDesk == null ? "N/A" : item.UserHelpDesk;
                    ws.Cells["BK" + loop].Value = item.PassHelpDesk == null ? "N/A" : item.PassHelpDesk;
                    ws.Cells["BL" + loop].Value = item.StatusHelpDesk == null ? "N/A" : item.StatusHelpDesk;
                    ws.Cells["BM" + loop].Value = item.LinkHelpDesk == null ? "N/A" : item.LinkHelpDesk;

                    ws.Cells["BN" + loop].Value = item.UserTeams == null ? "N/A" : item.UserTeams;
                    ws.Cells["BO" + loop].Value = item.PassTeams == null ? "N/A" : item.PassTeams;
                    ws.Cells["BP" + loop].Value = item.StatusTeams == null ? "N/A" : item.StatusTeams;
                    ws.Cells["BQ" + loop].Value = item.LinkTeams == null ? "N/A" : item.LinkTeams;

                    ws.Cells["BR" + loop].Value = item.UserCifin == null ? "N/A" : item.UserCifin;
                    ws.Cells["BS" + loop].Value = item.PassCifin == null ? "N/A" : item.PassCifin;
                    ws.Cells["BT" + loop].Value = item.StatusCifin == null ? "N/A" : item.StatusCifin;
                    ws.Cells["BU" + loop].Value = item.LinkCifin == null ? "N/A" : item.LinkCifin;

                    ws.Cells["BV" + loop].Value = item.UserEvidenteMaster == null ? "N/A" : item.UserEvidenteMaster;
                    ws.Cells["BW" + loop].Value = item.PassEvidenteMaster == null ? "N/A" : item.PassEvidenteMaster;
                    ws.Cells["BX" + loop].Value = item.StatusEvidenteMaster == null ? "N/A" : item.StatusEvidenteMaster;
                    ws.Cells["BY" + loop].Value = item.LinkEvidenteMaster == null ? "N/A" : item.LinkEvidenteMaster;

                    ws.Cells["BZ" + loop].Value = item.UserIntegracion == null ? "N/A" : item.UserIntegracion;
                    ws.Cells["CA" + loop].Value = item.PassIntegracion == null ? "N/A" : item.PassIntegracion;
                    ws.Cells["CB" + loop].Value = item.StatusIntegracion == null ? "N/A" : item.StatusIntegracion;
                    ws.Cells["CC" + loop].Value = item.LinkIntegracion == null ? "N/A" : item.LinkIntegracion;

                }



               
                ws.Cells["A:AZ"].AutoFitColumns();
                pck.Save();

                Response.ClearContent();
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment; filename=UsuariosBloqueados.xlsx");
                Response.BinaryWrite(pck.GetAsByteArray());
                Response.End();

              

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


            
        }

        public ActionResult GetTotalBlockedApplications()
        {
            var appsBlocked = db.Database.SqlQuery<BlockedApplication>("SP_GetTotalBlockedApplications").ToList();


            return View(appsBlocked.OrderByDescending(x => x.Quantity));
        }

        public void GetHistoryUserPassword()
        {
            var blockedUsers = db.UserPasswords.ToList();

            FileInfo newFile = new FileInfo(Path.Combine(Server.MapPath("~/Resources/HistoricoUsuarios.xlsx")));

            string FileName = string.Empty;

            if (newFile.Exists)
            {
                newFile.Delete();  // ensures we create a new workbook
                newFile = new FileInfo(Path.Combine(Server.MapPath("~/Resources/HistoricoUsuarios.xlsx")));
            }


            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage pck = new ExcelPackage(newFile);



            try
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Report");

                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#B7DEE8");

                ws.Cells["A1:CC1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells["A1:CC1"].Style.Fill.BackgroundColor.SetColor(colFromHex);

                ws.Cells["A1"].Value = "Cedula";
                ws.Cells["B1"].Value = "Nombre";
                ws.Cells["C1"].Value = "UsuarioRed";
                ws.Cells["D1"].Value = "ClaveUsuarioRed";
                ws.Cells["E1"].Value = "Estado_UsuarioRed";
                ws.Cells["F1"].Value = "Login";
                ws.Cells["G1"].Value = "Estado_Login";
                ws.Cells["H1"].Value = "Extension";
                ws.Cells["I1"].Value = "Estado_Extension";
                ws.Cells["J1"].Value = "Usuario_SAC";
                ws.Cells["K1"].Value = "Clave_SAC";
                ws.Cells["L1"].Value = "Estado_SAC";
                ws.Cells["M1"].Value = "Link_SAC";
                ws.Cells["N1"].Value = "UsuarioAC";
                ws.Cells["O1"].Value = "ClaveAC";
                ws.Cells["P1"].Value = "EstadoAC";
                ws.Cells["Q1"].Value = "LinkAC";
                ws.Cells["R1"].Value = "UsuarioPoliedro";
                ws.Cells["S1"].Value = "ClavePoliedro";
                ws.Cells["T1"].Value = "EstadoPoliedro";
                ws.Cells["U1"].Value = "LinkPoliedro";
                ws.Cells["V1"].Value = "UsuarioMYAPPS";
                ws.Cells["W1"].Value = "ClaveMYAPPS";
                ws.Cells["X1"].Value = "EstadoMYAPPS";
                ws.Cells["Y1"].Value = "LinkMYAPPS";
                ws.Cells["Z1"].Value = "UsuarioAgendamiento";
                ws.Cells["AA1"].Value = "ClaveAgendamiento";
                ws.Cells["AB1"].Value = "EstadoAgendamiento";
                ws.Cells["AC1"].Value = "LinkAgendamiento";
                ws.Cells["AD1"].Value = "UsuarioVisor";
                ws.Cells["AE1"].Value = "ClaveVisor";
                ws.Cells["AF1"].Value = "EstadoVisor";
                ws.Cells["AG1"].Value = "LinkVisor";
                ws.Cells["AH1"].Value = "UsuarioRR";
                ws.Cells["AI1"].Value = "ClaveRR";
                ws.Cells["AJ1"].Value = "EstadoRR";
                ws.Cells["AK1"].Value = "LinkRR";
                ws.Cells["AL1"].Value = "UsuarioMIENLACE";
                ws.Cells["AM1"].Value = "ClaveMIENLACE";
                ws.Cells["AN1"].Value = "EstadoMIENLACE";
                ws.Cells["AO1"].Value = "LinkMIENLACE";
                ws.Cells["AP1"].Value = "Usuario_EN_ENLACE_ISPIRA";
                ws.Cells["AQ1"].Value = "Clave_EN_ENLACE_ISPIRA";
                ws.Cells["AR1"].Value = "Estado_EN_ENLACE_ISPIRA";
                ws.Cells["AS1"].Value = "Link_EN_ENLACE_ISPIRA";
                ws.Cells["AT1"].Value = "Usuario_MI_IT";
                ws.Cells["AU1"].Value = "Clave_MI_IT";
                ws.Cells["AV1"].Value = "Estado_MI_IT";
                ws.Cells["AW1"].Value = "Link_MI_IT";
                ws.Cells["AX1"].Value = "UsuarioPortalSAC";
                ws.Cells["AY1"].Value = "ClavePortalSAC";
                ws.Cells["AZ1"].Value = "EstadoPortalSAC";
                ws.Cells["BA1"].Value = "LinkPortalSAC";
                ws.Cells["BB1"].Value = "UsuarioDime";
                ws.Cells["BC1"].Value = "ClaveDime";
                ws.Cells["BD1"].Value = "EstadoDime";
                ws.Cells["BE1"].Value = "LinkDime";
                ws.Cells["BF1"].Value = "Usuario_ID_VISION";
                ws.Cells["BG1"].Value = "Clave_ID_VISION";
                ws.Cells["BH1"].Value = "Estado_ID_VISION";
                ws.Cells["BI1"].Value = "Link_ID_VISION";
                ws.Cells["BJ1"].Value = "Usuario_HelpDesk";
                ws.Cells["BK1"].Value = "Clave_HelpDesk";
                ws.Cells["BL1"].Value = "Estado_HelpDesk";
                ws.Cells["BM1"].Value = "Link_HelpDesk";
                ws.Cells["BN1"].Value = "Usuario_Teams";
                ws.Cells["BO1"].Value = "Clave_Teams";
                ws.Cells["BP1"].Value = "Estado_Teams";
                ws.Cells["BQ1"].Value = "LinkTeams";
                ws.Cells["BR1"].Value = "Usuario_Cifin";
                ws.Cells["BS1"].Value = "Clave_Cifin";
                ws.Cells["BT1"].Value = "Estado_Cifin";
                ws.Cells["BU1"].Value = "Link_Cifin";
                ws.Cells["BV1"].Value = "Usuario_Evidente_Master";
                ws.Cells["BW1"].Value = "Clave_Evidente_Master";
                ws.Cells["BX1"].Value = "Estado_Evidente_Master";
                ws.Cells["BY1"].Value = "Link_Evidente_Master";
                ws.Cells["BZ1"].Value = "Usuario_Integracion";
                ws.Cells["CA1"].Value = "Clave_Integracion";
                ws.Cells["CB1"].Value = "Estado_Integracion";
                ws.Cells["CC1"].Value = "Link_Integracion";





                int loop = 1;
                foreach (var item in blockedUsers)
                {
                    loop++;
                    ws.Cells["A" + loop].Value = item.Document == null ? "N/A" : item.Document;
                    ws.Cells["B" + loop].Value = item.Name == null ? "N/A" : item.Name;
                    ws.Cells["C" + loop].Value = item.UserRed == null ? "N/A" : item.UserRed;
                    ws.Cells["D" + loop].Value = item.PassRed == null ? "N/A" : item.PassRed;
                    ws.Cells["E" + loop].Value = item.StatusRed == null ? "N/A" : item.StatusRed;

                    ws.Cells["F" + loop].Value = item.Login == null ? "N/A" : item.Login;
                    ws.Cells["G" + loop].Value = item.StatusLogin == null ? "N/A" : item.StatusLogin;

                    ws.Cells["H" + loop].Value = item.Extension == null ? "N/A" : item.Extension;
                    ws.Cells["I" + loop].Value = item.StatusExtension == null ? "N/A" : item.StatusExtension;

                    ws.Cells["J" + loop].Value = item.UserSac == null ? "N/A" : item.UserSac;
                    ws.Cells["K" + loop].Value = item.PassSac == null ? "N/A" : item.PassSac;
                    ws.Cells["L" + loop].Value = item.StatusSac == null ? "N/A" : item.StatusSac;
                    ws.Cells["M" + loop].Value = item.LinkSac == null ? "N/A" : item.LinkSac;

                    ws.Cells["N" + loop].Value = item.UserAc == null ? "N/A" : item.UserAc;
                    ws.Cells["O" + loop].Value = item.PassAc == null ? "N/A" : item.PassAc;
                    ws.Cells["P" + loop].Value = item.StatusAc == null ? "N/A" : item.StatusAc;
                    ws.Cells["Q" + loop].Value = item.LinkAc == null ? "N/A" : item.LinkAc;

                    ws.Cells["R" + loop].Value = item.UserPoliedro == null ? "N/A" : item.UserPoliedro;
                    ws.Cells["S" + loop].Value = item.PassPoliedro == null ? "N/A" : item.PassPoliedro;
                    ws.Cells["T" + loop].Value = item.StatusPoliedro == null ? "N/A" : item.StatusPoliedro;
                    ws.Cells["U" + loop].Value = item.LinkPoliedro == null ? "N/A" : item.LinkPoliedro;

                    ws.Cells["V" + loop].Value = item.UserMyApps == null ? "N/A" : item.UserMyApps;
                    ws.Cells["W" + loop].Value = item.PassMyApps == null ? "N/A" : item.PassMyApps;
                    ws.Cells["X" + loop].Value = item.StatusMyApps == null ? "N/A" : item.StatusMyApps;
                    ws.Cells["Y" + loop].Value = item.LinkMyApps == null ? "N/A" : item.LinkMyApps;

                    ws.Cells["Z" + loop].Value = item.UserAgendamiento == null ? "N/A" : item.UserAgendamiento;
                    ws.Cells["AA" + loop].Value = item.PassAgendamiento == null ? "N/A" : item.PassAgendamiento;
                    ws.Cells["AB" + loop].Value = item.StatusAgendamiento == null ? "N/A" : item.StatusAgendamiento;
                    ws.Cells["AC" + loop].Value = item.LinkAgendamiento == null ? "N/A" : item.LinkAgendamiento;

                    ws.Cells["AD" + loop].Value = item.UserVisor == null ? "N/A" : item.UserVisor;
                    ws.Cells["AE" + loop].Value = item.PassVisor == null ? "N/A" : item.PassVisor;
                    ws.Cells["AF" + loop].Value = item.StatusVisor == null ? "N/A" : item.StatusVisor;
                    ws.Cells["AG" + loop].Value = item.LinkVisor == null ? "N/A" : item.LinkVisor;

                    ws.Cells["AH" + loop].Value = item.UserRr == null ? "N/A" : item.UserRr;
                    ws.Cells["AI" + loop].Value = item.PassRr == null ? "N/A" : item.PassRr;
                    ws.Cells["AJ" + loop].Value = item.StatusRr == null ? "N/A" : item.StatusRr;
                    ws.Cells["AK" + loop].Value = item.LinkRr == null ? "N/A" : item.LinkRr;

                    ws.Cells["AL" + loop].Value = item.UserMiEnlace == null ? "N/A" : item.UserMiEnlace;
                    ws.Cells["AM" + loop].Value = item.PassMiEnlace == null ? "N/A" : item.PassMiEnlace;
                    ws.Cells["AN" + loop].Value = item.StatusMiEnlace == null ? "N/A" : item.StatusMiEnlace;
                    ws.Cells["AO" + loop].Value = item.LinkMiEnlace == null ? "N/A" : item.LinkMiEnlace;

                    ws.Cells["AP" + loop].Value = item.UserEnEnlaceIspira == null ? "N/A" : item.UserEnEnlaceIspira;
                    ws.Cells["AQ" + loop].Value = item.PassEnEnlaceIspira == null ? "N/A" : item.PassEnEnlaceIspira;
                    ws.Cells["AR" + loop].Value = item.StatusEnEnlaceIspira == null ? "N/A" : item.StatusEnEnlaceIspira;
                    ws.Cells["AS" + loop].Value = item.LinkEnEnlaceIspira == null ? "N/A" : item.LinkEnEnlaceIspira;

                    ws.Cells["AT" + loop].Value = item.UserMyIt == null ? "N/A" : item.UserMyIt;
                    ws.Cells["AU" + loop].Value = item.PassMyIt == null ? "N/A" : item.PassMyIt;
                    ws.Cells["AV" + loop].Value = item.StatusIt == null ? "N/A" : item.StatusIt;
                    ws.Cells["AW" + loop].Value = item.LinkIt == null ? "N/A" : item.LinkIt;

                    ws.Cells["AX" + loop].Value = item.UserPortalSac == null ? "N/A" : item.UserPortalSac;
                    ws.Cells["AY" + loop].Value = item.PassPortalSac == null ? "N/A" : item.PassPortalSac;
                    ws.Cells["AZ" + loop].Value = item.StatusPortalSac == null ? "N/A" : item.StatusPortalSac;
                    ws.Cells["BA" + loop].Value = item.LinkPortalSac == null ? "N/A" : item.LinkPortalSac;

                    ws.Cells["BB" + loop].Value = item.UserDime == null ? "N/A" : item.UserDime;
                    ws.Cells["BC" + loop].Value = item.PassDime == null ? "N/A" : item.PassDime;
                    ws.Cells["BD" + loop].Value = item.StatusDime == null ? "N/A" : item.StatusDime;
                    ws.Cells["BE" + loop].Value = item.LinkDime == null ? "N/A" : item.LinkDime;

                    ws.Cells["BF" + loop].Value = item.UserIdVision == null ? "N/A" : item.UserIdVision;
                    ws.Cells["BG" + loop].Value = item.PassIdVision == null ? "N/A" : item.PassIdVision;
                    ws.Cells["BH" + loop].Value = item.StatusIdVision == null ? "N/A" : item.StatusIdVision;
                    ws.Cells["BI" + loop].Value = item.LinkIdVision == null ? "N/A" : item.LinkIdVision;

                    ws.Cells["BJ" + loop].Value = item.UserHelpDesk == null ? "N/A" : item.UserHelpDesk;
                    ws.Cells["BK" + loop].Value = item.PassHelpDesk == null ? "N/A" : item.PassHelpDesk;
                    ws.Cells["BL" + loop].Value = item.StatusHelpDesk == null ? "N/A" : item.StatusHelpDesk;
                    ws.Cells["BM" + loop].Value = item.LinkHelpDesk == null ? "N/A" : item.LinkHelpDesk;

                    ws.Cells["BN" + loop].Value = item.UserTeams == null ? "N/A" : item.UserTeams;
                    ws.Cells["BO" + loop].Value = item.PassTeams == null ? "N/A" : item.PassTeams;
                    ws.Cells["BP" + loop].Value = item.StatusTeams == null ? "N/A" : item.StatusTeams;
                    ws.Cells["BQ" + loop].Value = item.LinkTeams == null ? "N/A" : item.LinkTeams;

                    ws.Cells["BR" + loop].Value = item.UserCifin == null ? "N/A" : item.UserCifin;
                    ws.Cells["BS" + loop].Value = item.PassCifin == null ? "N/A" : item.PassCifin;
                    ws.Cells["BT" + loop].Value = item.StatusCifin == null ? "N/A" : item.StatusCifin;
                    ws.Cells["BU" + loop].Value = item.LinkCifin == null ? "N/A" : item.LinkCifin;

                    ws.Cells["BV" + loop].Value = item.UserEvidenteMaster == null ? "N/A" : item.UserEvidenteMaster;
                    ws.Cells["BW" + loop].Value = item.PassEvidenteMaster == null ? "N/A" : item.PassEvidenteMaster;
                    ws.Cells["BX" + loop].Value = item.StatusEvidenteMaster == null ? "N/A" : item.StatusEvidenteMaster;
                    ws.Cells["BY" + loop].Value = item.LinkEvidenteMaster == null ? "N/A" : item.LinkEvidenteMaster;

                    ws.Cells["BZ" + loop].Value = item.UserIntegracion == null ? "N/A" : item.UserIntegracion;
                    ws.Cells["CA" + loop].Value = item.PassIntegracion == null ? "N/A" : item.PassIntegracion;
                    ws.Cells["CB" + loop].Value = item.StatusIntegracion == null ? "N/A" : item.StatusIntegracion;
                    ws.Cells["CC" + loop].Value = item.LinkIntegracion == null ? "N/A" : item.LinkIntegracion;

                }

                ws.Cells["A:AZ"].AutoFitColumns();
                pck.Save();

                Response.ClearContent();
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment; filename=UsuariosBloqueados.xlsx");
                Response.BinaryWrite(pck.GetAsByteArray());
                Response.End();



            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
