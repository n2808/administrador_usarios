﻿using ClaroGrandesSuperficies.BusinessLogic;
using ClaroGrandesSuperficies.Constantes;
using ClaroGrandesSuperficies.Models;
using ClaroGrandesSuperficies.Models.AdministracionUsuariosApp;
using ClaroGrandesSuperficies.Models.Logs;
using ClaroGrandesSuperficies.Viewmodel;
using Core.Models.Security;
using Core.Models.User;
using System;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ClaroGrandesSuperficies.Controllers
{
    public class AccountsController : Controller
    {


        private ContextModel db = new ContextModel();
        private ActiveDirectory activeDirectory = new ActiveDirectory();


        // GET: Accounts
        public ActionResult Index()
        {

            if (SessionPersister.Id != null)
                return RedirectToAction("AsignacionAplicativos", "Index", null);

            TempData["Message"] = TempData["Message"];
            return View();
        }



        [HttpPost]
        public async Task<ActionResult> Login(AccountsViewModel avm, string oldPassword, string confirmPassword)
        {

            if (string.IsNullOrEmpty(avm.Accounts.UserName) || string.IsNullOrEmpty(avm.Accounts.Password))
            {
                ViewBag.Error = "El usuario y la contraseña son requeridos";
                return View("Index");
            }

            var response = new UserActiveDirectory();           

            var activeDirectoryFilter = new ActiveDirectoryFilterVO()
            {
                Domain = "UNO27.COM",
                UserName = avm.Accounts.UserName,
                Password = avm.Accounts.Password
            };

            try
            {

                response = await activeDirectory.GetUserActiveDirectoryAsync(activeDirectoryFilter);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("Index");
            }


            // Buscamos el usuario por UserName del Directorio Activo.
            var userQuery = GetUserByUserNameActiveDirectory(response.UserName);
            var userWithApp = userQuery.FirstOrDefault();

            if (userWithApp == null)
            {
                ViewBag.Error = "El usuario que está ingresando no existe, consulte con el administrador.";
                return View("Index");
            }


            // Buscamos si el usuario está registrado en el aplicativo.
            var user = GetUserByUserName(userWithApp.Documento);

            if (user == null)
            {
                ViewBag.Error = "El usuario no se encuentra registrado en el aplicativo, consulte con el administrador.";
                return View("Index");
            }


            if (!VerifyUserMekano(user.Document))
            {
                ViewBag.Error = "El usuario no se encuentra registrado en la Empresa";
                return View("Index");
            }


            #region PROCESO DE LOGUEO ANTERIOR

            // Validación para el cambio de contraseña del usuario.
            //if (oldPassword != null && confirmPassword != null)
            //{
            //    if (!VerifyPassword(avm, oldPassword, confirmPassword))
            //    {
            //        ViewBag.Error = ViewBag.Error;
            //        return View("ResetPassword", avm);

            //    }
            //    else
            //    {
            //        TempData["Message"] = "La contraseña se cambió correctamente";
            //        return View("Index");
            //    }
            //}

            //DateTime dateNow = DateTime.Now;
            //DateTime? dateLogin = db.Users.Where(x => x.Document.Trim() == avm.Accounts.UserName.Trim()
            //                              && x.PassWord.Trim() == avm.Accounts.Password.Trim()
            //                              && x.Status)
            //                              .Select(x => x.LoginDate)
            //                              .FirstOrDefault();

            //// Validación para el cambio de contraseña.
            //if (!ValidateLoginDate(dateNow, dateLogin))
            //{
            //    ViewBag.Error = "El usuario debe cambiar la contraseña";
            //    return View("ResetPassword", avm);
            //}

            //AccountsModel am = new AccountsModel();
            //Accounts ResultUser = am.Login(avm.Accounts.UserName, avm.Accounts.Password);

            //if (string.IsNullOrEmpty(avm.Accounts.UserName) || string.IsNullOrEmpty(avm.Accounts.Password) || ResultUser == null)
            //{
            //    ViewBag.Error = "Usuario o contraseña incorrectos";
            //    return View("Index");
            //}

            //if (!VerifyStatus(avm))
            //{
            //    ViewBag.Error = "El usuario se encuentra inactivo. Consulte con el administrador";
            //    return View("Index");
            //}

            #endregion

            if (userWithApp != null)
            {
                // Editamos la contraseña para el aplicativo AC
                await EditPasswordByUser_App_AC(avm.Accounts.Password);
            }

            // get user data for the session.
            SessionPersister.UserName = user.Document;
            SessionPersister.Id = user.Id;


            InsertLog(SessionPersister.UserName);

            return RedirectToAction("Index", "AsignacionAplicativos", null);

        }



        private bool VerifyStatus(AccountsViewModel avm)
        {
            bool exists = db.Users.Where(c => c.Document == avm.Accounts.UserName && c.PassWord == avm.Accounts.Password && c.Status == true)
                                 .Select(x => x.Status).FirstOrDefault();
            if (exists)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public ActionResult Logout()
        {
            SessionPersister.UserName = string.Empty;
            SessionPersister.Id = null;
            return View("index");
        }

        public bool ValidateLoginDate(DateTime? dateNow, DateTime? dateLogin)
        {

            if (dateLogin == null)
            {
                dateLogin = DateTime.Now;
            }
            TimeSpan? ts = dateNow - dateLogin;
            int diff = ts.Value.Days;

            if (diff <= 30)
            {
                return true;

            }
            else
            {
                return false;

            }
        }


        public bool VerifyPassword(AccountsViewModel avm, string oldPassword, string confirmPassword)
        {

            // Consultamos la contraseña anterior;
            var _oldPassword = db.Users.Where(x => x.Document.Trim() == avm.Accounts.UserName.Trim()
                                       && x.PassWord.Trim() == oldPassword.Trim()
                                       && x.Status)
                                       .Select(x => x.PassWord)
                                       .FirstOrDefault();

            // Verificamos que la contraseña anterior sea válida.
            if (!_oldPassword.Trim().Equals(oldPassword.Trim()))
            {
                ViewBag.Error = "La contraseña anterior no coincide con la digitada";
                return false;
            }

            // Verificamos que las contraseñas ingresadas coincidan.
            if (confirmPassword.Trim() != avm.Accounts.Password.Trim())
            {
                ViewBag.Error = "Las contraseñas no coinciden";
                return false;

            }

            // Verificamos que el usuario no esté ingresando la misma contraseña que tenía.
            if (_oldPassword.Trim().Equals(avm.Accounts.Password.Trim()))
            {
                ViewBag.Error = "Esta contraseña ya fué asignada, ingrese una nueva";
                return false;

            }

            User user = db.Users.Where(x => x.Document.Trim() == avm.Accounts.UserName.Trim()
                                 && x.PassWord.Trim() == oldPassword.Trim()
                                 && x.Status).FirstOrDefault();
            user.LoginDate = DateTime.Now.AddDays(30);
            user.PassWord = avm.Accounts.Password.Trim();
            db.Entry(user).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            return true;
        }

        public void InsertLog(string userName)
        {

            string machineName = string.Empty;
            machineName = Dns.GetHostName();
            IPHostEntry ipHostEntry = Dns.GetHostEntry(machineName);
            IPAddress[] address = ipHostEntry.AddressList;
            var IpAddress = address[1].ToString();

            Log log = new Log();
            log.IpAddress = IpAddress;
            log.Machine = machineName;
            log.DocumentUser = userName;

            db.Logs.Add(log);
            db.SaveChanges();

        }


        public void CreateUser(string userName, string IpAddress, string machineName)
        {
            try
            {
                var userId = db.Users.Where(x => x.Document.Trim() == userName.Trim()).FirstOrDefault().Id;
                LogueoAutomatico logueoAutomatico = new LogueoAutomatico();
                logueoAutomatico.DocumentUser = userName.Trim();
                logueoAutomatico.IdUser = userId;
                logueoAutomatico.IpAddress = IpAddress;
                logueoAutomatico.Machine = machineName;
                db.LogueoAutomaticos.Add(logueoAutomatico);
                db.SaveChanges();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }

        [HttpGet]
        public JsonResult ReturnDataApp(int app, string user)
        {

            var userId = db.AsignacionAplicativos.Where(x => x.UsuarioAplicativo == user).Select(x => x.UserId).FirstOrDefault();
            var _app = db.Database.SqlQuery<AplicativoVM>("SP_Obtener_Usuario_Clave_Aplicativo @IdAplicativo={0}, @IdUsuario={1}", app, userId).FirstOrDefault();

            return Json(new { user = _app.Usuario, pass = _app.ClaveAplicativo, link = _app.Link }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult ResetAccount(string id)
        {

            try
            {

                User user = db.Users.Where(x => x.Document.Trim() == id.Trim()).FirstOrDefault();

                if (user == null)
                {
                    return Json(new { status = 201, message = "El usuario no existe, consulte con el administrador!" }, JsonRequestBehavior.AllowGet);

                }



                user.PassWord = user.Document.Trim();
                user.LoginDate = DateTime.Now.AddDays(-31);
                db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                return Json(new { status = 201, message = "Contraseña reseteada con éxito!" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = 500, message = "Error al resetear la clave consulte con el administrador" + ex.Message });
            }




        }

        public ActionResult AccessDenied()
        {
            return View();
        }

        public bool VerifyUserMekano(string document)
        {

            var Document = document;
            var nameExists = $"FORMACION_{ Document }";
            var nameFormation = db.Users.Where(x => x.Document.Equals(Document)).Select(x => x.Names).FirstOrDefault();
            var exists = db.Database.SqlQuery<Mekano>("SP_ValidacionEmpleados_MekanoCedula @EMP_Documento={0}", Document).Count();

            if (exists > 0 || nameFormation.Equals(nameExists))
            {
                return true;

            }
            else
            {
                return false;

            }
        }

        public ActionResult ResetearPassword(string Documento)
        {
            var usuario = db.Users.Where(x => x.Document.Trim() == Documento.Trim() && x.Status).FirstOrDefault();

            if (usuario == null)
            {
                ViewBag.Error = "El usuario no existe";
                return View(usuario);
            }

            return View(usuario);
        }

        public IQueryable<AsignacionAplicativosUsuario> GetUserByUserNameActiveDirectory(string userName)
        {
            return db.AsignacionAplicativos.Include(x => x.User)
                                                     .Where(x => x.UsuarioAplicativo.Trim() == userName.Trim());


        }

        public User GetUserByUserName(string userName)
        {
            return db.Users.Where(x => x.Document.Trim() == userName.Trim()).FirstOrDefault();

        }

        public async Task EditPasswordByUser_App_AC(string password)
        {

            var id = AplicativosSistema.AC;
            var _connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

            var store = "P_EditarClaveAplicativo";
            SqlCommand command = new SqlCommand(store, _connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlParameter parameter = new SqlParameter();
            command.Parameters.Clear();
            command.Parameters.AddWithValue("@Id", id);
            command.Parameters.AddWithValue("@CLAVE", password);
            _connection.Open();
            SqlDataReader reader = await command.ExecuteReaderAsync();

            _connection.Close();


        }


    }
}

