﻿using AdministracionUsuarios.BusinessLogic;
using cifrado_CypherHelper;
using ClaroGrandesSuperficies.Constantes;
using ClaroGrandesSuperficies.Models;
using ClaroGrandesSuperficies.Models.UserManagement;
using Core.Models.Security;
using LinqToExcel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ClaroGrandesSuperficies.Controllers
{
    public class DesactivacionCargueController : Controller
    {

        private ContextModel db = new ContextModel();
        private ActivacionUsuariosBS activacionUsuariosBS = new ActivacionUsuariosBS() { };
        // GET: CargueUsuarios
        public ActionResult Index()
        {
            return View();
        }


        public JsonResult CargarArchivoUsuarios()
        {
            Guid? Id = SessionPersister.Id;

            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;

                    HttpPostedFileBase file = files[0];
                    string fileName = file.FileName;

                    string path = Path.Combine(Server.MapPath("~/Resources/ActivacionUsuarios"),
                                 Id.ToString() + ".xlsx");
                    file.SaveAs(path);

                    if (!activacionUsuariosBS.InsertarUsuarios(path))
                    {

                        return Json(new { status = 504, message = "El número de documento es obligatorio, por favor verifique los campos vacios en el archivo excel..!" });
                    }
                    else
                    {
                        return Json(new { status = 201, message = "El archivo se ha cargado correctamente!" });
                        
                    }

                }

                catch (Exception e)
                {
                    return Json(new { status = 504, message = Configuraciones.MENSAJE_ERROR_FORMATO_INCORRECTO, errord = e.Message });
                }
            }

            return Json("Por favor seleccione el archivo");
        }










    }
}