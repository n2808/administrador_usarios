﻿using ClaroGrandesSuperficies.Constantes;
using ClaroGrandesSuperficies.Models;
using ClaroGrandesSuperficies.Models.AdministracionUsuariosApp;
using ClaroGrandesSuperficies.Viewmodel;
using Core.Models.Security;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web.Mvc;

namespace ClaroGrandesSuperficies.Controllers
{
    public class AsignacionAplicativosController : Controller
    {

        private ContextModel db = new ContextModel();

        // GET: AsignacionAplicativos
        public ActionResult Index()
        {

            TempData["Mensaje"] = TempData["Mensaje"];

            if (SessionPersister.HasRol("Administrador"))
            {
                return RedirectToAction("Index", "CargueUsuarios");
            }
            else
            {
                var userSession = SessionPersister.Id;
                var idUsuario = db.Users.Where(x => x.Id == userSession && x.Status).Select(x => x.Id).FirstOrDefault();


                if (idUsuario == null)
                {
                    return RedirectToAction("Index", "Accounts");
                }


                var aplicativosUsuario = db.Database.SqlQuery<UsuarioVM>("DecifrarClaveAplicativos @IdUsuario={0}", idUsuario).ToList();

                var totalAplicativosBloqueados = db.AsignacionAplicativos.Where(x => x.EstadoAplicativo == false && x.Documento == SessionPersister.UserName.Trim()).Count();

                if (aplicativosUsuario.Count() <= 0)
                {

                    if (SessionPersister.HasRol("Administrador"))
                    {
                        TempData["Message"] = "El Usuario no posee registros en el aplicativo, debe descargar el formato y subirlos por este medio.";
                        return RedirectToAction("Index", "CargueUsuarios");
                    }
                    else
                    {
                        SessionPersister.UserName = string.Empty;
                        SessionPersister.Id = null;
                        return RedirectToAction("Index", "Accounts");
                    }

                }

                ViewBag.tipificacion = new SelectList(db.Tipificaciones.Where(x => x.Status).OrderBy(x => x.Name), "Id", "Name");

                TempData["Message"] = totalAplicativosBloqueados;

                // Capturar Ip Usuario.
                String IP = Request.UserHostAddress;
                ViewBag.IpPrueba = IP;


                return View(aplicativosUsuario);
            }

        }
               

        // Proceso se suspende por cambio de requerimientos.
        //[HttpGet]
        //public JsonResult BloquearApp(int? id)
        //{
        //    var tipificacion = db.TipificacionAplicativos.Include(x => x.Tipificacion).Where(x => x.AsignacionAplicativosUsuarioId == id).ToList();

        //    return Json(new { status = 200, message = "Ok", data = tipificacion }, JsonRequestBehavior.AllowGet);
        //    //return Json(new { status = 200, message = "Ok" }, JsonRequestBehavior.AllowGet);
        //}


        [HttpGet]
        public JsonResult LockUser(int id, int tipificacion)
        {
            if (id <= 0)
            {
                return Json(new { status = 500, message = "Error, consulte con el Administrador" }, JsonRequestBehavior.AllowGet);
            }

            if (tipificacion <= 0)
            {
                return Json(new { status = 500, message = "Error, Debe seleccionar el motivo de bloqueo" }, JsonRequestBehavior.AllowGet);
            }

            AsignacionAplicativosUsuario userPassword = new AsignacionAplicativosUsuario();


            userPassword = db.AsignacionAplicativos.Where(x => x.Id == id).FirstOrDefault();
            try
            {
                userPassword.TipificacionId = tipificacion;
                userPassword.EstadoAplicativo = false;
                userPassword.FechaReporteBloqueoApp = DateTime.Now;
                userPassword.FechaDesbloqueoApp = null;
                userPassword.FechaEstadoTramite = null;
                userPassword.EstadoTramiteId = 1;
                userPassword.Observacion = null;
                db.Entry(userPassword).State = EntityState.Modified;
                db.SaveChanges();

                LogBloqueoApp log = new LogBloqueoApp()
                {
                    Documento = userPassword.Documento,
                    EstadoAplicativo = false,
                    FechaReporteBloqueoApp = DateTime.Now,
                    IdAplicativo = userPassword.IdAplicativo,
                    TipificacionId = tipificacion,
                    TotalBloqueos = 0,
                    UserId = userPassword.UserId

                };

                db.LogBloqueoApps.Add(log);
                db.SaveChanges();

                var logGuardados = db.LogBloqueoApps.Where(x => x.UserId == userPassword.UserId && x.IdAplicativo == userPassword.IdAplicativo).ToList();
                var numeroBloqueos = logGuardados[0].TotalBloqueos + 1;

                if (logGuardados.Count > 0)
                {
                    foreach (var item in logGuardados)
                    {
                        item.TotalBloqueos = numeroBloqueos;
                        db.Entry(item).State = EntityState.Modified;
                        db.SaveChanges();
                    }


                }


                return Json(new { status = 200, message = "El estado fue actualizado con éxito" }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        /*public bool InsertarTipificacionAplicativo(int id, int tipificacion)
        {
            if (id <= 0 && tipificacion <= 0)
            {
                return false;
            }

            try
            {
                TipificacionAplicativo tipificacionApli = new TipificacionAplicativo();
                tipificacionApli.AsignacionAplicativosUsuarioId = id;
                tipificacionApli.TipificacionId = tipificacion;
                db.TipificacionAplicativos.Add(tipificacionApli);
                db.SaveChanges();
                return true;

            }
            catch (Exception)
            {

                throw;
            }
        }*/


        public JsonResult CambiarContraseñaApp(int? id)
        {
            // Validar que el id no venga vacío.
            if (id <= 0)
            {
                return Json(new { status = 500, message = "Error, consulte con el Administrador" }, JsonRequestBehavior.AllowGet);

            }

            var AppUsuario = db.AsignacionAplicativos.Include(x => x.AplicativoUsuario).Where(x => x.Id == id).FirstOrDefault();

            if (AppUsuario == null)
            {
                return Json(new { status = 500, message = "Error, el usuario no existe, consulte con el administrador" }, JsonRequestBehavior.AllowGet);

            }

            return Json(new { status = 200, message = "Ok", data = AppUsuario }, JsonRequestBehavior.AllowGet);

        }


        public JsonResult EditarContraseñaApp(int? id, string contrasena)
        {
            if (id <= 0 || string.IsNullOrEmpty(contrasena))
            {
                return Json(new { status = 500, message = "error, Intente de nuevo. Si el problema persiste consulte con el administrador." }, JsonRequestBehavior.AllowGet);

            }

            try
            {
                db.Database.SqlQuery<responseVM>("P_EditarClaveAplicativo @Id={0}, @CLAVE={1}", id, contrasena.Trim()).FirstOrDefault();
                return Json(new { status = 200, message = "Ok" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }

        public ActionResult LogueoAutomatico(string UsuarioApp, string ClaveApp, string LinkAplicativo, int IdAplicativo)
        {


            if (ValidarItemsLogueoAutomatico(UsuarioApp, ClaveApp, LinkAplicativo))
            {

                //FirefoxDriverService service = FirefoxDriverService.CreateDefaultService(@"C:\geckodriverFirefox");                                                                                        
                //service.FirefoxBinaryPath = @"C:\Program Files\Mozilla Firefox\firefox.exe";

                //FirefoxOptions firefoxOptions = new FirefoxOptions();
                //IWebDriver driver = new RemoteWebDriver(new Uri("http://www.example.com"), firefoxOptions);
                //TimeSpan time = TimeSpan.FromMinutes(5);
                //IWebDriver driver = new FirefoxDriver(service, options, time);


                IWebDriver driver = new ChromeDriver();
                driver.Navigate().GoToUrl(LinkAplicativo);
                driver.Manage().Window.Maximize();
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMinutes(5);

                switch (IdAplicativo)
                {
                    //EMPIEZA RPA DE LOGIN APLICATIVO SAC

                    case AplicativosSelenium.SAC:

                        try
                        {
                            System.Threading.Thread.Sleep(30000);
                            IWebElement usuario = driver.FindElement(By.Id("contenido-form:usuario"));
                            usuario.SendKeys(UsuarioApp);
                            System.Threading.Thread.Sleep(30000);
                            IWebElement contrasena = driver.FindElement(By.Id("contenido-form:clave"));
                            contrasena.SendKeys(ClaveApp);
                            System.Threading.Thread.Sleep(30000);
                            IWebElement button = driver.FindElement(By.Id("contenido-form:btnIniciarSesión"));
                            button.Click();
                        }
                        catch (Exception ex)
                        {
                            CloseRobot();
                            LogueoAutomatico(UsuarioApp, ClaveApp, LinkAplicativo, IdAplicativo);
                            throw new Exception(ex.Message);
                        }

                        break;

                    //EMPIEZA RPA DE LOGIN APLICATIVO POLIEDRO

                    case AplicativosSelenium.POLIEDRO:
                        try
                        {
                            IWebElement usuario = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtUsuario"));
                            usuario.SendKeys(UsuarioApp);
                            driver.FindElement(By.XPath("/html/body/form/div[3]/div[2]/section/div/div[2]/div[2]/main/div[2]/div/div[1]/fieldset/div[1]/div[3]/div/input")).Click();
                            System.Threading.Thread.Sleep(5000);
                            IWebElement contrasena = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtContraseña"));
                            contrasena.SendKeys(ClaveApp);
                            System.Threading.Thread.Sleep(2000);
                            IWebElement button = driver.FindElement(By.Id("btnIngresarUsuarioContraseña"));
                            button.Click();
                            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(8);
                        }
                        catch (Exception ex)
                        {
                            CloseRobot();
                            LogueoAutomatico(UsuarioApp, ClaveApp, LinkAplicativo, IdAplicativo);
                            throw new Exception(ex.Message);
                        }

                        break;

                    //EMPIEZA RPA DE LOGIN APLICATIVO VISOR

                    case AplicativosSelenium.VISOR:
                        try
                        {
                            IWebElement usuario = driver.FindElement(By.XPath("/html/body/form/div/div/fieldset/p[1]/input"));
                            usuario.SendKeys(UsuarioApp);
                            System.Threading.Thread.Sleep(5000);
                            IWebElement contrasena = driver.FindElement(By.XPath("/html/body/form/div/div/fieldset/p[2]/input"));
                            contrasena.SendKeys(ClaveApp);
                            System.Threading.Thread.Sleep(2000);
                            IWebElement button = driver.FindElement(By.XPath("/html/body/form/div/div/fieldset/p[4]/input"));
                            button.Click();
                        }
                        catch (Exception ex)
                        {
                            CloseRobot();
                            LogueoAutomatico(UsuarioApp, ClaveApp, LinkAplicativo, IdAplicativo);
                            throw new Exception(ex.Message);
                        }
                        break;

                    //EMPIEZA RPA DE LOGIN APLICATIVO MI ENLACE

                    case AplicativosSelenium.MI_ENLACE:
                        try
                        {
                            IWebElement usuario = driver.FindElement(By.Id("txtUserName"));
                            usuario.SendKeys(UsuarioApp);
                            System.Threading.Thread.Sleep(5000);
                            IWebElement contrasena = driver.FindElement(By.Id("txtPassword"));
                            contrasena.SendKeys(ClaveApp);
                            System.Threading.Thread.Sleep(2000);
                            IWebElement button = driver.FindElement(By.Id("btnLogIn"));
                            button.Click();
                        }
                        catch (Exception ex)
                        {
                            CloseRobot();
                            LogueoAutomatico(UsuarioApp, ClaveApp, LinkAplicativo, IdAplicativo);
                            throw new Exception(ex.Message);
                        }
                        break;

                    //EMPIEZA RPA DE LOGIN APLICATIVO EN ENLACE INSPIRA

                    case AplicativosSelenium.EN_ENLACE_INSPIRA:
                        try
                        {
                            IWebElement usuario = driver.FindElement(By.Id("username"));
                            usuario.SendKeys(UsuarioApp);
                            System.Threading.Thread.Sleep(5000);
                            IWebElement contrasena = driver.FindElement(By.Id("password"));
                            contrasena.SendKeys(ClaveApp);
                            System.Threading.Thread.Sleep(2000);
                            IWebElement button = driver.FindElement(By.Id("loginbtn"));
                            button.Click();
                        }
                        catch (Exception ex)
                        {
                            CloseRobot();
                            LogueoAutomatico(UsuarioApp, ClaveApp, LinkAplicativo, IdAplicativo);
                            throw new Exception(ex.Message);
                        }
                        break;

                    //EMPIEZA RPA DE LOGIN APLICATIVO MY IT

                    case AplicativosSelenium.MY_IT:
                        try
                        {
                            IWebElement usuario = driver.FindElement(By.XPath("/html/body/div/div/div/main/form/div[2]/input"));
                            usuario.SendKeys(UsuarioApp);
                            System.Threading.Thread.Sleep(5000);
                            IWebElement contrasena = driver.FindElement(By.XPath("/html/body/div/div/div/main/form/div[3]/input"));
                            contrasena.SendKeys(ClaveApp);
                            System.Threading.Thread.Sleep(2000);
                            IWebElement button = driver.FindElement(By.XPath("/html/body/div/div/div/main/form/button"));
                            button.Click();
                        }
                        catch (Exception ex)
                        {
                            CloseRobot();
                            LogueoAutomatico(UsuarioApp, ClaveApp, LinkAplicativo, IdAplicativo);
                            throw new Exception(ex.Message);
                        }
                        break;

                    //EMPIEZA RPA DE LOGIN APLICATIVO DIME

                    case AplicativosSelenium.DIME:
                        try
                        {
                            IWebElement usuario = driver.FindElement(By.Id("Usuario"));
                            usuario.SendKeys(UsuarioApp);
                            System.Threading.Thread.Sleep(5000);
                            IWebElement contrasena = driver.FindElement(By.Id("Contrase_a"));
                            contrasena.SendKeys(ClaveApp);
                            System.Threading.Thread.Sleep(2000);
                            IWebElement button = driver.FindElement(By.Id("btiniciar"));
                            button.Click();
                        }
                        catch (Exception ex)
                        {
                            CloseRobot();
                            LogueoAutomatico(UsuarioApp, ClaveApp, LinkAplicativo, IdAplicativo);
                            throw new Exception(ex.Message);
                        }
                        break;

                    //EMPIEZA RPA DE LOGIN APLICATIVO VISION

                    case AplicativosSelenium.ID_VISION:
                        try
                        {
                            IWebElement usuario = driver.FindElement(By.Id("cpC_dcLogin_txtUserId"));
                            usuario.SendKeys(UsuarioApp);
                            System.Threading.Thread.Sleep(5000);
                            IWebElement contrasena = driver.FindElement(By.Id("cpC_dcLogin_txtPassword"));
                            contrasena.SendKeys(ClaveApp);
                            System.Threading.Thread.Sleep(2000);
                            IWebElement button = driver.FindElement(By.XPath("/html/body/form/div[3]/div[1]/div[2]/div/div/div[6]/div/div"));
                            button.Click();
                        }
                        catch (Exception ex)
                        {
                            CloseRobot();
                            LogueoAutomatico(UsuarioApp, ClaveApp, LinkAplicativo, IdAplicativo);
                            throw new Exception(ex.Message);
                        }
                        break;

                    //EMPIEZA RPA DE LOGIN APLICATIVO HELPDESK

                    case AplicativosSelenium.HELPDESK:
                        try
                        {
                            IWebElement usuario = driver.FindElement(By.Id("Accounts_UserName"));
                            usuario.SendKeys(UsuarioApp);
                            System.Threading.Thread.Sleep(5000);
                            IWebElement contrasena = driver.FindElement(By.Id("Accounts_Password"));
                            contrasena.SendKeys(ClaveApp);
                            System.Threading.Thread.Sleep(2000);
                            IWebElement button = driver.FindElement(By.XPath("/html/body/div/div/section/div[3]/div/div/div[1]/form/div/div[3]/input"));
                            button.Click();
                        }
                        catch (Exception ex)
                        {
                            CloseRobot();
                            LogueoAutomatico(UsuarioApp, ClaveApp, LinkAplicativo, IdAplicativo);
                            throw new Exception(ex.Message);
                        }
                        break;

                    //EMPIEZA RPA DE LOGIN APLICATIVO TEAMS

                    case AplicativosSelenium.TEAMS:
                        try
                        {

                            IWebElement usuario = driver.FindElement(By.XPath("/html/body/div/form[1]/div/div/div[2]/div[1]/div/div/div/div/div[1]/div[3]/div/div/div/div[2]/div[2]/div/input[1]"));
                            usuario.SendKeys(UsuarioApp);
                            System.Threading.Thread.Sleep(5000);
                            driver.FindElement(By.XPath("/html/body/div/form[1]/div/div/div[2]/div[1]/div/div/div/div/div[1]/div[3]/div/div/div/div[4]/div/div/div/div[2]/input")).Click();
                            System.Threading.Thread.Sleep(5000);
                            IWebElement contrasena = driver.FindElement(By.XPath("/html/body/div/form[1]/div/div/div[2]/div[1]/div/div/div/div/div/div[3]/div/div[2]/div/div[2]/div/div[2]/input"));
                            contrasena.SendKeys(ClaveApp);
                            System.Threading.Thread.Sleep(2000);
                            IWebElement button = driver.FindElement(By.XPath("/html/body/div/form[1]/div/div/div[2]/div[1]/div/div/div/div/div/div[3]/div/div[2]/div/div[3]/div[2]/div/div/div/div/input"));
                            button.Click();
                        }
                        catch (Exception ex)
                        {
                            CloseRobot();
                            LogueoAutomatico(UsuarioApp, ClaveApp, LinkAplicativo, IdAplicativo);
                            throw new Exception(ex.Message);
                        }
                        break;


                    //EMPIEZA RPA DE LOGIN APLICATIVO DIAGNOSTICADOR

                    case AplicativosSelenium.DIAGNOSTICADOR:
                        try
                        {
                            IWebElement usuario = driver.FindElement(By.XPath("/html/body/div/div/div[2]/div/div[2]/div/div/div/div[1]/div/div/div/div[1]/div[2]/input"));
                            usuario.SendKeys(UsuarioApp);
                            System.Threading.Thread.Sleep(5000);
                            IWebElement contrasena = driver.FindElement(By.XPath("/html/body/div/div/div[2]/div/div[2]/div/div/div/div[1]/div/div/div/div[2]/div[2]/input"));
                            contrasena.SendKeys(ClaveApp);
                            System.Threading.Thread.Sleep(2000);
                            IWebElement button = driver.FindElement(By.XPath("/html/body/div/div/div[2]/div/div[2]/div/div/div/div[1]/div/div/div/div[3]/div/div/span"));
                            button.Click();
                        }
                        catch (Exception ex)
                        {
                            CloseRobot();
                            LogueoAutomatico(UsuarioApp, ClaveApp, LinkAplicativo, IdAplicativo);
                            throw new Exception(ex.Message);
                        }
                        break;


                    //EMPIEZA RPA DE LOGIN APLICATIVO MI CLARO ASESOR
                    case AplicativosSelenium.MI_CLARO_ASESOR:
                        try
                        {
                            IWebElement usuario = driver.FindElement(By.Id("username"));
                            usuario.SendKeys(UsuarioApp);
                            System.Threading.Thread.Sleep(5000);
                            IWebElement contrasena = driver.FindElement(By.Id("password"));
                            contrasena.SendKeys(ClaveApp);
                            System.Threading.Thread.Sleep(2000);
                            IWebElement button = driver.FindElement(By.Id("login-button"));
                            button.Click();
                        }
                        catch (Exception ex)
                        {
                            CloseRobot();
                            LogueoAutomatico(UsuarioApp, ClaveApp, LinkAplicativo, IdAplicativo);
                            throw new Exception(ex.Message);
                        }
                        break;

                    //EMPIEZA RPA DE LOGIN APLICATIVO CMAX

                    case AplicativosSelenium.CMAX:
                        try
                        {
                            IWebElement usuario = driver.FindElement(By.Id("j_username"));
                            usuario.SendKeys(UsuarioApp);
                            System.Threading.Thread.Sleep(5000);
                            IWebElement contrasena = driver.FindElement(By.Id("j_password"));
                            contrasena.SendKeys(ClaveApp);
                            System.Threading.Thread.Sleep(2000);
                            IWebElement button = driver.FindElement(By.XPath("/html/body/div[1]/div[1]/div[1]/div/div[2]/div[4]/form/table/tbody/tr[3]/td/input"));
                            button.Click();
                        }
                        catch (Exception ex)
                        {
                            CloseRobot();
                            LogueoAutomatico(UsuarioApp, ClaveApp, LinkAplicativo, IdAplicativo);
                            throw new Exception(ex.Message);
                        }
                        break;

                    //EMPIEZA RPA DE LOGIN APLICATIVO T&D

                    case AplicativosSelenium.TYD:
                        try
                        {
                            IWebElement usuario = driver.FindElement(By.Id("username"));
                            usuario.SendKeys(UsuarioApp);
                            System.Threading.Thread.Sleep(5000);
                            IWebElement contrasena = driver.FindElement(By.Id("password"));
                            contrasena.SendKeys(ClaveApp);
                            System.Threading.Thread.Sleep(2000);
                            IWebElement button = driver.FindElement(By.XPath("/html/body/div/div/div/div/div/form/fieldset/button"));
                            button.Click();
                        }
                        catch (Exception ex)
                        {
                            CloseRobot();
                            LogueoAutomatico(UsuarioApp, ClaveApp, LinkAplicativo, IdAplicativo);
                            throw new Exception(ex.Message);
                        }
                        break;

                    //EMPIEZA RPA DE LOGIN APLICATIVO SARA

                    case AplicativosSelenium.SARA:
                        try
                        {
                            IWebElement usuario = driver.FindElement(By.XPath("/html/body/div[3]/div/div[2]/p[1]/input"));
                            usuario.SendKeys(UsuarioApp);
                            System.Threading.Thread.Sleep(5000);
                            IWebElement contrasena = driver.FindElement(By.XPath("/html/body/div[3]/div/div[2]/p[2]/input"));
                            contrasena.SendKeys(ClaveApp);
                            System.Threading.Thread.Sleep(2000);
                            IWebElement button = driver.FindElement(By.XPath("/html/body/div[3]/div/button"));
                            button.Click();
                        }
                        catch (Exception ex)
                        {
                            CloseRobot();
                            LogueoAutomatico(UsuarioApp, ClaveApp, LinkAplicativo, IdAplicativo);
                            throw new Exception(ex.Message);
                        }
                        break;

                    //EMPIEZA RPA DE LOGIN APLICATIVO POLIEDRO 1

                    case AplicativosSelenium.POLIEDRO_1:
                        try
                        {
                            IWebElement usuario = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtUsuario"));
                            usuario.SendKeys(UsuarioApp);
                            System.Threading.Thread.Sleep(5000);
                            driver.FindElement(By.XPath("/html/body/form/div[3]/div[2]/section/div/div[2]/div[2]/main/div[2]/div/div[1]/fieldset/div[1]/div[3]/div/input")).Click();
                            IWebElement contrasena = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtContraseña"));
                            contrasena.SendKeys(ClaveApp);
                            System.Threading.Thread.Sleep(2000);
                            IWebElement button = driver.FindElement(By.Id("btnIngresarUsuarioContraseña"));
                            button.Click();
                        }
                        catch (Exception ex)
                        {
                            CloseRobot();
                            LogueoAutomatico(UsuarioApp, ClaveApp, LinkAplicativo, IdAplicativo);
                            throw new Exception(ex.Message);
                        }
                        break;

                    //EMPIEZA RPA DE LOGIN APLICATIVO POLIEDRO 2

                    case AplicativosSelenium.POLIEDRO_2:
                        try
                        {
                            IWebElement usuario = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtUsuario"));
                            usuario.SendKeys(UsuarioApp);
                            System.Threading.Thread.Sleep(5000);
                            driver.FindElement(By.XPath("/html/body/form/div[3]/div[2]/section/div/div[2]/div[2]/main/div[2]/div/div[1]/fieldset/div[1]/div[3]/div/input")).Click();
                            IWebElement contrasena = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtContraseña"));
                            contrasena.SendKeys(ClaveApp);
                            System.Threading.Thread.Sleep(2000);
                            IWebElement button = driver.FindElement(By.Id("btnIngresarUsuarioContraseña"));
                            button.Click();
                        }
                        catch (Exception ex)
                        {
                            CloseRobot();
                            LogueoAutomatico(UsuarioApp, ClaveApp, LinkAplicativo, IdAplicativo);
                            throw new Exception(ex.Message);
                        }
                        break;

                    //EMPIEZA RPA DE LOGIN APLICATIVO POLIEDRO 3

                    case AplicativosSelenium.POLIEDRO_3:
                        try
                        {
                            IWebElement usuario = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtUsuario"));
                            usuario.SendKeys(UsuarioApp);
                            System.Threading.Thread.Sleep(5000);
                            driver.FindElement(By.XPath("/html/body/form/div[3]/div[2]/section/div/div[2]/div[2]/main/div[2]/div/div[1]/fieldset/div[1]/div[3]/div/input")).Click();
                            IWebElement contrasena = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtContraseña"));
                            contrasena.SendKeys(ClaveApp);
                            System.Threading.Thread.Sleep(2000);
                            IWebElement button = driver.FindElement(By.Id("btnIngresarUsuarioContraseña"));
                            button.Click();
                        }
                        catch (Exception ex)
                        {
                            CloseRobot();
                            LogueoAutomatico(UsuarioApp, ClaveApp, LinkAplicativo, IdAplicativo);
                            throw new Exception(ex.Message);
                        }
                        break;

                    //EMPIEZA RPA DE LOGIN APLICATIVO POLIEDRO 4

                    case AplicativosSelenium.POLIEDRO_4:
                        try
                        {
                            IWebElement usuario = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtUsuario"));
                            usuario.SendKeys(UsuarioApp);
                            System.Threading.Thread.Sleep(5000);
                            driver.FindElement(By.XPath("/html/body/form/div[3]/div[2]/section/div/div[2]/div[2]/main/div[2]/div/div[1]/fieldset/div[1]/div[3]/div/input")).Click();
                            IWebElement contrasena = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtContraseña"));
                            contrasena.SendKeys(ClaveApp);
                            System.Threading.Thread.Sleep(2000);
                            IWebElement button = driver.FindElement(By.Id("btnIngresarUsuarioContraseña"));
                            button.Click();
                        }
                        catch (Exception ex)
                        {
                            CloseRobot();
                            LogueoAutomatico(UsuarioApp, ClaveApp, LinkAplicativo, IdAplicativo);
                            throw new Exception(ex.Message);
                        }
                        break;

                    //EMPIEZA RPA DE LOGIN APLICATIVO POLIEDRO 5

                    case AplicativosSelenium.POLIEDRO_5:
                        try
                        {
                            IWebElement usuario = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtUsuario"));
                            usuario.SendKeys(UsuarioApp);
                            System.Threading.Thread.Sleep(5000);
                            driver.FindElement(By.XPath("/html/body/form/div[3]/div[2]/section/div/div[2]/div[2]/main/div[2]/div/div[1]/fieldset/div[1]/div[3]/div/input")).Click();
                            IWebElement contrasena = driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtContraseña"));
                            contrasena.SendKeys(ClaveApp);
                            System.Threading.Thread.Sleep(2000);
                            IWebElement button = driver.FindElement(By.Id("btnIngresarUsuarioContraseña"));
                            button.Click();
                        }
                        catch (Exception ex)
                        {
                            CloseRobot();
                            LogueoAutomatico(UsuarioApp, ClaveApp, LinkAplicativo, IdAplicativo);
                            throw new Exception(ex.Message);
                        }
                        break;

                    //EMPIEZA RPA DE LOGIN APLICATIVO WHATSAPP AGENTES

                    case AplicativosSelenium.WHATSAPP_AGENTE:
                        try
                        {
                            IWebElement usuario = driver.FindElement(By.XPath("/html/body/div[2]/div/section/div/div/div[2]/div[2]/form/div[1]/input"));
                            usuario.SendKeys(UsuarioApp);
                            System.Threading.Thread.Sleep(5000);
                            IWebElement contrasena = driver.FindElement(By.XPath("/html/body/div[2]/div/section/div/div/div[2]/div[2]/form/div[2]/input"));
                            contrasena.SendKeys(ClaveApp);
                            System.Threading.Thread.Sleep(2000);
                            IWebElement button = driver.FindElement(By.XPath("/html/body/div[2]/div/section/div/div/div[2]/div[2]/form/button"));
                            button.Click();
                        }
                        catch (Exception ex)
                        {
                            CloseRobot();
                            LogueoAutomatico(UsuarioApp, ClaveApp, LinkAplicativo, IdAplicativo);
                            throw new Exception(ex.Message);
                        }
                        break;

                    //EMPIEZA RPA DE LOGIN APLICATIVO WHATSAPP SUPERVISOR

                    case AplicativosSelenium.WHATSAPP_SUPERVISOR:
                        try
                        {
                            IWebElement usuario = driver.FindElement(By.Id("ctl00_contentplaceholderContenido_textboxUser"));
                            usuario.SendKeys(UsuarioApp);
                            System.Threading.Thread.Sleep(5000);
                            IWebElement contrasena = driver.FindElement(By.Id("ctl00_contentplaceholderContenido_textboxPassword"));
                            contrasena.SendKeys(ClaveApp);
                            System.Threading.Thread.Sleep(2000);
                            IWebElement button = driver.FindElement(By.Id("sumbitLogin"));
                            button.Click();
                        }
                        catch (Exception ex)
                        {
                            CloseRobot();
                            LogueoAutomatico(UsuarioApp, ClaveApp, LinkAplicativo, IdAplicativo);
                            throw new Exception(ex.Message);
                        }
                        break;

                    //EMPIEZA RPA DE LOGIN APLICATIVO MY APPS

                    case AplicativosSelenium.MY_APPS:
                        try
                        {
                            IWebElement usuario = driver.FindElement(By.Id("usuario"));
                            usuario.SendKeys(UsuarioApp);
                            System.Threading.Thread.Sleep(5000);
                            IWebElement contrasena = driver.FindElement(By.Id("contra"));
                            contrasena.SendKeys(ClaveApp);
                            System.Threading.Thread.Sleep(2000);
                            IWebElement button = driver.FindElement(By.XPath("/html/body/div[5]/header/div[1]/div[1]/div/nav/div[3]/form/div"));
                            button.Click();
                        }
                        catch (Exception ex)
                        {
                            CloseRobot();
                            LogueoAutomatico(UsuarioApp, ClaveApp, LinkAplicativo, IdAplicativo);
                            throw new Exception(ex.Message);
                        }
                        break;


                    default:
                        break;
                }

                return RedirectToAction("Index", "AsignacionAplicativos");
            }
            else
            {
                TempData["Mensaje"] = "No es posible el logueo automático, porque algunos datos son requeridos.";
                return RedirectToAction("Index", "AsignacionAplicativos");
            }


        }

        public static void CloseRobot()
        {
            try
            {
                Thread.Sleep(1000);
                System.Diagnostics.Process processChrome = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfoChrome = new System.Diagnostics.ProcessStartInfo();
                startInfoChrome.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfoChrome.FileName = "cmd.exe";
                startInfoChrome.Arguments = "/c " + "taskkill /F /IM chrome.exe";
                processChrome.StartInfo = startInfoChrome;
                processChrome.Start();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
        }


        public bool ValidarItemsLogueoAutomatico(string usuarioApp, string claveApp, string linkAplicativo)
        {
            if (string.IsNullOrEmpty(usuarioApp) || string.IsNullOrEmpty(claveApp) || string.IsNullOrEmpty(linkAplicativo)) return false;

            return true;
        }


        public ActionResult ReturnStatusUserApp(string document)
        {
            var list = new List<StatusUserAppViewModel>();

            if (!string.IsNullOrEmpty(document))
            {
                list = db.Database.SqlQuery<StatusUserAppViewModel>("P_ReporteAppUsuarioPorDocumento @Documento={0}", document.Trim()).ToList();
            }

            ViewBag.TotalRegistros = list.Count();
            return View(list);
        }

    }
}
