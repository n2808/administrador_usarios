﻿using ClaroGrandesSuperficies.BusinessLogic.Validaciones;
using ClaroGrandesSuperficies.Constantes;
using ClaroGrandesSuperficies.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClaroGrandesSuperficies.Controllers
{
    public class CargueUsuariosBloqueadosController : Controller
    {
        private ContextModel db = new ContextModel();

        CargueDesbloqueoUsuariosBS cargueDesbloqueoUsuarios = new CargueDesbloqueoUsuariosBS();
        public ActionResult IndexUnlock()
        {
            return View();
        }

        public JsonResult CargarArchivoUsuarios()
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;

                    HttpPostedFileBase file = files[0];
                    string fileName = file.FileName;

                    EliminarArchivosResources();

                    string path = Path.Combine(Server.MapPath("~/Resources/CargueDesbloqueoAppUsuario"),
                                  Path.GetFileName(file.FileName));
                    file.SaveAs(path);

                    var dt = cargueDesbloqueoUsuarios.InsertarUsuariosAplicativos(path);

                    return Json(new { status = 201, message = "El archivo se ha cargado correctamente!" });

                }

                catch (Exception ex)
                {
                    return Json(new { status = 504, message = Configuraciones.MENSAJE_ERROR_FORMATO_INCORRECTO });
                }
            }

            return Json("Por favor seleccione el archivo");
        }


        public void EliminarArchivosResources()
        {
            try
            {
                var pathResources = Path.Combine(Server.MapPath("~/Resources/CargueDesbloqueoAppUsuario").ToString());

                DirectoryInfo di = new DirectoryInfo(pathResources);

                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();

                }
            }
            catch (Exception ex)
            {
                throw new Exception(Configuraciones.MENSAJE_ERROR_FORMATO_INCORRECTO);
            }
        }

    }
}
