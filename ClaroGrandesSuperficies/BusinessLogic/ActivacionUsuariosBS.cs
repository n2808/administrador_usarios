﻿using AdministracionUsuarios.BusinessLogic.Validaciones;
using ClaroGrandesSuperficies.Models;
using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Web.Mvc;

namespace AdministracionUsuarios.BusinessLogic
{
    public class ActivacionUsuariosBS
    {
        private static ContextModel db = new ContextModel();

        public bool InsertarUsuarios(string path)
        {
            LimpiarTablaTemporalBS.TruncarTablaTemporal("ActivacionAplicativo");
            var dt = this.CargarExcel(path);

            if (this.BulkUsuarios(dt))
            {
                return true;
            }
            else
            {
                return false;

            }

        }


        public DataTable CargarExcel(string cadenaConexion)
        {

            var strconn = ("Provider=Microsoft.ACE.OLEDB.12.0;" +
                 ("Data Source=" + (cadenaConexion + ";Extended Properties=\"Excel 12.0;HDR=YES\"")));

            DataTable dataTable = new DataTable();


            OleDbConnection mconn = new OleDbConnection(strconn);
            mconn.Open();
            DataTable dtSchema = mconn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

            if ((null != dtSchema) && (dtSchema.Rows.Count > 0))
            {
                //string firstSheetName = dtSchema.Rows[0]["TABLE_NAME"].ToString();
                string firstSheetName = "App$";
                new OleDbDataAdapter("SELECT * FROM [" + firstSheetName + "]", mconn).Fill(dataTable);
            }
            mconn.Close();

            return dataTable;
        }

        public bool BulkUsuarios(DataTable dtUsuarios)
        {


            using (SqlBulkCopy bulkcopy = new SqlBulkCopy(db.Database.Connection.ConnectionString))
            {
                bulkcopy.DestinationTableName = "ActivacionAplicativo";
                bulkcopy.DestinationTableName = "ActivacionAplicativo";
                bulkcopy.ColumnMappings.Add("app", "Aplicativonombre");
                bulkcopy.ColumnMappings.Add("Documentousuario", "UsuarioAplicativo");
                bulkcopy.ColumnMappings.Add("Estado", "EstadoAplicativoNombre");


                try
                {
                    bulkcopy.WriteToServer(dtUsuarios);
                    int errores = ValidarCamposVaciosBS.ValidarCamposVacios("P_VALIDAR_ACTIVACIONUSUARIOS");

                    if (errores > 0)
                    {
                        return false;
                    }
                    else
                    {
                        InsertarUserPasswords();
                        return true;

                    }

                }
                catch (Exception)
                {

                    return false;
                }
            }

        }





        public void InsertarUserPasswords()
        {
            try
            {
                SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString);

                SqlCommand comandoInsert = new SqlCommand("P_INSERTAR_ACTIVACIONUSUARIOS", connection);

                comandoInsert.CommandType = CommandType.StoredProcedure;

                // Add the input parameter and set its properties.
                SqlParameter parameter = new SqlParameter();

                comandoInsert.Parameters.Clear();

                connection.Open();
                SqlDataReader reader = comandoInsert.ExecuteReader();

                connection.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("Error al ejecutar procedimiento almacenado. \n" + ex.Message.ToString());
            }
        }




    }
}