﻿using ClaroGrandesSuperficies.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AdministracionUsuarios.BusinessLogic.Validaciones
{
    public static class LimpiarTablaTemporalBS
    {

        private static ContextModel db = new ContextModel();

        public static void TruncarTablaTemporal(string Tabla)
        {

            try
            {
                SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString);

                SqlCommand comandoInsert = new SqlCommand("TRUNCATE TABLE "+ Tabla, connection);

                comandoInsert.CommandType = CommandType.Text;

                // Add the input parameter and set its properties.
                SqlParameter parameter = new SqlParameter();

                comandoInsert.Parameters.Clear();

                connection.Open();
                SqlDataReader reader = comandoInsert.ExecuteReader();

                connection.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("Error al ejecutar procedimiento almacenado. \n" + ex.Message.ToString());
            }
        }

    }
}