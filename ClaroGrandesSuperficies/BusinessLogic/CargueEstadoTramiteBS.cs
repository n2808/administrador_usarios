﻿using AdministracionUsuarios.BusinessLogic.Validaciones;
using ClaroGrandesSuperficies.Constantes;
using ClaroGrandesSuperficies.Models;
using ClaroGrandesSuperficies.Viewmodel;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace ClaroGrandesSuperficies.BusinessLogic
{
    public class CargueEstadoTramiteBS
    {
        private static ContextModel db = new ContextModel();
        public bool InsertarCargueEstadoTramite(string path)
        {

            try
            {
                LimpiarTablaTemporalBS.TruncarTablaTemporal("TemporalEstadoTramites");

                DataTable dtUsuarios = CargarExcel(path);
                BulkCargueEstadoTramite(dtUsuarios);
                return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public DataTable CargarExcel(string cadenaConexion)
        {

            try
            {
                var strconn = ("Provider=Microsoft.ACE.OLEDB.12.0;" +
                ("Data Source=" + (cadenaConexion + ";Extended Properties=\"Excel 12.0;HDR=YES\"")));

                DataTable dataTable = new DataTable();


                OleDbConnection mconn = new OleDbConnection(strconn);
                mconn.Open();
                DataTable dtSchema = mconn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                if ((null != dtSchema) && (dtSchema.Rows.Count > 0))
                {
                    //string firstSheetName = dtSchema.Rows[0]["TABLE_NAME"].ToString();
                    string firstSheetName = "Cargue$";
                    new OleDbDataAdapter("SELECT * FROM [" + firstSheetName.Trim() + "]", mconn).Fill(dataTable);
                }
                mconn.Close();

                return dataTable;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


        public void BulkCargueEstadoTramite(DataTable dtUsuarios)
        {
            List<ErroresFormatoVM> errores = new List<ErroresFormatoVM>();

            using (SqlBulkCopy bulkcopy = new SqlBulkCopy(db.Database.Connection.ConnectionString))
            {
                bulkcopy.DestinationTableName = "TemporalEstadoTramites";
                bulkcopy.ColumnMappings.Add("Cedula", "Documento");
                bulkcopy.ColumnMappings.Add("Nombre_Aplicativo", "NombreAplicativo");
                bulkcopy.ColumnMappings.Add("Estado_Tramite", "EstadoTramite");
                bulkcopy.ColumnMappings.Add("Observacion", "Observacion");

                try
                {
                    bulkcopy.WriteToServer(dtUsuarios);
                    errores = ValidarCamposVaciosBS.ValidarCamposVaciosUsuariosAPlicativos("P_ValidarCamposVaciosEstadoTramite");


                    if (errores.Count > 0)
                    {
                        ExportExcel(errores);
                        throw new Exception(Configuraciones.MENSAJE_ERROR_FORMATO_ASIGNACION);
                    }
                    else
                    {
                        InsertarCargue();
                    }

                }
                catch (Exception ex)
                {
                    if (ex.HResult == -2146233079)
                    {
                        throw new Exception(Configuraciones.MENSAJE_ERROR_COLUMNAS);
                    }
                    else
                    {
                        throw new Exception(ex.Message);
                    }
                }
            }
        }

        public void InsertarCargue()
        {
            try
            {
                SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString);

                SqlCommand comandoInsert = new SqlCommand("SP_ActualizarEstadoTramite", connection);

                comandoInsert.CommandType = CommandType.StoredProcedure;

                // Add the input parameter and set its properties.
                SqlParameter parameter = new SqlParameter();

                comandoInsert.Parameters.Clear();

                connection.Open();
                SqlDataReader reader = comandoInsert.ExecuteReader();

                connection.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("Error al ejecutar procedimiento almacenado. \n" + ex.Message.ToString());
            }
        }


        public void ExportExcel(List<ErroresFormatoVM> erroresFormatoCargue)
        {

            var archivoErroresCargue = ValidarArchivoExistenteCarpeta();

            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
            ExcelPackage pck = new ExcelPackage(archivoErroresCargue);
            try
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("ErroresCargue" + DateTime.Now);

                ws.Cells["A1"].Value = "Id";
                ws.Cells["B1"].Value = "TipoError";

                int loop = 1;

                foreach (var item in erroresFormatoCargue)
                {
                    loop++;

                    ws.Cells["A" + loop].Value = item.Identificacion;
                    ws.Cells["B" + loop].Value = item.TipoError;

                }
                ws.Cells["A:AZ"].AutoFitColumns();
                pck.Save();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public FileInfo ValidarArchivoExistenteCarpeta()
        {
            FileInfo newFile = new FileInfo(HttpContext.Current.Server.MapPath("~/ErroresFormato/CargueEstadoTramite/ErroresFormatoCargueEstadoTramite.xlsx"));

            if (newFile.Exists)
            {
                newFile.Delete();
            }
            return newFile;
        }
    }
}