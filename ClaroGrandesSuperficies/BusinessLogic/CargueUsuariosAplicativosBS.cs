﻿using AdministracionUsuarios.BusinessLogic.Validaciones;
using ClaroGrandesSuperficies.Constantes;
using ClaroGrandesSuperficies.Models;
using ClaroGrandesSuperficies.Viewmodel;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Web;

namespace ClaroGrandesSuperficies.BusinessLogic
{
    public class CargueUsuariosAplicativosBS
    {

        private static ContextModel db = new ContextModel();
        public bool InsertarUsuariosAplicativos(string path)
        {

            try
            {
                LimpiarTablaTemporalBS.TruncarTablaTemporal("TemporalAsignacionAplicativosUsuario");

                DataTable dtUsuarios = CargarExcel(path);

                BulkAsignacionAplicativosUsuario(dtUsuarios);

                return true;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public DataTable CargarExcel(string cadenaConexion)
        {

            try
            {
                var strconn = ("Provider=Microsoft.ACE.OLEDB.12.0;" +
                ("Data Source=" + (cadenaConexion + ";Extended Properties=\"Excel 12.0;HDR=YES\"")));

                DataTable dataTable = new DataTable();


                OleDbConnection mconn = new OleDbConnection(strconn);
                mconn.Open();
                DataTable dtSchema = mconn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                if ((null != dtSchema) && (dtSchema.Rows.Count > 0))
                {
                    string firstSheetName1 = dtSchema.Rows[2]["TABLE_NAME"].ToString();
                    string firstSheetName = "Cargue$";
                    new OleDbDataAdapter("SELECT * FROM [" + firstSheetName.Trim() + "]", mconn).Fill(dataTable);
                }
                mconn.Close();

                return dataTable;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


        public void BulkAsignacionAplicativosUsuario(DataTable dtUsuarios)
        {
     

            List<ErroresFormatoVM> errores = new List<ErroresFormatoVM>();

            using (SqlBulkCopy bulkcopy = new SqlBulkCopy(db.Database.Connection.ConnectionString))
            {
                bulkcopy.DestinationTableName = "TemporalAsignacionAplicativosUsuario";
                bulkcopy.ColumnMappings.Add("Cedula", "Documento");
                bulkcopy.ColumnMappings.Add("Nombre_Aplicativo", "NombreAplicativo");
                bulkcopy.ColumnMappings.Add("Usuario_Aplicativo", "UsuarioAplicativo");
                bulkcopy.ColumnMappings.Add("Clave_Aplicativo", "ClaveAplicativo");
                bulkcopy.ColumnMappings.Add("Son_Para_Formacion", "EsFormacion");

                try
                {
                    bulkcopy.WriteToServer(dtUsuarios);
                    errores = ValidarCamposVaciosBS.ValidarCamposVaciosUsuariosAPlicativos("P_ValidarCamposVaciosAsignacionAplicativos");


                    if (errores.Count > 0)
                    {
                        ExportExcel(errores);
                        throw new Exception(Configuraciones.MENSAJE_ERROR_FORMATO_ASIGNACION);
                    }
                    else
                    {
                        InsertarAplicativosUsuario();
                    }
                                        

                }
                catch (Exception ex)
                {
                    if (ex.HResult == -2146233079)
                    {
                        throw new Exception(Configuraciones.MENSAJE_ERROR_COLUMNAS);
                    }
                    else
                    {
                        throw new Exception(ex.Message);
                    }

                }
            }
        }

        public void InsertarAplicativosUsuario()
        {
            try
            {
                SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString);

                SqlCommand comandoInsert = new SqlCommand("SP_CrearActualizarUsuarios", connection);

                comandoInsert.CommandType = CommandType.StoredProcedure;

                // Add the input parameter and set its properties.
                SqlParameter parameter = new SqlParameter();

                comandoInsert.Parameters.Clear();

                connection.Open();
                SqlDataReader reader = comandoInsert.ExecuteReader();

                connection.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("Error al ejecutar procedimiento almacenado. \n" + ex.Message.ToString());
            }
        }


        public void ExportExcel(List<ErroresFormatoVM> erroresFormatoCargue)
        {

            var archivoErroresCargue = ValidarArchivoExistenteCarpeta();

            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
            ExcelPackage pck = new ExcelPackage(archivoErroresCargue);
            try
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("ErroresCargue" + DateTime.Now);

                ws.Cells["A1"].Value = "Id";
                ws.Cells["B1"].Value = "TipoError";

                int loop = 1;

                foreach (var item in erroresFormatoCargue)
                {
                    loop++;

                    ws.Cells["A" + loop].Value = item.Identificacion;
                    ws.Cells["B" + loop].Value = item.TipoError;

                }
                ws.Cells["A:AZ"].AutoFitColumns();
                pck.Save();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public FileInfo ValidarArchivoExistenteCarpeta()
        {
            FileInfo newFile = new FileInfo(HttpContext.Current.Server.MapPath("~/ErroresFormato/ErroresFormatoAplicativosUsuario.xlsx"));

            if (newFile.Exists)
            {
                newFile.Delete();
            }
            return newFile;
        }


    }
}